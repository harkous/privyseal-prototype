PrivySeal: Share data on the cloud privately!
=====================

> **NOTE:** This is Alpha quality code!

**Table of Contents**

- Introduction
- Software Overview
- Requirements
- Build, installation and execution
- Issue Tracking
- Licensing
- Contact

# Introduction

PrivySeal is a software that provides easily usable privacy technologies to end users who want to share their data on any cloud service. It is based on published research and employs crowdsourcing and a psychologically grounded model for risk estimation. PrivySeal provides three key services: 1) It determines the risk of sharing data items on the cloud; 2) it provides recommendations for risk mitigation and finally 3) it provides privacy enhancing technologies to mitigate the risk. 

# Software Overview

PrivySeal provides client side privacy enhancing technologies to end-users. 

**Some of the main features of the client-side include:**

* **Encrpytion**: PrivySeal allows users to encrypt their files
* **Face Blurring**: PrivySeal detects faces in images and allows users to blur them
* **Hiding image parts**: PrivySeal provides users the very attractive feature of manually selecting any part of an image and then blurring it
* **Hiding metadata**: PrivySeal can extract metadata from a wide variety of files and allows the users to hide any metadata item.
* **Thumbnail Generation**: PrivySeal allows users to generate thumbnails of documents and images to share on the cloud along-with encrypted data items



**The server side component has been implemented separately but is not part of this current release.
However, it will soon be integrated here.** 

The server side component aggregates data from different clients and
after each sharing operation from the clients, the context of the item and the policy applied are sent to the server.
The server side then based on the processing and analysis of the crowdsourced
information, guides users about the privacy risk that is posed by
sharing different items in different contexts. We use Item Response
Theory (IRT) which is a well-known psychometric function that has been widely
used in psychology, education, public health, and computer adaptive testing.

The server side shall also provide recommendations to clients on what policies to apply to different
data items in order to mitigate risk

An overview of what the software will do in completion is illustrated in the following figure:

![PrivySeal-PETS for Dummies-page-001.jpg](https://bitbucket.org/repo/yMrgxo/images/4219489903-PrivySeal-PETS%20for%20Dummies-page-001.jpg)


# Requirements

Developed using Eclipse and Java 1.7.


# Build, installation and execution

The project can be opened in Eclipse and built from there.

# Issue Tracking
For the moment, we are going to use bitbucket for issue tracking.

# Licensing
PrivySeal is licensed under the GPLv3.

# Contact
Contact Hamza Harkous, Rameez Rahman at their EPFL address (google it!)