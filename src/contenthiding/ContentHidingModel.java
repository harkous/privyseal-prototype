package contenthiding;

import static com.googlecode.javacv.cpp.opencv_highgui.cvLoadImage;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

import com.googlecode.javacv.cpp.opencv_core.CvRect;
import com.googlecode.javacv.cpp.opencv_core.IplImage;

import contenthiding.thumbnailing.ThumbnailGenerator;
import de.uni_siegen.wineme.come_in.thumbnailer.FileDoesNotExistException;
import de.uni_siegen.wineme.come_in.thumbnailer.ThumbnailerException;
import guidesign.MainWindow;

public final class ContentHidingModel {
	private final String THUMBNAIL_PATH = "thumbnails/";
	private final String[] supportedImageFormats = {"png", "jpg"};
	private final String[] supportedDocumentFormats = {"pdf", "odp", "ods", "odt"};
	public final static int[] resolutionBaseValues = {200, 400, 600, 800, 1000};
	public final static int[] resolutionA4DocValues = {141, 283, 424, 566, 707};

	private static ContentHidingModel instance = null;
	
	private DocSummary docSummary;
	private ThumbnailGenerator thumbGen;

	private ContentHidingModel(){
		docSummary = new DocSummary();		
		thumbGen = new ThumbnailGenerator();
	}
	
	public static synchronized ContentHidingModel getInstance() {
	      if(instance == null) {
	         instance = new ContentHidingModel();
	      }
	      return instance;
	   }
	
	public String[] getSupportedImageFormats() {
		return supportedImageFormats;
	}

	public String[] getSupportedDocumentFormats() {
		return supportedDocumentFormats;
	}
	
	public File generateThumbnail(File inFile, int width, int height) throws Exception{
		File thumb = null;
		thumbGen.setInFile(inFile);
		thumbGen.setOutFolder(new File(THUMBNAIL_PATH));
		thumbGen.setThumbnailSize(width, height);
		
		thumb = thumbGen.createThumbnail();
		
		return thumb;
	}
	
	public String createSummary(File inFile) throws IOException{
		//if new doc or no summary -> create new summary
			docSummary.setDoc(inFile);
			docSummary.summarize();
		
		return docSummary.getSummary();
		
	}
	
	public void setSummary(String text){
		docSummary.setSummary(text);
	}
	
	public String getSummary(){
		return docSummary.getSummary();
	}

	public int[][] generateDocResolutions() throws IOException{
		//Store resolutions
		int[][] resoultions = new int[resolutionA4DocValues.length][2];
		for (int i = 0; i < resoultions.length; i++) {
			resoultions[i][0] = resolutionA4DocValues[i];
			resoultions[i][1] = resolutionBaseValues[i];
		}
		
		return resoultions;
	}
	
	public int[][] generateImageResolutions(String path) throws IOException{
		//Read image file
		BufferedImage image = ImageIO.read(new File(path));
		//Store resolutions
		int[][] resoultions = new int[resolutionBaseValues.length][2];
		
		//Determine resolutions, always the wider side is the round number
		//e.g. width > height -> width = 200 height = a smaller value ensuring
		//that the image stays proportional
		int j = 0;
		for (int i : resolutionBaseValues) {		
			if(image.getWidth() >= i || image.getHeight() >= i){
				if(image.getWidth() > image.getHeight()){
					float ratio = (float)image.getWidth() / i;
					float resizedHeight = (float)image.getHeight() / ratio;
					resoultions[j][0] = i;
					resoultions[j][1] = (int)resizedHeight;
				}else{
					float ratio = (float)image.getHeight() / i;
					float resizedWidth = (float)image.getWidth() / ratio;
					resoultions[j][0] = (int)resizedWidth;
					resoultions[j][1] = i;
				}
				j++;
			}else{
				resoultions = null;
				break;
			}
		}
		
		return resoultions;
	}
	
	public BufferedImage blur(BufferedImage src, List<? extends Rectangle> rectList, int intensity, float scaleRatio){
		//Create copy of source img
		BufferedImage blurredImage = new BufferedImage(src.getWidth(), src.getHeight(), src.getType());
		blurredImage.setData(src.copyData(null));
		
		BoxBlurFilter blurFilter = new BoxBlurFilter(intensity, intensity, 1);
		
		for(Rectangle r : rectList){
			if(scaleRatio > 0){
				blurFilter.filter(blurredImage, blurredImage, Math.round((float)r.x / scaleRatio), Math.round((float)r.y / scaleRatio), Math.round((float)r.width / scaleRatio), Math.round((float)r.height / scaleRatio));
			}else{
				blurFilter.filter(blurredImage, blurredImage, r.x, r.y, r.width, r.height);
			}
		}
		
		return blurredImage;
	}
	
	public BufferedImage blur(BufferedImage src, Rectangle r, int intensity){
		//Create copy of source img
		BufferedImage blurredImage = new BufferedImage(src.getWidth(), src.getHeight(), src.getType());
		blurredImage.setData(src.copyData(null));
		
		BoxBlurFilter blurFilter = new BoxBlurFilter(intensity, intensity, 1);
		
		blurFilter.filter(blurredImage, blurredImage, r.x, r.y, r.width, r.height);
		
		return blurredImage;
	}
	
	public List<CvRect> detectFaces(File f){
		IplImage img = cvLoadImage(f.getPath());
		//Face detection
		ArrayList<CvRect> faceAreas = new ArrayList<>();
		FaceDetector.detect(img, faceAreas);
		
		return faceAreas;
	}
}
