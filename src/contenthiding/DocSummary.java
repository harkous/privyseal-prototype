package contenthiding;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.swing.JOptionPane;

import net.sf.classifier4J.summariser.SimpleSummariser;

public class DocSummary {
	private File doc;
	private String summary;
	
	public DocSummary(){
		doc = null;
		summary = null;
	}
	
	public String summarize() throws IOException{
	
		byte[] encoded = Files.readAllBytes(Paths.get(doc.getPath()));
		String text = Charset.defaultCharset().decode(ByteBuffer.wrap(encoded)).toString();
		SimpleSummariser sum = new SimpleSummariser();
		if(!text.equals("")){
			summary = sum.summarise(text, 5);
		}else{
			summary = "";
		}

		return summary;
	}
	
	public String getSummary(){
		return summary;
	}
	
	public void setSummary(String sum){
		this.summary = sum;
	}

	public File getDoc() {
		return doc;
	}

	public void setDoc(File doc) {
		this.doc = doc;
	}
}
