package contenthiding;

import java.util.ArrayList;

import com.googlecode.javacv.cpp.opencv_core.CvMemStorage;
import com.googlecode.javacv.cpp.opencv_core.CvRect;
//import com.googlecode.javacv.cpp.opencv_core.CvScalar;
import com.googlecode.javacv.cpp.opencv_core.CvSeq;
import com.googlecode.javacv.cpp.opencv_core.IplImage;
import com.googlecode.javacv.cpp.opencv_objdetect.CvHaarClassifierCascade;

import static com.googlecode.javacv.cpp.opencv_core.cvLoad;
import static com.googlecode.javacv.cpp.opencv_core.cvGetSeqElem;
import static com.googlecode.javacv.cpp.opencv_core.cvClearMemStorage;
//import static com.googlecode.javacv.cpp.opencv_core.cvPoint;
//import static com.googlecode.javacv.cpp.opencv_core.CV_AA;
//import static com.googlecode.javacv.cpp.opencv_core.cvRectangle;
//import static com.googlecode.javacv.cpp.opencv_highgui.cvSaveImage;
//import static com.googlecode.javacv.cpp.opencv_highgui.cvWaitKey;
//import static com.googlecode.javacv.cpp.opencv_highgui.cvLoadImage;
import static com.googlecode.javacv.cpp.opencv_objdetect.cvHaarDetectObjects;
 
public class FaceDetector{
 
	public static final String XML_FILE = "conf/haarcascade_frontalface_alt.xml";
	
	public static void detect(IplImage src, ArrayList<CvRect> faceAreas){
 
		CvHaarClassifierCascade cascade = new CvHaarClassifierCascade(cvLoad(XML_FILE));
		CvMemStorage storage = CvMemStorage.create();
		CvSeq sign = cvHaarDetectObjects(
				src,
				cascade,
				storage,
				1.2,
				3,
				0);
 
		cvClearMemStorage(storage);
 
		int total_Faces = sign.total();		
		System.out.println("Faces fetected: " + total_Faces);
		
		for(int i = 0; i < total_Faces; i++){
			
			CvRect r = new CvRect(cvGetSeqElem(sign, i));
			faceAreas.add(r);
			/*
			cvRectangle (
					src,
					cvPoint(r.x(), r.y()),
					cvPoint(r.width() + r.x(), r.height() + r.y()),
					CvScalar.RED,
					2,
					CV_AA,
					0); 
			*/
		}
 
		//cvShowImage("Result", src);
		//cvSaveImage("img/group_new.jpg", src);
		//cvWaitKey(0);
 
	}			
}