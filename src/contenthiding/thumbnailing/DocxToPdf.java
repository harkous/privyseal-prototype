package contenthiding.thumbnailing;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;

import org.apache.poi.xwpf.converter.pdf.PdfConverter;
import org.apache.poi.xwpf.converter.pdf.PdfOptions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import contenthiding.ContentHidingModel;

public class DocxToPdf {

	public static void main(String[] args) {
		// create();
		// create();
	}

	public static void create(File inFile, File outFile) {
		long startTime = System.currentTimeMillis();
//		File outFile = null;
		try {
			// 1) Load docx with POI XWPFDocument
	        FileInputStream is = new FileInputStream(inFile);

			XWPFDocument document = new XWPFDocument(is);
			long stopTime = System.currentTimeMillis();
			System.out.println("loading with " + (stopTime - startTime)
					+ " ms.");

			// 2) Convert POI XWPFDocument 2 PDF with iText
//			outFile = new File(inFile.getPath()+"_out");
			outFile.getParentFile().mkdirs();

			OutputStream out = new FileOutputStream(outFile);
			PdfOptions options = null;// PDFViaITextOptions.create().fontEncoding(
										// "windows-1250" );
			PdfConverter.getInstance().convert(document, out, options);
			//
			// long stopTime2 = System.currentTimeMillis();
			// System.out.println( "loading with " + ( stopTime2 - stopTime ) +
			// " ms." );
			//
			// ContentHidingModel contentHidingModel =
			// ContentHidingModel.getInstance();
			// contentHidingModel.generateThumbnail(outFile, 256,256);
			// System.out.println( "loading with " + (
			// System.currentTimeMillis() - stopTime2 ) + " ms." );

		} catch (Throwable e) {
			e.printStackTrace();
		}

//		System.out.println("Generate DocxResume.pdf with "
//				+ (System.currentTimeMillis() - startTime) + " ms.");
//		return outFile;

	}

}
