package contenthiding.thumbnailing;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.URL;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.text.Document;
import com.itextpdf.text.html.simpleparser.HTMLWorker;
import com.itextpdf.text.pdf.PdfWriter;

/** * This Class will generate PDF * */
public class PDFGenerator {
	public static void main(String[] args) throws Exception {
		 FileOutputStream fileOutputStream = new
		 FileOutputStream("target/Template.pdf");
		 String htmlString = FileUtils.readFileToString(new
		 File("target/package.html"));//("E:/Projects_Products/knowledgePlus/PDFCreator/index.html");
		
		 try {
		 String k ;//= "<html><body> This is my Project </body></html>";
		 String inputFile= "target/output.html";
		 URL url2 = new File(inputFile).toURI().toURL();
		
		 InputStream in = url2.openStream();
		
		 k= IOUtils.toString(in);
		 OutputStream file = new FileOutputStream(new
		 File("target/test.pdf"));
		 Document document = new Document();
		 PdfWriter writer = PdfWriter.getInstance(document, file);
		 document.open();
		 InputStream is = new ByteArrayInputStream(k.getBytes());
		 XMLWorkerHelper.getInstance().parseXHtml(writer, document, is);
		 document.close();
		 file.close();
		 } catch (Exception e) {
		 e.printStackTrace();
		 }
//		try {
//			String k;// = "<html><body> This is my Project </body></html>";
//			String inputFile = "target/output.html";
//			URL url2 = new File(inputFile).toURI().toURL();
//
//			InputStream in = url2.openStream();
//
//			k = IOUtils.toString(in);
//			OutputStream file = new FileOutputStream(
//					new File("target/Test.pdf"));
//			Document document = new Document();
//			PdfWriter.getInstance(document, file);
//			document.open();
//			HTMLWorker htmlWorker = new HTMLWorker(document);
//			htmlWorker.parse(new StringReader(k));
//			document.close();
//			file.close();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		Document document = new Document();
//		PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("target/test.pdf"));
//		document.open();
////		XMLWorkerHelper.getInstance().parseXHtml(writer, document, new FileInputStream("target/BarChartExample.html"), new FileInputStream("target/excelStyle.css"));
//		XMLWorkerHelper.getInstance().parseXHtml(writer, document, new FileInputStream("target/output.html"));
		
//		document.close();
	}

}