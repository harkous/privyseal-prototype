package contenthiding.thumbnailing;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFSlide;

public class PptxToPng {
	public static void main (String args[]) throws IOException{
		create (new File ("target/PPTXProjectAndDeveloppersWithVelocity.pptx"),new File("target/PPTXProjectAndDeveloppersWithVelocity.png"));
	}
	public static void create(File inFile, File outFile) throws IOException {
		//"target/PPTXProjectAndDeveloppersWithVelocity.pptx"
		FileInputStream is = new FileInputStream(inFile);
        XMLSlideShow ppt = new XMLSlideShow(is);
       
        is.close();

        double zoom = 4; // magnify it by 2
        AffineTransform at = new AffineTransform();
        at.setToScale(zoom, zoom);
        
        Dimension pgsize = ppt.getPageSize();

        XSLFSlide[] slide = ppt.getSlides();
        
//        for (int i = 0; i < slide.length; i++) {
        for (int i = 0; i < 1; i++) {
            BufferedImage img = new BufferedImage((int)Math.ceil(pgsize.width*zoom), (int)Math.ceil(pgsize.height*zoom), BufferedImage.TYPE_INT_RGB);
            Graphics2D graphics = img.createGraphics();
            graphics.setTransform(at);

            graphics.setPaint(Color.white);
            graphics.fill(new Rectangle2D.Float(0, 0, pgsize.width, pgsize.height));
            slide[i].draw(graphics);
            FileOutputStream out = new FileOutputStream(outFile);
            javax.imageio.ImageIO.write(img, "png", out);
            out.close();
        }
    }
}