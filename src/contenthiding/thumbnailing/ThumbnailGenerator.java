package contenthiding.thumbnailing;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FilenameUtils;

import de.uni_siegen.wineme.come_in.thumbnailer.FileDoesNotExistException;
import de.uni_siegen.wineme.come_in.thumbnailer.ThumbnailerException;
import de.uni_siegen.wineme.come_in.thumbnailer.ThumbnailerManager;
import de.uni_siegen.wineme.come_in.thumbnailer.thumbnailers.NativeImageThumbnailer;
import de.uni_siegen.wineme.come_in.thumbnailer.thumbnailers.OpenOfficeThumbnailer;
import de.uni_siegen.wineme.come_in.thumbnailer.thumbnailers.PDFBoxThumbnailer;

public class ThumbnailGenerator {
	private static final String LOG4J_CONFIG_FILE = "conf/privyseal.log4j.properties";
	private ThumbnailerManager thumbnailer;
	private File inFile;
	private File outFolder;

	public ThumbnailGenerator() {

		thumbnailer = new ThumbnailerManager();
		// Default image size
		thumbnailer.setImageSize(160, 120, 0);

		thumbnailer.registerThumbnailer(new NativeImageThumbnailer());
		thumbnailer.registerThumbnailer(new OpenOfficeThumbnailer());
		thumbnailer.registerThumbnailer(new PDFBoxThumbnailer());

		try {
			de.uni_siegen.wineme.come_in.thumbnailer.Main
					.initLogging(LOG4J_CONFIG_FILE);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public File createThumbnail() throws Exception {
		thumbnailer.setThumbnailFolder(outFolder);
		//TODO: later change it to a temporary path
		File temp = new File(inFile.getPath() + "_out");
		File outputFile = null;

		switch (FilenameUtils.getExtension(inFile.getPath()).toLowerCase()) {
			case ("docx") :
				DocxToPdf.create(inFile, temp);
				outputFile = thumbnailer.createThumbnail(temp);
				temp.delete();
				break;
			case ("pptx") :
				PptxToPng.create(inFile, temp);
				outputFile = thumbnailer.createThumbnail(temp);
				temp.delete();
				break;
			case ("ppt") :
				PptToPng.create(inFile, temp);
				outputFile = thumbnailer.createThumbnail(temp);
				temp.delete();
				break;
			case ("xlsx") :
				XlsxToPdf.create(inFile, temp);
				outputFile = thumbnailer.createThumbnail(temp);
				temp.delete();
				break;
			case ("xls") :
				XlsToPdf.create(inFile, temp);
				outputFile = thumbnailer.createThumbnail(temp);
				temp.delete();
				break;
			default :
				outputFile = thumbnailer.createThumbnail(inFile);
				if (temp != null) {
					temp.delete();
				}
				break;
		}
		return outputFile;
	}

	public File getOutFolder() {
		return outFolder;
	}

	public void setOutFolder(File outFolder) {
		this.outFolder = outFolder;
	}

	public File getInFile() {
		return inFile;
	}

	public void setInFile(File inFile) {
		this.inFile = inFile;
	}

	public void setThumbnailSize(int width, int height) {
		thumbnailer.setImageSize(width, height, 0);
	}

}
