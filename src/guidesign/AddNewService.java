package guidesign;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JSeparator;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.events.Attribute;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class AddNewService extends JDialog implements ActionListener{

	private final JPanel contentPanel = new JPanel();
	private JTextField textFieldName;
	private JTextField textFieldLogo;
	private JTextField textFieldSyncFolder;
	private JTextField textFieldPrivateFolder;
	private JLabel lblNewLabel;
	private JLabel lblLogooptioanal;
	private JButton btnBrowse;
	private JLabel lblSyncFoler;
	private JButton btnBrowse_1;
	private JLabel lblPrivateFolderName;
	private JButton cancelButton;
	private HashMap<String, String> serviceInfo;
	private int choice;
	private String PSRootFolder;


	/**
	 * Create the dialog.
	 * @wbp.parser.constructor
	 */
	public AddNewService(JFrame frame, String rootFolder) {
		super(frame, true);
		this.PSRootFolder = rootFolder;
		createUI(frame, null, null, null, null);
		
	}

	public AddNewService(JFrame frame, String name, String iconPath, String serviceFolder, String privySealFolder, String rootFolder){
		super(frame, true);
		this.PSRootFolder = rootFolder;
		createUI(frame, name, iconPath, serviceFolder, privySealFolder);
	}
	
	private void createUI(JFrame frame, String name, String iconPath, String serviceFolder, String privySealFolder){
		setTitle("Add new cloud service provider");
		setBounds(100, 100, 632, 238);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBackground(Color.WHITE);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.EAST);

		lblNewLabel = new JLabel("Name:");

		textFieldName = new JTextField();
		textFieldName.setColumns(10);
		textFieldName.setText(name);

		lblLogooptioanal = new JLabel("Logo (optional):");

		textFieldLogo = new JTextField();
		textFieldLogo.setColumns(10);
		textFieldLogo.setText(iconPath);

		btnBrowse = new JButton("Browse...");
		btnBrowse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LookAndFeel swingLF = UIManager.getLookAndFeel();
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());					
					JFileChooser selectFile = new JFileChooser();
					UIManager.setLookAndFeel(swingLF);
					selectFile.setFileSelectionMode(JFileChooser.FILES_ONLY);
					int returnVal = selectFile.showOpenDialog(contentPanel);
					if(returnVal == JFileChooser.APPROVE_OPTION){
						textFieldLogo.setText(selectFile.getSelectedFile().getAbsolutePath());
					}
				} catch (ClassNotFoundException | InstantiationException
						| IllegalAccessException
						| UnsupportedLookAndFeelException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}					
			}
		});

		lblSyncFoler = new JLabel("Location of synchronized folder:");

		textFieldSyncFolder = new JTextField();
		textFieldSyncFolder.setColumns(10);
		textFieldSyncFolder.setText(serviceFolder);
		
		btnBrowse_1 = new JButton("Browse...");
		btnBrowse_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LookAndFeel swingLF = UIManager.getLookAndFeel();
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());					
					JFileChooser selectFile = new JFileChooser();
					UIManager.setLookAndFeel(swingLF);
					selectFile.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
					int returnVal = selectFile.showOpenDialog(contentPanel);
					if(returnVal == JFileChooser.APPROVE_OPTION){
						textFieldSyncFolder.setText(selectFile.getSelectedFile().getAbsolutePath());
					}
				} catch (ClassNotFoundException | InstantiationException
						| IllegalAccessException
						| UnsupportedLookAndFeelException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}	
			}
		});
		
		lblPrivateFolderName = new JLabel("Name of private folder");
		
		textFieldPrivateFolder = new JTextField();
		textFieldPrivateFolder.setColumns(10);
		textFieldPrivateFolder.setText(privySealFolder);
		
		JSeparator separator = new JSeparator();
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addGap(44)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(lblLogooptioanal)
						.addComponent(lblNewLabel))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(textFieldName, GroupLayout.DEFAULT_SIZE, 365, Short.MAX_VALUE)
						.addComponent(textFieldLogo, GroupLayout.DEFAULT_SIZE, 365, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnBrowse)
					.addGap(32))
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addGap(42)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(lblSyncFoler)
						.addComponent(lblPrivateFolderName))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(textFieldSyncFolder, GroupLayout.DEFAULT_SIZE, 280, Short.MAX_VALUE)
						.addComponent(textFieldPrivateFolder, GroupLayout.DEFAULT_SIZE, 280, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.RELATED, 10, GroupLayout.PREFERRED_SIZE)
					.addComponent(btnBrowse_1)
					.addGap(31))
				.addComponent(separator, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 606, Short.MAX_VALUE)
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addGap(3)
							.addComponent(lblNewLabel))
						.addComponent(textFieldName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(13)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblLogooptioanal)
						.addComponent(textFieldLogo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnBrowse))
					.addGap(18)
					.addComponent(separator, GroupLayout.PREFERRED_SIZE, 2, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblSyncFoler)
						.addComponent(textFieldSyncFolder, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnBrowse_1))
					.addGap(6)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblPrivateFolderName)
						.addComponent(textFieldPrivateFolder, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(61, Short.MAX_VALUE))
		);
		
		contentPanel.setLayout(gl_contentPanel);
		
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);

		JButton okButton = new JButton("OK");
		okButton.setActionCommand("OK");
		okButton.addActionListener(this);
		buttonPane.add(okButton);
		getRootPane().setDefaultButton(okButton);
		
		cancelButton = new JButton("Cancel");
		cancelButton.setActionCommand("Cancel");
		cancelButton.addActionListener(this);
		buttonPane.add(cancelButton);
				
		pack();
		setLocationRelativeTo(frame);
		setVisible(true);
}
	public Map<String, String> getServiceInfo(){
		return serviceInfo;
	}
	
	public String getServiceName(){
		return serviceInfo.get("name");
	}
	
	public String getServiceLogo(){
		return serviceInfo.get("logoPath");
	}
	
	public String getServiceFolder(){
		return serviceInfo.get("serviceFolder");
	}
	
	
	public String getServicePrivySealFolder(){
		return serviceInfo.get("privySealFolder");
	}


	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getActionCommand().equals("Cancel")){
			choice = 1;
		}else if (arg0.getActionCommand().equals("OK")){
			serviceInfo = new HashMap<>();
			serviceInfo.put("name", textFieldName.getText());
			serviceInfo.put("logoPath", textFieldLogo.getText());
			serviceInfo.put("serviceFolder", textFieldSyncFolder.getText());
			serviceInfo.put("privySealFolder", textFieldPrivateFolder.getText());
			//Create service folder if doesn't exists
			File serviceDir = new File(PSRootFolder + File.separator + textFieldPrivateFolder.getText());
			if(!serviceDir.exists()){
				serviceDir.mkdir();
			}
			createPropertiesXML(serviceDir);
			
			choice = 0;
		}
		
		this.dispose();
		
	}

	public void createPropertiesXML(File dir){
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder;
		try {
			docBuilder = docFactory.newDocumentBuilder();

			// root elements
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("jFileSync");
			doc.appendChild(rootElement);
			// JFS profile
			Element JFSDir = doc.createElement("directory");
			JFSDir.setAttribute("src", dir.getPath());
			JFSDir.setAttribute("tgt", textFieldSyncFolder.getText());			
			rootElement.appendChild(JFSDir);
			
			// Add encryptList
			Element encList = doc.createElement("encryptList");
			rootElement.appendChild(encList);			

			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			transformerFactory.setAttribute("indent-number", 4); // doesn't
																	// work...
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(dir.getAbsolutePath() + File.separator + "settings.xml"));

			// Output to console for testing
			// StreamResult result = new StreamResult(System.out);

			transformer.transform(source, result);

			// System.out.println("File saved!");			
		} catch (TransformerException te) {
			te.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public int getChoice() {
		return choice;
	}
}
