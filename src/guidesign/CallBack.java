package guidesign;

public interface CallBack {
    void methodToCallBackOnClick(Boolean state);
    void methodToCallBackOnHover(Boolean state);
    
}
