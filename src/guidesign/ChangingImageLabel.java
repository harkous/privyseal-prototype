package guidesign;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

class ChangingImageLabel extends JLabel {

	boolean eventsEnabled = false;
	boolean turned_on = false;
	String on;
	String off;
	String ask_turn_on;
	String ask_turn_off;

	String currentState = "off";
	CallBack callback = null;

	public ChangingImageLabel(String on_, String off_, String ask_turn_on_,
			String ask_turn_off_, boolean enabled_, boolean turned_on_,
			CallBack callback_) {
		this(on_, off_, ask_turn_on_, ask_turn_off_, enabled_, turned_on_);
		callback = callback_;
		registerClick(turned_on);
	}

	public ChangingImageLabel(String on_, String off_, String ask_turn_on_,
			String ask_turn_off_, boolean enabled_, boolean turned_on_) {
		on = "/img/" + on_ + ".png";
		off = "/img/" + off_ + ".png";
		ask_turn_on = "/img/" + ask_turn_on_ + ".png";
		ask_turn_off = "/img/" + ask_turn_off_ + ".png";

		setIcon(new ImageIcon(MainWindow.class.getResource(off)));
		setOpaque(false);
		addMouseListener(new ImagePanelMouseListner());
		eventsEnabled = enabled_;
		turned_on = turned_on_;
	}

	public String getState() {
		return currentState;
	}

	@Override
	public void setEnabled(boolean eventsEnabled_) {
		super.setEnabled(eventsEnabled_);
		eventsEnabled = eventsEnabled_;
	}

	public void setState(String state) {
		currentState = state;

		switch (state) {
		case "on":
			setImage(on);
			break;
		case "ask_turn_on":
			setImage(ask_turn_on);
			break;
		case "off":
			setImage(off);
			break;
		case "ask_turn_off":
			setImage(ask_turn_off);
			break;
		default:
			break;

		}
	}

	public void setImage(String resourePath) {
		setIcon(new ImageIcon(MainWindow.class.getResource(resourePath)));

	}

	public void registerClick(boolean state) {
		if (callback != null) {
			callback.methodToCallBackOnClick(state);
		}
	}
	public void registerHover(boolean state) {
		if (callback != null) {
			callback.methodToCallBackOnHover(state);
		}
	}
	public class ImagePanelMouseListner implements MouseListener {

		@Override
		public void mouseClicked(java.awt.event.MouseEvent arg0) {
			if (eventsEnabled) {
				turned_on = !turned_on;
				if (turned_on) {
					setState("on");
				} else {
					setState("off");
				}
				registerClick(turned_on);
			}
		}

		@Override
		public void mouseEntered(java.awt.event.MouseEvent arg0) {
			if (eventsEnabled) {

				if (turned_on) {
					setState("ask_turn_off");
				} else {
					setState("ask_turn_on");
				}
				registerHover(true);
			}
		}

		@Override
		public void mouseExited(java.awt.event.MouseEvent arg0) {
			if (eventsEnabled) {
				if (turned_on) {
					setState("on");
				} else {
					setState("off");
				}
				registerHover(false);
			}
		}

		@Override
		public void mousePressed(java.awt.event.MouseEvent arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseReleased(java.awt.event.MouseEvent arg0) {
			// TODO Auto-generated method stub

		}
	}

}
