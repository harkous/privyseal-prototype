package guidesign;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.BoxLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

public class DocSummaryPreview extends JDialog implements ActionListener{

	private final JPanel contentPanel = new JPanel();
	private JButton btnOk;
	private JButton btnCancel;
	private JTextArea textArea;
	private JPanel panel;
	private boolean saveText;
	private String summaryText;
	private JScrollPane scrollPane;
	private JPanel panel_1;
	/*
	public static void main(String[] args) {
		try {
			JFrame frame = new JFrame();
			frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			frame.setVisible(true);
			DocSummaryPreview dialog = new DocSummaryPreview(frame, "Hello World!");
			System.out.println(dialog.isSaveText());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	*/
	
	/**
	 * Create the dialog.
	 */
	public DocSummaryPreview(JFrame frame, String text) {
		super(frame, true);
		this.summaryText = text;
		createUI(frame, summaryText);
	}
	
	private void createUI(JFrame frame, String text){
		setTitle("Summary preview");
		setBounds(100, 100, 632, 400);
		contentPanel.setBackground(GuiParameters.panelBackgroundColor);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPanel);
		contentPanel.setLayout(new BorderLayout(0, 0));
		
//		JLabel lblSummary = new JLabel("Summary:");
//		lblSummary.setHorizontalAlignment(SwingConstants.CENTER);
//		lblSummary.setAlignmentX(Component.CENTER_ALIGNMENT);
//		lblSummary.setBackground(GuiParameters.panelBackgroundColor);
//		lblSummary.setForeground(GuiParameters.defaultTxtColor);
//		contentPanel.add(lblSummary, BorderLayout.NORTH);
		
		scrollPane = new JScrollPane();
		contentPanel.add(scrollPane);
		
		panel_1 = new JPanel();
		panel_1.setBackground(GuiParameters.panelBackgroundColor);
		scrollPane.setViewportView(panel_1);
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.X_AXIS));
		
		textArea = new JTextArea(text);
		textArea.setWrapStyleWord(true);
		textArea.setLineWrap(true);
		panel_1.add(textArea);
		textArea.setEditable(true);
		textArea.setBorder(new LineBorder(Color.BLACK));
		textArea.setBackground(GuiParameters.panelBackgroundColor);
		textArea.setForeground(GuiParameters.defaultTxtColor);
		
		panel = new JPanel();
		panel.setBackground(GuiParameters.panelBackgroundColor);
		contentPanel.add(panel, BorderLayout.SOUTH);
		
		btnOk = new JButton("Save modification");
		panel.add(btnOk);
		btnOk.setBackground(Color.WHITE);
		btnOk.setActionCommand("OK");
		btnOk.addActionListener(this);
		
		btnCancel = new JButton("Cancel");
		panel.add(btnCancel);
		btnCancel.setBackground(Color.WHITE);
		btnCancel.setActionCommand("Cancel");
		btnCancel.addActionListener(this);
		
		setLocationRelativeTo(frame);
		setVisible(true);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equals("Cancel")){
			saveText = false;
		}else if (e.getActionCommand().equals("OK")){
			saveText = true;
			//Overwrite summaryText
			summaryText = textArea.getText();
		}
		this.dispose();
		
	}
	
	public String getSummaryText() {
		return summaryText;
	}

	public void setSummaryText(String summaryText) {
		this.summaryText = summaryText;
	}

	public boolean isSaveText() {
		return saveText;
	}

	public void setSaveText(boolean saveText) {
		this.saveText = saveText;
	}

}
