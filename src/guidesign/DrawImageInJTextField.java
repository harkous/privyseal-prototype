package guidesign;

import java.awt.*;  
import javax.swing.*;  
class DrawImageInJTextField extends JFrame  
{  
  public DrawImageInJTextField()  
  {  
    setDefaultCloseOperation(EXIT_ON_CLOSE);  
    getContentPane().add(new ImagePanel());  
    pack();  
    setLocationRelativeTo(null);  
  }  
  class ImagePanel extends JPanel  
  {  
    JTextField tf = new JTextField(10);  
    Image img;  
    public ImagePanel()  
    {  
      setLayout(new GridLayout(1,1));  
      setPreferredSize(tf.getPreferredSize());  
      tf.setOpaque(false);  
      add(tf);  
      try  
      {  
        img = javax.imageio.ImageIO.read(MainWindow.class.getResource("/img/metadata.png"));  
      }  
      catch(Exception e){}//do nothing  
    }  
    public void paintComponent(Graphics g)  
    {  
      super.paintComponent(g);  
      if(img != null) g.drawImage(img, 0,0,this.getWidth(),this.getHeight(),this);  
    }  
  }  
  public static void main(String[] args){new DrawImageInJTextField().setVisible(true);}  
}  