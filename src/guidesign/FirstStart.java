package guidesign;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.LookAndFeel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import javax.swing.BoxLayout;
import java.awt.Component;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.ImageIcon;

import org.apache.poi.xwpf.usermodel.TextAlignment;

public class FirstStart extends JDialog implements ActionListener {

	private final JPanel contentPanel = new JPanel();
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			JFrame frame = new JFrame();
			FirstStart dialog = new FirstStart(frame);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public FirstStart(JFrame parentFrame) {
		super(parentFrame, true);
		
		setTitle("PrivySeal");
		setBounds(100, 100, 450, 580);
		getContentPane().setLayout(new BorderLayout());
		createUI();
		addWindowListener(new WindowAdapter() {
			@Override  
		      public void windowClosing(WindowEvent e) {  
		        System.exit(0);
		      }  
		});
		setLocationRelativeTo(parentFrame);
		setVisible(true);
	}

	public void createUI(){
		{
			JPanel panel = new JPanel();
			panel.setBackground(Color.WHITE);
			getContentPane().add(panel, BorderLayout.NORTH);
			{
				JLabel lblWelcome = new JLabel("Welcome to PrivySeal!");
				lblWelcome.setHorizontalTextPosition(SwingConstants.CENTER);
				lblWelcome.setVerticalTextPosition(SwingConstants.TOP);
				lblWelcome.setIcon(new ImageIcon(FirstStart.class.getResource("/img/bigstroke.png")));
				panel.add(lblWelcome);
			}
		}
		contentPanel.setBackground(Color.WHITE);
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			JLabel lblPleaseSetThe = new JLabel("Please set the private root folder of this application:");
			contentPanel.add(lblPleaseSetThe);
		}
		{
			textField = new JTextField();
			contentPanel.add(textField);
			textField.setColumns(25);
		}
		{
			JButton btnBrowse = new JButton("Browse...");
			btnBrowse.setBackground(Color.WHITE);
			btnBrowse.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					LookAndFeel swingLF = UIManager.getLookAndFeel();
					try {
						UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
						JFileChooser selectFile = new JFileChooser();
						UIManager.setLookAndFeel(swingLF);
						selectFile.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
						int returnVal = selectFile.showOpenDialog(contentPanel);
						if(returnVal == JFileChooser.APPROVE_OPTION){
							textField.setText(selectFile.getSelectedFile().getAbsolutePath());
						}	
					} catch (ClassNotFoundException
							| InstantiationException
							| IllegalAccessException
							| UnsupportedLookAndFeelException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}												
				}
			});
			contentPanel.add(btnBrowse);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBackground(Color.WHITE);
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
			{
				JButton okButton = new JButton("OK");
				okButton.setBackground(Color.WHITE);
				okButton.setAlignmentX(Component.CENTER_ALIGNMENT);
				okButton.setActionCommand("OK");
				okButton.addActionListener(this);
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
	}
	public String getPSRootFolder(){
		return textField.getText();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		File f = new File(textField.getText());

		if(!textField.getText().equals("") && f.isDirectory()){
			this.dispose();
		}else{
			JOptionPane.showMessageDialog(this, "Please choose an existing directory.", "Directory not found", JOptionPane.ERROR_MESSAGE);
		}
	}

}
