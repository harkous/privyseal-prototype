package guidesign;

import java.awt.Color;
import java.awt.Font;

public class GuiParameters {
	public static Color darkBlue =new Color(0,144,135);//new Color(0, 115, 197);//new Color(70, 130, 180);//new Color(0, 115, 197);// ;//(25,142,181);//(66,136,138); //39,180,184///new Color(24,24,240);//80, 80, 210);
	public static Color darkGrey = new Color(158, 158, 163);
	public static Color lightBlue1 = new Color(189, 189, 217);
	public static Color lightBlue = new Color(33, 159, 209);
	public static Color lightGray = new Color(242, 242, 242);
	public static Color lightGreen = new Color(0, 217, 0);
	public static Color orange = new Color(255, 96, 0);
	public static Color lightYellow = new Color(255, 255, 23);
	public static Color radioButtonSelectedColor = lightGray;// new Color(255, 250,
														// 212);//
														// (255,196,196);
	public static Color radioButtonSelectedTxtColor = Color.black;
	public static Color radioButtonUnselectedTxtColor = lightGray;
	public static Color radioButtonUnselectedColor = darkBlue;// new Color(227, 125,
															// 125);
	public static Color headerColor = lightGray;
	public static Color panelBackgroundColor = darkBlue;// new
													// Color(211,
													// 211,
													// 211);
	 public static Color bigButtonBgColor ;// Color.RED;// Color.red;
	public static Color bigButtonFrColor;

	public static Color minorButtonBgColor = lightGray;
	public static Color defaultTxtColor = lightGray;
	public static Color minorButtonFrColor = Color.BLACK;
	public static Font highlightedButtonFont = new Font("Lucida Fax", Font.BOLD, 30);
	public static Font tipTextFont = new Font("Lucida Fax", Font.BOLD, 16);
	public static Font helpTextFont = new Font("Lucida Fax", Font.BOLD, 12);
	public static Font titleFont = new Font("Lucida Fax", Font.BOLD, 30);
	public static Font tickFont = new Font("Lucida Fax", Font.BOLD, 24);
	public static Font menuFont = new Font("Lucida Fax", Font.BOLD, 14);
}
