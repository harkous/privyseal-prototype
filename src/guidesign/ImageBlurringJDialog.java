package guidesign;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.PageAttributes.OriginType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferInt;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import javax.imageio.IIOException;
import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.border.EmptyBorder;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.MouseInputAdapter;
import javax.swing.Box;
import javax.swing.JTextArea;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

import com.googlecode.javacv.cpp.opencv_core.CvRect;

import contenthiding.ContentHidingModel;

import javax.swing.JSlider;

public class ImageBlurringJDialog extends JDialog implements ActionListener {

	private final JPanel contentPanel = new JPanel();
	private final int imageDisplayWidth = 775;
	private final int imageDisplayHeight = 436; //16:9 ratio
	private final int minimalRectSize = 5;	//In order to avoid 1-2px difficulty selectable areas
	private JPanel panel;
	private JButton btnOk;
	private File imageFile;
	private JLabel lblImage;
	private JPanel panel_1;
	private JPanel panel_2;
	private JButton btnBlurFaces;
	private Component verticalStrut;
	private Component verticalStrut_1;
	private JButton btnCancel;
	private JTextArea txtrTip;
	private JPanel panel_3;
	private SelectionRectangle selectRect;
	private List<SelectionRectangle> rectList;
	private BufferedImage origImage;
	private BufferedImage currentImage;
	private BufferedImage blurredImage;
	private BufferedImage scaledImage;
	private BufferedImage outputImage;
	private float scaleRatio;
	private ContentHidingModel contentHidingModel;
	private JSlider slider;
	private int blurIntesity;
	private JLabel lblBlurStrength;
	private Component verticalStrut_2;
	private JLabel lblNumBlurredAreas;
	private boolean userChoseOK;
		
	public static void main(String[] args) {
		try {
			JFrame frame = new JFrame();
			frame.setSize(300, 200);
			frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			frame.setVisible(true);
			File f = new File("C:\\Users\\The Tiger\\Desktop\\vuc_nagy.JPG");
			//File f = new File("D:\\BME\\Erasmus\\EPFL\\CloudPrivacy\\context_extraction\\eclipse workspace\\FaceBlurring\\img\\group.jpg");
			ImageBlurringJDialog dialog = new ImageBlurringJDialog(frame, f);
			dialog.setVisible(true);
			System.out.println(dialog.isUserChoseOK());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Create the dialog.
	 */
	public ImageBlurringJDialog(JFrame frame, File imgFile) {
		super(frame, true);
		this.imageFile = imgFile;
		rectList = new ArrayList<>();
		contentHidingModel = ContentHidingModel.getInstance();
		setResizable(false);
		setBounds(100, 100, 1000, 563);
		
		initImages();
		createUI(frame);
		
		addWindowListener(new WindowAdapter() {
			@Override  
		      public void windowClosing(WindowEvent e) {  
				userChoseOK = false;
				//Reset rectangle list
				rectList.removeAll(rectList);
				if(lblNumBlurredAreas != null)
					lblNumBlurredAreas.setText("# of blurred areas: " + rectList.size());
		      }  
		});
	}	

	private void initImages(){
		//Save original image to memory
		try {
			origImage = ImageIO.read(imageFile);
			origImage = colorTransparentPartOfImage(origImage);
			
			outputImage = new BufferedImage(origImage.getWidth(), origImage.getHeight(), origImage.getType());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			origImage = new BufferedImage(100,100,BufferedImage.TYPE_INT_RGB);
			outputImage = new BufferedImage(origImage.getWidth(), origImage.getHeight(), origImage.getType());
			//e.printStackTrace();
		}
		
		scaleRatio = determineScaleRatio(imageDisplayWidth, imageDisplayHeight);
		
		//Create working copy which is smaller in order to fit the screen
		//This is going to be the 'original' while editing
		scaledImage = new BufferedImage(Math.round(origImage.getWidth() * scaleRatio), Math.round(origImage.getHeight() * scaleRatio), origImage.getType());
		Graphics g = scaledImage.getGraphics();
		g.drawImage(origImage, 0, 0, scaledImage.getWidth(), scaledImage.getHeight(), null);
		try {
			//In order to have a file for face detection
			ImageIO.write(scaledImage, "png", new File("scaled_tmp.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//Create the displayed image
		currentImage = new BufferedImage(scaledImage.getWidth(), scaledImage.getHeight(), scaledImage.getType());
		g = currentImage.getGraphics();
		g.drawImage(scaledImage, 0, 0, null);
		g.dispose();
		
		//Show currentImage
		ImageIcon icon = new ImageIcon(currentImage);
		if(lblImage != null)
			lblImage.setIcon(icon);
		
		//Reset rectangle list
		rectList.removeAll(rectList);
		if(lblNumBlurredAreas != null)
			lblNumBlurredAreas.setText("# of blurred areas: " + rectList.size());
	}

	public void createUI(JFrame frame){
		setTitle("Thumbnail preview");
		contentPanel.setBackground(GuiParameters.panelBackgroundColor);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPanel);
		contentPanel.setLayout(null);
		
		JLabel lblTask = new JLabel("Select the areas you want to blur:");
		lblTask.setBounds(5, 5, 984, 14);
		lblTask.setBackground(GuiParameters.panelBackgroundColor);
		lblTask.setForeground(GuiParameters.defaultTxtColor);
		lblTask.setHorizontalAlignment(SwingConstants.LEFT);
		contentPanel.add(lblTask);
		lblTask.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblTask.setFont(GuiParameters.tipTextFont);
		panel_1 = new JPanel();
		panel_1.setBounds(5, 19, 775, 436);
		panel_1.setBackground(GuiParameters.panelBackgroundColor);
		//panel_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		contentPanel.add(panel_1);
		
		lblImage = new JLabel("");
		lblImage.setBackground(Color.DARK_GRAY);
		lblImage.setAlignmentX(Component.CENTER_ALIGNMENT);			

		//Show currentImage
		ImageIcon icon = new ImageIcon(currentImage);
		lblImage.setIcon(icon);
		
		RectangleDrawingMouseListener rectMouseListener = new RectangleDrawingMouseListener();
		lblImage.addMouseListener(rectMouseListener);
		lblImage.addMouseMotionListener(rectMouseListener);
		panel_1.add(lblImage);
		
		panel = new JPanel();
		panel.setBounds(5, 491, 984, 38);
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		panel.setBackground(GuiParameters.panelBackgroundColor);
		contentPanel.add(panel);
		
		txtrTip = new JTextArea();
		txtrTip.setLineWrap(true);
		txtrTip.setSize(765, 30);
		panel.add(txtrTip);
		txtrTip.setText("Tip: You can select the areas that you want to blur by clicking and selecting an area on the picture. You can also detect and blur the  faces by clicking on the blur faces button. If you want to delete your selection, just click on an area and press Delete.");
		txtrTip.setForeground(GuiParameters.defaultTxtColor);
		txtrTip.setBackground(GuiParameters.panelBackgroundColor);
		txtrTip.setFont(GuiParameters.tipTextFont);
		panel_2 = new JPanel();
		panel_2.setBounds(779, 19, 210, 472);
		panel_2.setBorder(new EmptyBorder(0, 5, 0, 5));
		panel_2.setBackground(GuiParameters.panelBackgroundColor);
		contentPanel.add(panel_2);
		panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.Y_AXIS));
		
		verticalStrut_1 = Box.createVerticalStrut(20);
		panel_2.add(verticalStrut_1);
		
		btnBlurFaces = new JButton("Blur Faces");
		btnBlurFaces.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Detect faces
				List<CvRect> faceList = contentHidingModel.detectFaces(new File("scaled_tmp.png"));
				//Add faces to the rectList
				for(CvRect r : faceList){
					SelectionRectangle sr = new SelectionRectangle(new Point(r.x(), r.y()), new Dimension(r.width(), r.height()));
					sr.setSelected(true);
					rectList.add(sr);
				}
				//Paint image with faces blurred and selected
				repaint();
				lblImage.repaint();
			}
		});
		btnBlurFaces.setAlignmentX(Component.CENTER_ALIGNMENT);
		panel_2.add(btnBlurFaces);
		btnBlurFaces.setFont(GuiParameters.helpTextFont);

		verticalStrut = Box.createVerticalStrut(20);
		panel_2.add(verticalStrut);
		
		slider = new JSlider();
		slider.setPaintLabels(true);
		slider.setMajorTickSpacing(1);
		slider.setBackground(GuiParameters.panelBackgroundColor);
		slider.setPaintTicks(true);
		slider.setMinimum(10);
		slider.setMaximum(20);
		slider.setValue(15);		
		//Create the label table.
        Hashtable<Integer, JLabel> labelTable = new Hashtable<Integer, JLabel>();
        JLabel lowLabel=new JLabel("Low");
        lowLabel.setFont(GuiParameters.helpTextFont);
        lowLabel.setForeground(GuiParameters.defaultTxtColor);
        JLabel mediumLabel=new JLabel("Medium");
        mediumLabel.setFont(GuiParameters.helpTextFont);
        mediumLabel.setForeground(GuiParameters.defaultTxtColor);
        JLabel highLabel=new JLabel("High");
        highLabel.setFont(GuiParameters.helpTextFont);

        highLabel.setForeground(GuiParameters.defaultTxtColor);
        
        labelTable.put(new Integer(10),lowLabel );
        labelTable.put(new Integer(15), mediumLabel );
        labelTable.put(new Integer(20), highLabel );
        
        slider.setLabelTable(labelTable);
        slider.setForeground(GuiParameters.defaultTxtColor);
        
        slider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                blurIntesity = slider.getValue();
                repaint();
                lblImage.repaint();
            }
        });
		
		lblBlurStrength = new JLabel("Blur strength:");
		lblBlurStrength.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblBlurStrength.setBackground(GuiParameters.panelBackgroundColor);
		lblBlurStrength.setForeground(GuiParameters.defaultTxtColor);
		lblBlurStrength.setFont(GuiParameters.helpTextFont);
		
		panel_2.add(lblBlurStrength);
        
		panel_2.add(slider);
		
		verticalStrut_2 = Box.createVerticalStrut(20);
		panel_2.add(verticalStrut_2);
		
		panel_3 = new JPanel();
		panel_2.add(panel_3);
		panel_3.setBackground(GuiParameters.panelBackgroundColor);
		
		btnOk = new JButton("OK");
		btnOk.setAlignmentX(Component.CENTER_ALIGNMENT);
		btnOk.setBackground(GuiParameters.minorButtonBgColor);
		btnOk.setForeground(GuiParameters.minorButtonFrColor);
		btnOk.setActionCommand("OK");
		
		btnCancel = new JButton("Cancel");
		btnCancel.setAlignmentX(Component.CENTER_ALIGNMENT);
		btnCancel.setActionCommand("Cancel");
		btnCancel.addActionListener(this);
		btnCancel.setFont(GuiParameters.helpTextFont);

		panel_3.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		panel_3.add(btnOk);
		panel_3.add(btnCancel);
		
		lblNumBlurredAreas = new JLabel("# of blurred areas: 0");
		lblNumBlurredAreas.setBounds(5, 466, 160, 14);
		lblNumBlurredAreas.setBackground(GuiParameters.panelBackgroundColor);
		lblNumBlurredAreas.setForeground(GuiParameters.defaultTxtColor);
		contentPanel.add(lblNumBlurredAreas);
		btnOk.addActionListener(this);
		
		blurIntesity = slider.getValue();
		
		InputMap im = contentPanel.getInputMap(JPanel.WHEN_IN_FOCUSED_WINDOW);
		ActionMap am = contentPanel.getActionMap();

		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "DeleteButton");
		am.put("DeleteButton", new AbstractAction() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				//Delete selected rectangles
				Iterator<SelectionRectangle> it = rectList.listIterator();
				while(it.hasNext()){
					SelectionRectangle sRect = it.next();
					if(sRect.isSelected()){
						it.remove();
						//System.out.println("Rect removed");
					}
				}			
				
				repaint();
				lblImage.repaint();
			}
			
		});
		
		setLocationRelativeTo(frame);
		//setVisible(true);
	}

	public void paintBlurRect(SelectionRectangle r){    	
		Graphics g = currentImage.getGraphics();
		
		//Draw blurred rectangle
		blurredImage = contentHidingModel.blur(scaledImage, selectRect, blurIntesity);
		g.drawImage(blurredImage.getSubimage(selectRect.x, selectRect.y, selectRect.width, selectRect.height), selectRect.x, selectRect.y, null);
		g.dispose();
        
	    //Make selection visible
	    if(r.isSelected()){
		    drawRect(currentImage, currentImage, r);
	    }
	    
	    lblNumBlurredAreas.setText("# of blurred areas: " + rectList.size());
	}
	
	public void paintSelectionRect(SelectionRectangle r){
		//Save previous image
		BufferedImage previousImage = new BufferedImage(currentImage.getWidth(), currentImage.getHeight(), currentImage.getType());
    	Graphics g = previousImage.getGraphics();
    	g.drawImage(currentImage, 0, 0, null);
    	
    	g = currentImage.getGraphics();
    	
		//Draw the selection rectangle first on original image so if selectRect is made smaller during selection, no ghost rectangles remain
        if(r != null){
            r = determineRectangleBoundaries(r);
            BufferedImage newRectImage = new BufferedImage(scaledImage.getWidth(), scaledImage.getHeight(), scaledImage.getType());
        	drawRect(scaledImage, newRectImage, r);
        		
        	g.drawImage(newRectImage, 0, 0, null);        	        	
        }
    	
    	//Draw previously added rectangles: copy from previous image rather than calling blur every time
        for(SelectionRectangle re : rectList){
	        if (re != null) {
	        	re = determineRectangleBoundaries(re);	        	
	        }
	                	        
	        g.drawImage(previousImage.getSubimage(re.x, re.y, re.width, re.height), re.x, re.y, null);
	        //drawRect(currentImage, currentImage, re);	       
        }           
        
        g.dispose();
	}
	
	public void repaint() {		
		//Start with a clean image
        Graphics g = currentImage.getGraphics();
		g.drawImage(scaledImage, 0, 0, null);
        
		//Draw previously added rectangles
        for(SelectionRectangle r : rectList){
	        if (r != null) {
	        	r = determineRectangleBoundaries(r);	        	
	        }
	        
	        //Blur the area of the rectangle
	        blurredImage = contentHidingModel.blur(scaledImage, r, blurIntesity);													
	        g.drawImage(blurredImage.getSubimage(r.x, r.y, r.width, r.height), r.x, r.y, null);
	        //drawRect(currentImage, currentImage, r);
	        
	        //Make selection visible
	        if(r.isSelected()){
		        drawRect(currentImage, currentImage, r);
		    }
        }              
        
        g.dispose();
    }

	@Override
	public void setVisible(boolean v){
		super.setVisible(v);
		repaint();
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equals("OK")){
			userChoseOK = true;
			
			int adjustedBlurIntensity;
			//Function implemented: https://www.wolframalpha.com/input/?i=plot+from+x+%3D+100+to+10000%2C+1.16398+%5E%28x%2F273+%2B+15%29+
			if(origImage.getWidth() > origImage.getHeight()){				
				adjustedBlurIntensity = (int)Math.round(Math.pow(1.16398, (double)origImage.getWidth() / 273.0 + blurIntesity));				
			}else{
				adjustedBlurIntensity = (int)Math.round(Math.pow(1.16398, (double)origImage.getHeight() / 273.0 + blurIntesity));
			}
			
			//Create blurred image output
			blurredImage = contentHidingModel.blur(origImage, rectList, adjustedBlurIntensity, scaleRatio);
			Graphics g = outputImage.getGraphics();
			g.drawImage(blurredImage, 0, 0, null);					
			g.dispose();
		}else{
			userChoseOK = false;
			//Reset rectangle list
			rectList.removeAll(rectList);
			if(lblNumBlurredAreas != null)
				lblNumBlurredAreas.setText("# of blurred areas: " + rectList.size());
		}
		
		//Delete temp file created for face blurring
		File scaled_tmp = new File("scaled_tmp.png");
		scaled_tmp.delete();
		
		setVisible(false);
	}
	
	public BufferedImage getOutputImage() {
		return outputImage;
	}

	public boolean isUserChoseOK() {
		return userChoseOK;
	}

	private float determineScaleRatio(float displayWidth, float displayHeight){
		//If scaling is needed
		if(origImage.getWidth() > displayWidth || origImage.getHeight() > displayHeight){
			//Width is the only side that doesn't fit
			if(origImage.getWidth() > displayWidth && origImage.getHeight() <= displayHeight){
				return displayWidth / (float)origImage.getWidth();
			//Height is the only side that doesn't fit
			}else if(origImage.getWidth() <= displayWidth && origImage.getHeight() > displayHeight){
				return  displayHeight / (float)origImage.getHeight();
			//Neither one of them fit
			}else if(origImage.getWidth() > displayWidth && origImage.getHeight() > displayHeight){
				//Compute height
				float tempRatio = displayWidth / (float)origImage.getWidth();
				float tempHeight = (float)origImage.getHeight() * tempRatio;
				//It it doesn't fit
				if(tempHeight > displayHeight){
					//Compute a hight that fits
					float secondRatio = displayHeight / tempHeight;
					float tempWidth = (float)origImage.getWidth() * tempRatio;
					//Determine the new (smaller) width
					tempWidth *= secondRatio;
					//Get the overall ratio
					return tempWidth / (float)origImage.getWidth();
				}
				return tempRatio;				
			}
		}
		
		return 1f;
	}
	
	
	private BufferedImage colorTransparentPartOfImage(BufferedImage image) {
        int width = image.getWidth();
        int height = image.getHeight();
        WritableRaster raster = image.getRaster();

        for (int xx = 0; xx < width; xx++) {
            for (int yy = 0; yy < height; yy++) {
            	Color originalColor = new Color(image.getRGB(xx, yy), true);
            	if(originalColor.getAlpha() < 1){
	                int[] pixels = raster.getPixel(xx, yy, (int[]) null);
	                pixels[0] = 255;
	                pixels[1] = 255;
	                pixels[2] = 255;
	                pixels[3] = 255;
	                raster.setPixel(xx, yy, pixels);
            	}
            }
        }
        return image;
    }
	
	private SelectionRectangle determineRectangleBoundaries(SelectionRectangle r){
		//Check in which direction to draw the rectangle.
    	if(r.getWidth() < 0 && r.getHeight() < 0){
    		//The original starting point becomes the lower right corner
    		Point start = new Point((int)r.getX(), (int)r.getY());
    		//New starting point computation
    		start.x = start.x + (int)r.getWidth();
    		start.y = start.y + (int)r.getHeight();
    		//New dimensions
    		int newWidth = r.x - start.x;
    		int newHeight = r.y - start.y;
    		//New selection rectangle
    		r = new SelectionRectangle(start, new Dimension(newWidth, newHeight));
    	}else if(r.getWidth() < 0 && r.getHeight() >= 0){
    		//The original starting point becomes the top right corner
    		Point start = new Point((int)r.getX(), (int)r.getY());
    		//New starting point computation
    		start.x = start.x + (int)r.getWidth();
    		//New dimensions
    		int newWidth = r.x - start.x;
    		int newHeight = (int)r.getHeight();
    		//New selection rectangle
    		r = new SelectionRectangle(start, new Dimension(newWidth, newHeight));
    	}else if(!(r.getWidth() < 0) && r.getHeight() < 0){
    		//The original starting point becomes the lower right corner
    		Point start = new Point((int)r.getX(), (int)r.getY());
    		//New starting point computation
    		start.y = start.y + (int)r.getHeight();
    		//New dimensions
    		int newWidth =  (int)r.getWidth();
    		int newHeight = r.y - start.y;
    		//New selection rectangle
    		r = new SelectionRectangle(start, new Dimension(newWidth, newHeight));
    	}
    	
    	return r;
	}
	
	private void drawRect(BufferedImage imgIn, BufferedImage imgOut, SelectionRectangle r){
		if(r.width > 0 && r.height > 0){
			Graphics2D g = imgOut.createGraphics();
	        g.drawImage(imgIn,0,0, null);
	        
			//Draw the rectangle
	        g.setColor(Color.RED);
	        if(r.isSelected()){
		        Stroke oldStroke = g.getStroke();
		        int thickness = 4;	//It has to be even to look good
		        g.setStroke(new BasicStroke(thickness));
		        g.draw(r);
		        g.setStroke(oldStroke);
		        //g.setColor(new Color(255,255,255,150));
		        //g.fillRect(r.x + thickness / 2, r.y + thickness / 2, r.width - thickness, r.height - thickness);
	        }else{
	        	g.draw(r);
	        	g.setColor(new Color(255,255,255,150));
	        	g.fill(r);
	        }

	        g.dispose();
		}
	}

	public void setImageFile(File imageFile) {
		this.imageFile = imageFile;
		initImages();
	}

	public File getImageFile() {
		return imageFile;
	}
	
	private class SelectionRectangle extends Rectangle{
		boolean selected;
		public SelectionRectangle(Point p, Dimension d){
			super(p, d);
			selected = false;
		}
		
		public boolean isSelected(){
			return selected;
		}
		
		public void setSelected(boolean s){
			this.selected = s;
		}
	}
	
	private class RectangleDrawingMouseListener extends MouseInputAdapter {
		Point start = new Point();
		
		@Override
		public void mousePressed(MouseEvent me){			
			start = me.getPoint();	        
	        //System.out.println("Starting point: " + start);
		}
		
		@Override
		public void mouseReleased(MouseEvent me){
			Point releasePoint = me.getPoint();
			if(selectRect != null){
				//If it is not a click & gives a minimal rectangle size
				if(selectRect.width > minimalRectSize || selectRect.height > minimalRectSize){
					//Check if rectangle is out of bounds
					if(releasePoint.x >= 0 && releasePoint.x <= scaledImage.getWidth() && releasePoint.y >= 0 && releasePoint.y <= scaledImage.getHeight()){
						//Check if same rectangle is already added or not
						boolean add = true;
						for (SelectionRectangle r : rectList) {
							if(r.x == selectRect.x && r.y == selectRect.y && r.height == selectRect.height && r.width == selectRect.width){
								add = false;
							}
						}
						
						//Add the selected area to list
						if(add){
							rectList.add(selectRect);											    
					        selectRect.setSelected(true);
					        paintBlurRect(selectRect);
							//System.out.println("Rect added");
						}
					}
					
			        repaint();
			        lblImage.repaint();
				}
			}
			
			selectRect = null;
			
		}
		

	    @Override
	    public void mouseDragged(MouseEvent me) {
	        Point end = me.getPoint();
	        selectRect = new SelectionRectangle(start, new Dimension(end.x-start.x, end.y-start.y));
	        selectRect = determineRectangleBoundaries(selectRect);
	        if(selectRect.width > minimalRectSize || selectRect.height > minimalRectSize){
		        paintSelectionRect(selectRect);
		        lblImage.repaint();		        
	        }	        
	        //System.out.println("Rectangle: " + selectRect);
	    }
	    
	    @Override
	    public void mouseClicked(MouseEvent me){
	    	Point clickPoint = me.getPoint();
	    	
	    	for(SelectionRectangle r : rectList){
	    		r.setSelected(false);
	    		//If click is inside a rectangle
	    		//if(clickPoint.x >= r.x && clickPoint.y >= r.y && clickPoint.x <= (r.x + r.width) && clickPoint.y <= (r.y + r.height)){
	    		if(r.contains(clickPoint)){
	    			r.setSelected(true);
	    			//System.out.println("Rect selected");	    			
	    		}
	    		
	    	}
    		repaint();
	    	lblImage.repaint();
	    }
	}
}

