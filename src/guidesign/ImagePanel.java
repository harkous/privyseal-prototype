package guidesign;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

import metadata.MetadataManager;

class ImagePanel extends JPanel {
	JTextPane txtProperty = new JTextPane();
	JTextPane txtPropertyName = new JTextPane();
	MetadataManager.ManagedMetadata propertyType;
	Image img;
	Boolean hidden = false;
	Boolean wasInsidePanel = false;
	String currentState = "shown";

	public ImagePanel(String title, String text) {

		SimpleAttributeSet attribs = new SimpleAttributeSet();
		StyleConstants.setAlignment(attribs, StyleConstants.ALIGN_CENTER);
		StyleConstants.setFontFamily(attribs, "Lucida Fax");
		StyleConstants.setBold(attribs, true);
		StyleConstants.setFontSize(attribs,10);
		StyleConstants.setForeground(attribs, GuiParameters.lightGray);
		
		txtPropertyName.setText(title);
		txtPropertyName.setOpaque(false);
		txtPropertyName.setBorder(null);
		txtPropertyName.setForeground(Color.WHITE);
		txtPropertyName.setParagraphAttributes(attribs, true);
		txtPropertyName.setEditable(false);
		StyleConstants.setForeground(attribs, Color.BLACK);

		txtProperty.setText(text);
		txtProperty.setOpaque(false);
		txtProperty.setBorder(null);
		txtProperty.setParagraphAttributes(attribs, true);
		txtProperty.setEditable(false);
		txtProperty.setPreferredSize(new Dimension (80,100));

//		txtProperty.setBounds(0, 0, 10, 10);
		txtProperty.addMouseListener(new TextAreaMouseListner());
		GroupLayout gl_panelImgCreationDate = new GroupLayout(this);
		gl_panelImgCreationDate.setHorizontalGroup(gl_panelImgCreationDate
				.createParallelGroup(Alignment.CENTER)
				.addGroup(
						Alignment.CENTER,
						gl_panelImgCreationDate.createSequentialGroup()
								.addComponent(txtProperty,
										GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE))
				.addGroup(
						gl_panelImgCreationDate.createSequentialGroup()
								.addComponent(txtPropertyName)));
		gl_panelImgCreationDate.setVerticalGroup(gl_panelImgCreationDate
				.createParallelGroup(Alignment.CENTER).addGroup(
						gl_panelImgCreationDate
								.createSequentialGroup()
								.addContainerGap()
								.addComponent(txtPropertyName)
								.addGap(18)
								.addComponent(txtProperty,
										GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addContainerGap(37, Short.MAX_VALUE)));
		this.setLayout(gl_panelImgCreationDate);

		try {
			img = javax.imageio.ImageIO.read(MainWindow.class
					.getResource("/img/metadata_s.png"));
		} catch (Exception e) {
			e.printStackTrace();
		}

		setPreferredSize(new Dimension(img.getWidth(null), img.getHeight(null)));
		setBackground(Color.WHITE);
		setOpaque(false);

		addMouseListener(new ImagePanelMouseListner());

	}

	public String getState() {
		return currentState;
	}

	public void setState(String state) {
		currentState = state;

		switch (state) {
			case "hidden" :
				setImage("/img/hidden_s.png");
				hidden = true;
				break;
			case "hide?" :
				setImage("/img/hide_ques_s.png");
				break;
			case "shown" :
				setImage("/img/metadata_s.png");
				hidden = false;
				break;
			case "show?" :
				setImage("/img/show_s.png");
				break;
			default :
				break;

		}
	}

	public void setImage(String resourePath) {
		try {
			img = javax.imageio.ImageIO.read(MainWindow.class
					.getResource(resourePath));
			repaint();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (img != null)
			g.drawImage(img, 0, 0, img.getWidth(null), img.getHeight(null),
					this);
	}

	public void setTxtProperty(String prop) {
		this.txtProperty.setText(prop);
	}

	public void setTxtPropertyName(String name) {
		this.txtPropertyName.setText(name);
	}

	public void setPropertyType(MetadataManager.ManagedMetadata type) {
		this.propertyType = type;
	}

	public MetadataManager.ManagedMetadata getPropertyType() {
		return propertyType;
	}

	public class ImagePanelMouseListner implements MouseListener {

		@Override
		public void mouseClicked(java.awt.event.MouseEvent arg0) {
			hidden = !hidden;
			if (hidden) {
				setState("hidden");
			} else {
				setState("shown");
			}
		}

		@Override
		public void mouseEntered(java.awt.event.MouseEvent arg0) {
			if (!wasInsidePanel) {
				if (hidden) {
					setState("show?");
				} else {
					setState("hide?");
				}
			}
		}

		@Override
		public void mouseExited(java.awt.event.MouseEvent arg0) {
			wasInsidePanel = false;
			if (hidden) {
				setState("hidden");
			} else {
				setState("shown");
			}
		}

		@Override
		public void mousePressed(java.awt.event.MouseEvent arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseReleased(java.awt.event.MouseEvent arg0) {
			// TODO Auto-generated method stub

		}
	}

	public class TextAreaMouseListner extends ImagePanelMouseListner
			implements
				MouseListener {

		@Override
		public void mouseClicked(java.awt.event.MouseEvent arg0) {
			hidden = !hidden;
			if (hidden) {
				setState("hidden");
			} else {
				setState("shown");
			}
		}

		@Override
		public void mouseEntered(java.awt.event.MouseEvent arg0) {
			setState("show?");
			if (hidden) {
				setState("show?");
			} else {
				setState("hide?");
			}
		}

		@Override
		public void mouseExited(java.awt.event.MouseEvent arg0) {
			wasInsidePanel = true;
		}

		@Override
		public void mousePressed(java.awt.event.MouseEvent arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseReleased(java.awt.event.MouseEvent arg0) {
			// TODO Auto-generated method stub

		}
	}
}
