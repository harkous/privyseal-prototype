package guidesign;

import java.awt.EventQueue;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JTextField;

import java.awt.Color;

import javax.swing.SwingConstants;

import java.awt.FlowLayout;
import java.awt.Component;

import javax.swing.Box;

import java.awt.Dimension;

import javax.swing.BoxLayout;

import de.uni_siegen.wineme.come_in.thumbnailer.FileDoesNotExistException;
import de.uni_siegen.wineme.come_in.thumbnailer.ThumbnailerException;
import eu.hansolo.steelseries.gauges.Radial1Vertical;
import eu.hansolo.steelseries.tools.BackgroundColor;
import eu.hansolo.steelseries.tools.FrameDesign;
import eu.hansolo.steelseries.tools.GaugeType;
import eu.hansolo.steelseries.tools.LedColor;
import eu.hansolo.steelseries.tools.Model;
import eu.hansolo.steelseries.tools.Orientation;

import javax.swing.JTextPane;
import javax.swing.JSeparator;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;

import javax.swing.JScrollPane;

import java.awt.Container;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.TexturePaint;
import java.awt.Toolkit;

import javax.swing.ActionMap;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.InputMap;
import javax.swing.JFileChooser;
import javax.swing.JTextArea;
import javax.swing.JWindow;
import javax.swing.KeyStroke;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.AbstractAction;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JMenu;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;
import javax.swing.JToolTip;
import javax.swing.JViewport;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.plaf.ColorUIResource;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import java.awt.SystemColor;

import eu.hansolo.steelseries.tools.KnobStyle;
import eu.hansolo.steelseries.tools.FrameType;
import eu.hansolo.steelseries.tools.FrameEffect;
import eu.hansolo.steelseries.tools.ForegroundType;
import eu.hansolo.steelseries.tools.KnobType;
import eu.hansolo.steelseries.tools.Direction;
import eu.hansolo.steelseries.tools.PointerType;
import eu.hansolo.steelseries.tools.Section;
import eu.hansolo.steelseries.tools.TicklabelOrientation;
import eu.hansolo.steelseries.tools.TickmarkType;

import java.awt.CardLayout;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.ScrollPaneConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.ss.formula.ptg.AddPtg;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import syncecncrypt.SyncManager;
import contenthiding.ContentHidingModel;
import contenthiding.DocSummary;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

import java.awt.Font;

import javax.swing.border.MatteBorder;
import javax.swing.border.BevelBorder;

import metadata.MetadataManager;
import metadata.MetadataManager.ManagedMetadata;
import net.miginfocom.swing.MigLayout;

import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;

import guidesign.GuiParameters.*;

public class MainWindow {

	private JFrame frame;
	private JTextArea txtHelp;
	private JTextField txtPath;
	private JPanel panelCenterSelect;
	private JPanel panelCenterRisk;
	private JPanel panelCenterMeta;
	private JPanel panelCenterEncryptDoc;
	private JPanel panelCenterEncryptPic;
	private JPanel panelNorth;
	private JPanel panelSouth;
	private JPanel panelWest;
	private JPanel panelEast;
	private JPanel panelSelect;
	private JPanel panel_no_selection;
	private ArrayList<JPanel> centerPanelList;
	private ArrayList<JRadioButton> menuPointList;
	// private ArrayList<JPanel> panelProviderList;
	private JRadioButton radbtnEditProp;
	private JRadioButton radbtnSelectFile;
	private JRadioButton radbtnEncrypt;
	private JRadioButton radbtnRisk;
	private ArrayList<ServiceCheckBox> cloudServices;
	private String strTextHelp;
	private JPanel panelMetadataExtraButtons;
	private ContentHidingModel contentHidingModel;
	private MetadataManager metadataManager;
	private JComboBox<String> resolutionDoc;
	private JComboBox<String> resolutionPic;
	private JButton btnAddNewProvider;
	private JButton btnAnalyzeRisk;
	private JButton btnUpload;
	private ImageBlurringJDialog imageBlurDialog;
	private static Boolean nativeLookAndFeel = false;
	private ThumbnailPreview thumbnailPreview;
	private JTextPane txtDragYourComputer;
	private String privySealRootDir;

	private JTextField textField;
	private JTextField txtNoFileSelected;
	private ImagePanel panelImgTopRight;
	private ImagePanel panelImgBottomRight;
	private ImagePanel panelImgTopLeft;
	private ImagePanel panelImgBottomLeft;

	private ChangingImageLabel lblSummaryDoc;
	private ChangingImageLabel lblEncryptionDoc;
	private ChangingImageLabel lblThumbnailDoc;
	private ChangingImageLabel lblEncryptionPic;
	private ChangingImageLabel lblThumbnailPic;
	private ChangingImageLabel lblBlurredPic;

	private JLabel lblRecommendationDoc;
	private JLabel lblRecommendationPic;
	private JButton btnRemoveAll;
	private JButton btnPreviewSummary;
	private JButton btnPreviewThumbnailDoc;
	private boolean menusEnabled = false; // boolean to enable the other menus
	Boolean encryptionEnabledByDefault = false;

	JButton btnPicPreview;
	JButton btnBlurredPicPreview;

	// indicates that we have a new file analyzed
	private Boolean newFileAnalyzed = false;
	// indicates the currently shown panel
	private JPanel currentPanel;
	// private JTextField txtFile;
	// after analysis is done

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {

				try {
					if (nativeLookAndFeel) {

						GuiParameters.bigButtonFrColor = Color.black;// Color.red;
						GuiParameters.bigButtonBgColor = GuiParameters.lightGray;// Color.red;
						UIManager.setLookAndFeel(UIManager
								.getSystemLookAndFeelClassName());
					} else {
						GuiParameters.bigButtonFrColor = GuiParameters.lightGray;// Color.red;
						GuiParameters.bigButtonBgColor = GuiParameters.orange;// Color.red;
						UIManager.setLookAndFeel(UIManager
								.getCrossPlatformLookAndFeelClassName());

					}
					// UIManager.setLookAndFeel(UIManager
					// .getSystemLookAndFeelClassName());
					// UIManager.setLookAndFeel(UIManager
					// .getCrossPlatformLookAndFeelClassName());
					MainWindow window = new MainWindow();
					window.frame.setVisible(true);
					// SplashWindow w1 = new SplashWindow(window.frame, 500);
					// w1.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * 
	 * @throws IOException
	 */
	public MainWindow() throws IOException {
		contentHidingModel = ContentHidingModel.getInstance();
		metadataManager = new MetadataManager();
		initialize();
		imageBlurDialog = new ImageBlurringJDialog(frame, new File(
				txtPath.getText()));
		thumbnailPreview = new ThumbnailPreview(frame, contentHidingModel);
	}

	/**
	 * Initialize the contents of the frame.
	 * 
	 * @throws IOException
	 */
	private void initialize() throws IOException {

		frame = new JFrame();
		frame.setBackground(GuiParameters.darkBlue);
		frame.getContentPane().setBackground(GuiParameters.darkBlue);
		frame.setBounds(10, 10, 1079, 702);
		// frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		frame.setResizable(false);

		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent event) {
				exitProcedure();
			}
		});

		panelNorth = new JPanel();
		// panelNorth = new BackgroundPanel(
		// javax.imageio.ImageIO.read(MainWindow.class
		// .getResource("/img/header.png")),
		// BackgroundPanel.SCALED);
		panelNorth.setBorder(new EmptyBorder(5, 0, 0, 5));
		panelNorth.setBackground(GuiParameters.headerColor);
		frame.getContentPane().add(panelNorth, BorderLayout.NORTH);

		JLabel lblLogo = new JLabel("PrivySeal");
		lblLogo.setForeground(GuiParameters.darkBlue);

		lblLogo.setIcon(new ImageIcon(MainWindow.class
				.getResource("/img/privyseal_enhanced_s.png")));
		lblLogo.setFont(GuiParameters.titleFont);

		GroupLayout gl_panelNorth = new GroupLayout(panelNorth);
		gl_panelNorth.setHorizontalGroup(gl_panelNorth.createParallelGroup(
				Alignment.LEADING).addGroup(
				gl_panelNorth.createSequentialGroup().addGap(350)
						.addComponent(lblLogo)
						.addContainerGap(467, Short.MAX_VALUE)));
		gl_panelNorth.setVerticalGroup(gl_panelNorth.createParallelGroup(
				Alignment.TRAILING).addComponent(lblLogo,
				GroupLayout.DEFAULT_SIZE, 92, Short.MAX_VALUE));

		panelNorth.setLayout(gl_panelNorth);

		Component horizontalGlue_1 = Box.createHorizontalGlue();
		panelNorth.add(horizontalGlue_1);

		panelWest = new JPanel();
		panelWest.setBorder(new EmptyBorder(5, 0, 0, 5));
		panelWest.setBackground(GuiParameters.panelBackgroundColor);
		frame.getContentPane().add(panelWest, BorderLayout.WEST);

		// panelEast = new BackgroundPanel(
		// javax.imageio.ImageIO.read(MainWindow.class
		// .getResource("/img/meterBackground_red.png")),
		// BackgroundPanel.TILED);
		panelEast = new JPanel();
		panelEast.setBorder(new EmptyBorder(5, 0, 0, 5));
		panelEast.setBackground(GuiParameters.panelBackgroundColor);
		frame.getContentPane().add(panelEast, BorderLayout.EAST);
		panelSouth = new JPanel();
		panelSouth.setBorder(new EmptyBorder(5, 0, 5, 0));
		panelSouth.setBackground(GuiParameters.panelBackgroundColor);
		frame.getContentPane().add(panelSouth, BorderLayout.SOUTH);

		JButton btnHideHelp = new JButton("Hide Help");
		btnHideHelp.setBackground(GuiParameters.lightGray);
		btnHideHelp.setFont(GuiParameters.menuFont);
		btnHideHelp.setText("Show Help");
		btnHideHelp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JButton btn = (JButton) arg0.getSource();
				if (txtHelp.isVisible()) {
					btn.setText("Show Help");
				} else {
					btn.setText("Hide Help");
					txtHelp.setText(strTextHelp);
				}

				txtHelp.setVisible(!txtHelp.isVisible());

			}
		});

		JPanel panel_1 = new JPanel();
		panel_1.setBackground(GuiParameters.panelBackgroundColor);

		JPanel panel = new JPanel();
		panel.setBackground(GuiParameters.panelBackgroundColor);

		btnAnalyzeRisk = new JButton(" Analyze ");
		// btnAnalyzeRisk.setIcon(new ImageIcon(MainWindow.class
		// .getResource("/img/Tachometer_white_s.png")));
		btnAnalyzeRisk.setBackground(GuiParameters.bigButtonBgColor);
		btnAnalyzeRisk.setForeground(GuiParameters.bigButtonFrColor);
		btnAnalyzeRisk.setFont(GuiParameters.highlightedButtonFont);

		// Border border = new LineBorder(GuiParameters.darkBlue, 1);
		// btnAnalyzeRisk.setBorder(border);
		panel.add(btnAnalyzeRisk);
		btnAnalyzeRisk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ServiceCheckBox serviceSelected = null;
				for (ServiceCheckBox scb : cloudServices) {
					if (scb.isSelected()) {
						serviceSelected = scb;
						break;
					}
				}
				if (serviceSelected != null && !txtPath.getText().equals("")) {
					// Extra handler for default services
					// Folders have to be set up when the user clicks on the
					// provider for the first time
					if (serviceSelected.getServiceFolder().equals("")
							|| serviceSelected.getPrivySealFolder().equals("")) {
						AddNewService newService = new AddNewService(frame,
								serviceSelected.getName(), serviceSelected
										.getIconPath(), serviceSelected
										.getServiceFolder(), serviceSelected
										.getPrivySealFolder(), privySealRootDir);
						// If OK button was pressed
						if (newService.getChoice() == 0) {
							serviceSelected.setName(newService.getServiceName());
							serviceSelected.setIconPath(newService
									.getServiceLogo());
							serviceSelected.setServiceFolder(newService
									.getServiceFolder());
							serviceSelected.setPrivySealFolder(newService
									.getServicePrivySealFolder());
							serviceSelected.setSyncManager(new SyncManager(
									new File(
											privySealRootDir
													+ File.separator
													+ newService
															.getServicePrivySealFolder()
													+ File.separator
													+ "settings.xml")));
						}
					}
					System.out.println("Analyze pressed");
					menusEnabled = true;
				} else if (txtPath.getText().equals("")) {
					JOptionPane.showMessageDialog(frame,
							"Please select the file you would like to share.",
							"No file selected", JOptionPane.WARNING_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(frame,
							"Please select a service provider",
							"No service provider selected",
							JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		// logo_privyseal
		JButton btnStats = new JButton("My Privacy Stats");
		btnStats.setIcon(new ImageIcon(MainWindow.class
				.getResource("/img/stats_s.png")));
		btnStats.setBackground(GuiParameters.minorButtonBgColor);
		btnStats.setForeground(GuiParameters.minorButtonFrColor);
		btnStats.setFocusable(false);
		btnStats.setFont(GuiParameters.menuFont);
		GroupLayout gl_panelSouth = new GroupLayout(panelSouth);
		gl_panelSouth
				.setHorizontalGroup(gl_panelSouth
						.createParallelGroup(Alignment.TRAILING)
						.addGroup(
								gl_panelSouth
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												gl_panelSouth
														.createParallelGroup(
																Alignment.TRAILING)
														.addGroup(
																gl_panelSouth
																		.createSequentialGroup()
																		.addComponent(
																				btnHideHelp)
																		.addGap(59)
																		.addComponent(
																				panel,
																				GroupLayout.PREFERRED_SIZE,
																				598,
																				GroupLayout.PREFERRED_SIZE)
																		.addGap(48)
																		.addComponent(
																				btnStats)
																		.addGap(148))
														.addGroup(
																gl_panelSouth
																		.createSequentialGroup()
																		.addComponent(
																				panel_1,
																				GroupLayout.DEFAULT_SIZE,
																				1112,
																				Short.MAX_VALUE)
																		.addGap(43)))));
		gl_panelSouth
				.setVerticalGroup(gl_panelSouth
						.createParallelGroup(Alignment.TRAILING)
						.addGroup(
								gl_panelSouth
										.createSequentialGroup()
										.addGap(30)
										.addGroup(
												gl_panelSouth
														.createParallelGroup(
																Alignment.TRAILING)
														.addComponent(
																btnHideHelp)
														.addGroup(
																gl_panelSouth
																		.createParallelGroup(
																				Alignment.LEADING)
																		.addGroup(
																				gl_panelSouth
																						.createSequentialGroup()
																						.addComponent(
																								btnStats)
																						.addGap(6))
																		.addComponent(
																				panel,
																				GroupLayout.PREFERRED_SIZE,
																				GroupLayout.DEFAULT_SIZE,
																				GroupLayout.PREFERRED_SIZE)))
										.addPreferredGap(
												ComponentPlacement.RELATED)
										.addComponent(panel_1,
												GroupLayout.PREFERRED_SIZE, 50,
												GroupLayout.PREFERRED_SIZE)));

		btnUpload = new JButton("   Upload  ");
		btnUpload.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Check what options are set

				File srcFile = new File(txtPath.getText());
				String serviceFolder = null;
				for (ServiceCheckBox scb : cloudServices) {
					if (scb.isSelected()) {
						serviceFolder = scb.getPrivySealFolder();
						break;
					}
				}

				if (lblEncryptionDoc.getState().equals("on")
						|| lblEncryptionPic.getState().equals("on")) {
					// Add file in XML to encrypt
					try {
						String filepath = privySealRootDir + File.separator
								+ serviceFolder + File.separator
								+ "settings.xml";
						DocumentBuilderFactory docFactory = DocumentBuilderFactory
								.newInstance();
						DocumentBuilder docBuilder;
						docBuilder = docFactory.newDocumentBuilder();
						Document doc = docBuilder.parse(filepath);

						// Get the root element
						Node jfs = doc.getFirstChild();

						// Get encryptList element if exists
						Node encryptList = doc.getElementsByTagName(
								"encryptList").item(0);
						if (encryptList == null) {
							Element encList = doc.createElement("encryptList");
							jfs.appendChild(encList);
						}

						encryptList = doc.getElementsByTagName("encryptList")
								.item(0);

						boolean add = true;
						NodeList fileList = encryptList.getChildNodes();
						for (int i = 0; i < fileList.getLength(); i++) {
							Node n = fileList.item(i);
							if (n.getTextContent().equals(srcFile.getName())) {
								add = false;
								break;
							}
						}

						// Create file entry if necessary
						if (add) {
							Element file = doc.createElement("file");
							file.setTextContent(srcFile.getName());
							encryptList.appendChild(file);
						}

						// write the content into xml file
						TransformerFactory transformerFactory = TransformerFactory
								.newInstance();
						transformerFactory.setAttribute("indent-number", 4); // doesn't
						// work...
						Transformer transformer = transformerFactory
								.newTransformer();
						transformer.setOutputProperty(OutputKeys.INDENT, "yes");
						DOMSource source = new DOMSource(doc);
						StreamResult result = new StreamResult(new File(
								filepath));

						// Output to console for testing
						// StreamResult result = new StreamResult(System.out);

						transformer.transform(source, result);

					} catch (ParserConfigurationException | SAXException
							| IOException | TransformerException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				if (lblSummaryDoc.getState().equals("on")) {
					// Create summary if it hasn't been created before
					if (contentHidingModel.getSummary() == null) {
						try {
							contentHidingModel.createSummary(new File(txtPath
									.getText()));
						} catch (IOException e) {
							JOptionPane
									.showMessageDialog(
											frame,
											"Can't find document to summarize. Check if a file is selected in the 'Select file' menu.",
											"No document found",
											JOptionPane.ERROR_MESSAGE);
						}
					}
					// Write summary on disk as txt
					try {
						FileUtils.writeStringToFile(
								new File(privySealRootDir
										+ File.separator
										+ serviceFolder
										+ File.separator
										+ FilenameUtils.removeExtension(srcFile
												.getName()) + "_summary.txt"),
								contentHidingModel.getSummary());
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				if (lblThumbnailDoc.getState().equals("on")) {
					// Create thumbnail of doc if it hasn't been created
					File[] thumbnails = thumbnailPreview.getThumbFiles();
					// If the thumbnail for the selected resolution doesn't
					// exist
					int selectedIndex = resolutionDoc.getSelectedIndex();
					if (thumbnails[selectedIndex] == null) {
						try {
							// Generate thumbnail
							int[][] resoultion = contentHidingModel
									.generateDocResolutions();
							File newThumb = contentHidingModel
									.generateThumbnail(
											new File(txtPath.getText()),
											resoultion[selectedIndex][0],
											resoultion[selectedIndex][1]);
							// Save it in array for further usage
							thumbnailPreview.setThumbFile(newThumb,
									selectedIndex);
						} catch (Exception e) {

							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					// Write doc thumbnail on disk
					try {
						FileUtils
								.copyFile(
										thumbnails[selectedIndex],
										new File(
												privySealRootDir
														+ File.separator
														+ serviceFolder
														+ File.separator
														+ FilenameUtils
																.removeExtension(srcFile
																		.getName())
														+ "_thumbnail.png"));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					// Delete generated thumbnails
					int i = 0;
					for (File f : thumbnails) {
						if (f != null) {
							f.delete();
							f = null;
							thumbnailPreview.setThumbFile(f, i);
							i++;
						}
					}
				}

				if (lblThumbnailPic.getState().equals("on")) {
					// Create thumbnail of pic if it hasn't been created
					File[] thumbnails = thumbnailPreview.getThumbFiles();
					// If the thumbnail for the selected resolution doesn't
					// exist
					int selectedIndex = resolutionPic.getSelectedIndex();
					if (thumbnails[selectedIndex] == null) {
						try {
							// Generate thumbnail
							int[][] resoultion = contentHidingModel
									.generateImageResolutions(txtPath.getText());
							File newThumb = contentHidingModel
									.generateThumbnail(
											new File(txtPath.getText()),
											resoultion[selectedIndex][0],
											resoultion[selectedIndex][1]);
							// Save it in array for further usage
							thumbnailPreview.setThumbFile(newThumb,
									selectedIndex);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					// Write pic thumbnail on disk
					try {
						FileUtils
								.copyFile(
										thumbnails[selectedIndex],
										new File(
												privySealRootDir
														+ File.separator
														+ serviceFolder
														+ File.separator
														+ FilenameUtils
																.removeExtension(srcFile
																		.getName())
														+ "_thumbnail.png"));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					// Delete generated thumbnails
					int i = 0;
					for (File f : thumbnails) {
						if (f != null) {
							f.delete();
							f = null;
							thumbnailPreview.setThumbFile(f, i);
							i++;
						}
					}
				}

				if (lblBlurredPic.getState().equals("on")) {
					// Do not generate because output image is available from
					// image blur dialog
					// Write blurred pic on disk
					try {
						ImageIO.write(
								imageBlurDialog.getOutputImage(),
								"png",
								new File(privySealRootDir
										+ File.separator
										+ serviceFolder
										+ File.separator
										+ FilenameUtils.removeExtension(srcFile
												.getName()) + "_blurred.png"));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				// Collecting what to show
				ArrayList<ManagedMetadata> metadataToShow = new ArrayList<>();

				if (panelImgTopLeft.getState().equals("shown")) {
					metadataToShow.add(panelImgTopLeft.getPropertyType());
				}
				if (panelImgTopRight.getState().equals("shown")) {
					metadataToShow.add(panelImgTopRight.getPropertyType());
				}
				if (panelImgBottomLeft.getState().equals("shown")) {
					metadataToShow.add(panelImgBottomLeft.getPropertyType());
				}
				if (panelImgBottomRight.getState().equals("shown")) {
					metadataToShow.add(panelImgBottomRight.getPropertyType());
				}
				// Write chosen file to disk
				metadataManager.writeFile(srcFile, privySealRootDir
						+ File.separator + serviceFolder + File.separator
						+ srcFile.getName(), metadataToShow);

				// TODO: Add progress window
			}
		});
		panel.add(btnUpload);
		btnUpload.setAlignmentX(Component.CENTER_ALIGNMENT);
		// btnUpload.setIcon(new ImageIcon(MainWindow.class
		// .getResource("/img/upload_white_s.png")));
		btnUpload.setFocusable(false);

		btnUpload.setVisible(false);
		btnUpload.setBackground(GuiParameters.bigButtonBgColor);
		btnUpload.setForeground(GuiParameters.bigButtonFrColor);
		btnUpload.setFont(GuiParameters.highlightedButtonFont);

		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.Y_AXIS));

		txtHelp = new JTextArea();
		txtHelp.setBackground(GuiParameters.panelBackgroundColor);
		panel_1.add(txtHelp);
		strTextHelp = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard  dummy text ever since the 1500s, \n"
				+ "when an unknown printer took a galley of type and scrambled it to make a type specimen book.It has survived not only five centuries, but also the leap into\n"
				+ " electronic typesetting, remaining essentially unchanged.";
		txtHelp.setText(strTextHelp);
		txtHelp.setEditable(false);
		panelSouth.setLayout(gl_panelSouth);

		txtHelp.setColumns(15);
		txtHelp.setRows(5);
		txtHelp.setFont(GuiParameters.helpTextFont);
		txtHelp.setBackground(GuiParameters.panelBackgroundColor);
		txtHelp.setForeground(GuiParameters.lightGray);
		txtHelp.setWrapStyleWord(true);
		txtHelp.setVisible(false);
		JPanel panelCenter = new JPanel();
		frame.getContentPane().add(panelCenter, BorderLayout.CENTER);
		panelCenter.setLayout(new CardLayout(0, 0));

		final JPanel panel_no_selection = new JPanel();
		panel_no_selection.setBorder(new EmptyBorder(5, 0, 0, 5));
		panel_no_selection.setBackground(GuiParameters.darkBlue);
		panelCenter.add(panel_no_selection, "name_141559544324");
		txtNoFileSelected = new JTextField() {
			public void setBorder(Border border) {
			}
		};
		txtNoFileSelected.setText("No File Selected!");
		txtNoFileSelected.setFont(new Font("Lucida Fax", Font.BOLD, 16));
		txtNoFileSelected.setEditable(false);
		txtNoFileSelected.setColumns(10);
		txtNoFileSelected.setBackground(GuiParameters.darkBlue);
		txtNoFileSelected.setForeground(GuiParameters.lightGray);

		JPanel panel_8 = new JPanel();
		panel_8.setBackground(GuiParameters.darkBlue);
		panel_8.setForeground(GuiParameters.darkBlue);
		GroupLayout gl_panel_no_selection = new GroupLayout(panel_no_selection);
		gl_panel_no_selection
				.setHorizontalGroup(gl_panel_no_selection
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_panel_no_selection
										.createSequentialGroup()
										.addGroup(
												gl_panel_no_selection
														.createParallelGroup(
																Alignment.LEADING)
														.addComponent(
																txtNoFileSelected,
																GroupLayout.PREFERRED_SIZE,
																684,
																GroupLayout.PREFERRED_SIZE)
														.addComponent(
																panel_8,
																GroupLayout.PREFERRED_SIZE,
																653,
																GroupLayout.PREFERRED_SIZE))
										.addContainerGap(
												GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)));
		gl_panel_no_selection.setVerticalGroup(gl_panel_no_selection
				.createParallelGroup(Alignment.LEADING).addGroup(
						gl_panel_no_selection
								.createSequentialGroup()
								.addComponent(txtNoFileSelected,
										GroupLayout.PREFERRED_SIZE, 40,
										GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED,
										GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE)
								.addComponent(panel_8,
										GroupLayout.PREFERRED_SIZE, 353,
										GroupLayout.PREFERRED_SIZE)
								.addContainerGap()));

		JLabel lblSelectFile = new JLabel("");
		lblSelectFile.setHorizontalAlignment(SwingConstants.RIGHT);
		lblSelectFile.setVerticalAlignment(SwingConstants.BOTTOM);
		lblSelectFile.setIcon(new ImageIcon(MainWindow.class
				.getResource("/img/document_b.png")));

		JLabel lblSelectFile_1 = new JLabel("Select a file and then come back!");
		lblSelectFile_1.setFont(new Font("Lucida Fax", Font.BOLD, 18));
		lblSelectFile_1
				.setForeground(GuiParameters.radioButtonUnselectedTxtColor);
		GroupLayout gl_panel_8 = new GroupLayout(panel_8);
		gl_panel_8
				.setHorizontalGroup(gl_panel_8
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_panel_8
										.createSequentialGroup()
										.addGroup(
												gl_panel_8
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																gl_panel_8
																		.createSequentialGroup()
																		.addGap(192)
																		.addComponent(
																				lblSelectFile))
														.addGroup(
																gl_panel_8
																		.createSequentialGroup()
																		.addGap(159)
																		.addComponent(
																				lblSelectFile_1,
																				GroupLayout.PREFERRED_SIZE,
																				329,
																				GroupLayout.PREFERRED_SIZE)))
										.addContainerGap(165, Short.MAX_VALUE)));
		gl_panel_8.setVerticalGroup(gl_panel_8.createParallelGroup(
				Alignment.LEADING).addGroup(
				gl_panel_8.createSequentialGroup().addGap(21)
						.addComponent(lblSelectFile)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(lblSelectFile_1)
						.addContainerGap(48, Short.MAX_VALUE)));
		panel_8.setLayout(gl_panel_8);
		panel_no_selection.setLayout(gl_panel_no_selection);

		panelCenterSelect = new JPanel();// new BackgroundPanel(
		// javax.imageio.ImageIO.read(MainWindow.class
		// .getResource("/img/rounded_rectangle.png")),
		// BackgroundPanel.SCALED);
		panelCenterSelect.setBackground(GuiParameters.panelBackgroundColor);
		panelCenter.add(panelCenterSelect, "name_141559517593139");
		btnAnalyzeRisk.setVisible(true);

		JPanel panel_5 = new JPanel();
		panel_5.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null,
				null, null));
		panel_5.setBackground(GuiParameters.darkBlue);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		scrollPane
				.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBorder(null);
		scrollPane.setBackground(GuiParameters.darkBlue);
		scrollPane.setViewportBorder(null);
		scrollPane.getViewportBorder();

		panelSelect = new JPanel();
		panelSelect.setBackground(GuiParameters.panelBackgroundColor);
		panelSelect.setBorder(null);
		scrollPane.setViewportView(panelSelect);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{200, 200, 200, 0};
		gbl_panel.rowHeights = new int[]{0, 163, 163, 0};
		gbl_panel.columnWeights = new double[]{1.0, 1.0, 1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 1.0, 1.0, Double.MIN_VALUE};
		panelSelect.setLayout(gbl_panel);

		InputMap im = panelSelect.getInputMap(JPanel.WHEN_IN_FOCUSED_WINDOW);
		ActionMap am = panelSelect.getActionMap();

		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "DeleteButton");
		am.put("DeleteButton", new AbstractAction() {

			@Override
			public void actionPerformed(ActionEvent e) {
				for (ServiceCheckBox scb : cloudServices) {
					if (scb.isSelected()) {
						int res = JOptionPane
								.showOptionDialog(
										frame,
										"Are you sure you want to delete the service provider selected?",
										"Delete service provider",
										JOptionPane.YES_NO_OPTION,
										JOptionPane.QUESTION_MESSAGE, null,
										null, 0);
						// If answer is yes
						if (res == 0) {
							Container parentPanel = scb.getParent();
							panelSelect.remove(parentPanel);
							cloudServices.remove(scb);
							break;
						}
					}
				}

				panelSelect.removeAll();

				int index = 0;
				for (ServiceCheckBox scb : cloudServices) {
					Container parentPanel = scb.getParent();
					GridBagConstraints gbc_panel = new GridBagConstraints();
					gbc_panel.fill = GridBagConstraints.NONE;
					gbc_panel.gridx = index % 3;
					gbc_panel.gridy = index / 3;
					panelSelect.add(parentPanel, gbc_panel);
					index++;
				}

				GridBagConstraints gbc_panelAddNewProv = new GridBagConstraints();
				gbc_panelAddNewProv.fill = GridBagConstraints.NONE;
				gbc_panelAddNewProv.gridx = (cloudServices.size()) % 3;
				gbc_panelAddNewProv.gridy = (cloudServices.size()) / 3;
				// panelSelect.add(panelAddnewProvider, gbc_panelAddNewProv);

				frame.validate();
				frame.repaint();

			}
		});
		panel_5.setLayout(new BoxLayout(panel_5, BoxLayout.X_AXIS));

		JLabel lblPath = new JLabel("Path:  ");
		lblPath.setBackground(GuiParameters.darkBlue);
		panel_5.add(lblPath);
		lblPath.setForeground(GuiParameters.lightGray);
		lblPath.setFont(GuiParameters.menuFont);
		txtPath = new JTextField();
		txtPath.setColumns(40);
		txtPath.setBackground(GuiParameters.lightGray);
		txtPath.setMaximumSize(new Dimension(frame.getWidth(), 25));
		txtPath.setDropTarget(new DropTarget() {
			public synchronized void drop(DropTargetDropEvent evt) {
				try {
					evt.acceptDrop(DnDConstants.ACTION_COPY);
					List<File> dropppedFiles = (List<File>) evt
							.getTransferable().getTransferData(
									DataFlavor.javaFileListFlavor);
					for (File file : dropppedFiles) {
						txtPath.setText(file.getAbsolutePath());
					}
				} catch (UnsupportedFlavorException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		});

		// In order to regenarate summary when a new file is set
		txtPath.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
				if (contentHidingModel.getSummary() != null) {
					contentHidingModel.setSummary(null);
				}
			}

			public void removeUpdate(DocumentEvent e) {
				if (contentHidingModel.getSummary() != null) {
					contentHidingModel.setSummary(null);
				}
			}

			public void insertUpdate(DocumentEvent e) {
				if (contentHidingModel.getSummary() != null) {
					contentHidingModel.setSummary(null);
				}
			}
		});
		panel_5.add(txtPath);

		JButton btnBrowse = new JButton("Browse...");
		btnBrowse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					LookAndFeel swingLF = UIManager.getLookAndFeel();
					UIManager.setLookAndFeel(UIManager
							.getSystemLookAndFeelClassName());
					JFileChooser selectFile = new JFileChooser();
					UIManager.setLookAndFeel(swingLF);
					int returnVal = selectFile.showOpenDialog(frame);
					if (returnVal == JFileChooser.APPROVE_OPTION) {
						txtPath.setText(selectFile.getSelectedFile()
								.getAbsolutePath());
					}
				} catch (ClassNotFoundException | InstantiationException
						| IllegalAccessException
						| UnsupportedLookAndFeelException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnBrowse.setBackground(GuiParameters.minorButtonBgColor);
		btnBrowse.setForeground(GuiParameters.minorButtonFrColor);
		btnBrowse.setFocusable(false);
		btnBrowse.setFont(GuiParameters.helpTextFont);
		panel_5.add(btnBrowse);
		panelCenterSelect.setLayout(new BoxLayout(panelCenterSelect,
				BoxLayout.Y_AXIS));

		JPanel panel_2 = new JPanel();
		panel_2.setBackground(GuiParameters.panelBackgroundColor);// Color.white
		panelCenterSelect.add(panel_2);

		txtDragYourComputer = new JTextPane() {

			@Override
			public void setBorder(Border border) {

				// No!

			}

		};
		txtDragYourComputer.setEditable(false);
		txtDragYourComputer
				.setText("Drag your file to a cloud provider\nOR select the provider and browse to the file path.");
		txtDragYourComputer.setFont(GuiParameters.tipTextFont);
		txtDragYourComputer.setBackground(GuiParameters.panelBackgroundColor);// Color.WHITE
		txtDragYourComputer.setForeground(GuiParameters.lightGray);

		// StyledDocument doc = txtDragYourComputer.getStyledDocument();
		// SimpleAttributeSet center = new SimpleAttributeSet();
		// StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);
		//
		// doc.setParagraphAttributes(0, doc.getLength(), center, false);
		btnAddNewProvider = new JButton("Add Provider");
		btnAddNewProvider.setVerticalAlignment(SwingConstants.BOTTOM);
		// btnAddNewProvider.setVerticalTextPosition(SwingConstants.BOTTOM);
		// btnAddNewProvider.setHorizontalTextPosition(SwingConstants.CENTER);

		btnAddNewProvider.setIcon(new ImageIcon(MainWindow.class
				.getResource("/img/cloud_add.png")));
		btnAddNewProvider.setBackground(GuiParameters.minorButtonBgColor);
		btnAddNewProvider.setForeground(GuiParameters.minorButtonFrColor);
		btnAddNewProvider.setFocusable(false);
		btnAddNewProvider.setFont(GuiParameters.menuFont);
		GroupLayout gl_panel_2 = new GroupLayout(panel_2);
		gl_panel_2.setHorizontalGroup(gl_panel_2.createParallelGroup(
				Alignment.LEADING).addGroup(
				gl_panel_2
						.createSequentialGroup()
						.addComponent(txtDragYourComputer,
								GroupLayout.PREFERRED_SIZE, 447,
								GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(btnAddNewProvider).addGap(55)));
		gl_panel_2.setVerticalGroup(gl_panel_2.createParallelGroup(
				Alignment.LEADING).addGroup(
				gl_panel_2
						.createSequentialGroup()
						.addContainerGap()
						.addGroup(
								gl_panel_2
										.createParallelGroup(Alignment.LEADING)
										.addComponent(btnAddNewProvider)
										.addComponent(txtDragYourComputer,
												GroupLayout.PREFERRED_SIZE, 47,
												GroupLayout.PREFERRED_SIZE))
						.addContainerGap()));
		panel_2.setLayout(gl_panel_2);
		btnAddNewProvider.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AddNewService newService = new AddNewService(frame,
						privySealRootDir);
				// If OK button was pressed
				if (newService.getChoice() == 0) {
					final ServiceCheckBox serChkbx = new ServiceCheckBox(
							newService.getServiceName(), newService
									.getServiceLogo(), newService
									.getServiceFolder(), newService
									.getServicePrivySealFolder(), false);
					serChkbx.addActionListener(new ActionListener() {

						@Override
						public void actionPerformed(ActionEvent arg0) {
							ServiceCheckBox source = (ServiceCheckBox) arg0
									.getSource();

							for (ServiceCheckBox scb : cloudServices) {
								if (!scb.equals(source)) {
									scb.setSelected(false);
									scb.setBorderPainted(false);
								}

								if (source.isSelected()) {
									source.setBorderPainted(true);
								} else {
									source.setBorderPainted(false);
								}
							}

						}
					});

					serChkbx.setDropTarget(new DropTarget() {
						public synchronized void drop(DropTargetDropEvent evt) {
							try {
								evt.acceptDrop(DnDConstants.ACTION_COPY);
								List<File> dropppedFiles = (List<File>) evt
										.getTransferable().getTransferData(
												DataFlavor.javaFileListFlavor);
								for (File file : dropppedFiles) {
									txtPath.setText(file.getAbsolutePath());
									for (ServiceCheckBox sbc : cloudServices) {
										sbc.setSelected(false);
										sbc.setBorderPainted(false);
									}

									serChkbx.setSelected(true);
									serChkbx.setBorderPainted(true);
								}
							} catch (UnsupportedFlavorException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}

					});

					serChkbx.setAlignmentX(Component.CENTER_ALIGNMENT);
					serChkbx.setHorizontalAlignment(SwingConstants.CENTER);
					serChkbx.setBackground(GuiParameters.darkBlue);
					serChkbx.setBorder(new LineBorder(Color.BLACK, 3));

					cloudServices.add(serChkbx);
					// panelSelect.remove(panelAddnewProvider);

					JPanel jp = new JPanel();
					jp.setBackground(GuiParameters.darkBlue);
					jp.setLayout(new BoxLayout(jp, BoxLayout.Y_AXIS));
					GridBagConstraints gbc_panel = new GridBagConstraints();
					gbc_panel.fill = GridBagConstraints.NONE;
					gbc_panel.gridx = (cloudServices.size() - 1) % 3;
					gbc_panel.gridy = (cloudServices.size() - 1) / 3;
					panelSelect.add(jp, gbc_panel);

					jp.add(serChkbx);

					GridBagConstraints gbc_panelAddNewProv = new GridBagConstraints();
					gbc_panelAddNewProv.fill = GridBagConstraints.NONE;
					gbc_panelAddNewProv.gridx = (cloudServices.size()) % 3;
					gbc_panelAddNewProv.gridy = (cloudServices.size()) / 3;
					// panelSelect.add(panelAddnewProvider,
					// gbc_panelAddNewProv);

					frame.validate();
					frame.repaint();

				}
			}
		});
		btnAddNewProvider.setVisible(true);
		panelCenterSelect.add(scrollPane);
		panelCenterSelect.add(panel_5);

		panelCenterRisk = new JPanel();
		panelCenterRisk.setBorder(new EmptyBorder(5, 0, 0, 5));
		panelCenterRisk.setBackground(GuiParameters.panelBackgroundColor);
		panelCenter.add(panelCenterRisk, "name_141559542438085");

		textField = new JTextField() {
			public void setBorder(Border border) {
			}
		};
		textField.setText("\nDetailed explanation of the risk of sharing\n");
		textField.setFont(new Font("Lucida Fax", Font.BOLD, 16));
		textField.setEditable(false);
		textField.setColumns(10);
		textField.setBackground(GuiParameters.darkBlue);
		textField.setForeground(GuiParameters.lightGray);

		JTextPane textPane_1 = new JTextPane();
		textPane_1
				.setText("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse porta aliquam tristique. Maecenas molestie sollicitudin nulla, ac ornare nunc bibendum ornare. Nullam accumsan nibh quis erat convallis, eu fermentum ipsum sodales. Aliquam commodo adipiscing nibh at tempus. Etiam vitae ultricies magna. Cras quam ipsum, vehicula id egestas at, eleifend ac risus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc viverra dui id ante scelerisque commodo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Maecenas posuere libero quis ante sodales fermentum. Phasellus pretium auctor venenatis. Integer eu lacus placerat, luctus ipsum sit amet, luctus libero.");
		textPane_1.setFont(new Font("Lucida Fax", Font.PLAIN, 14));
		textPane_1.setEditable(false);
		textPane_1.setBackground(GuiParameters.darkBlue);
		textPane_1.setForeground(GuiParameters.lightGray);
		panelCenterMeta = new JPanel();
		panelCenterMeta.setBorder(new EmptyBorder(5, 0, 0, 5));
		panelCenterMeta.setBackground(GuiParameters.panelBackgroundColor);
		panelCenter.add(panelCenterMeta);

		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBorder(null);
		scrollPane_2.setBackground(GuiParameters.darkBlue);

		JPanel panel_3 = new JPanel();

		JPanel panel_9 = new BackgroundPanel(
				javax.imageio.ImageIO.read(MainWindow.class
						.getResource("/img/metadata_bg_s.png")),
				BackgroundPanel.ACTUAL);// JPanel();

		panel_9.setBackground(GuiParameters.darkBlue);

		JPanel panel_6 = new JPanel();
		panel_6.setBackground(GuiParameters.darkBlue);
		panel_6.setLayout(new BoxLayout(panel_6, BoxLayout.X_AXIS));

		panelMetadataExtraButtons = new JPanel();
		panelMetadataExtraButtons.setBackground(GuiParameters.darkBlue);
		// panelMetadataExtraButtons.setVisible(false);
		panelMetadataExtraButtons.setOpaque(false);

		JButton btnAdvanced = new JButton("More Properties");
		btnAdvanced.setAlignmentY(Component.BOTTOM_ALIGNMENT);
		btnAdvanced.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnAdvanced.setVerticalAlignment(SwingConstants.TOP);
		btnAdvanced.setIcon(new ImageIcon(MainWindow.class
				.getResource("/img/settings_s.png")));
		btnAdvanced.setAlignmentX(Component.CENTER_ALIGNMENT);

		btnAdvanced.setBackground(GuiParameters.minorButtonBgColor);
		btnAdvanced.setForeground(GuiParameters.minorButtonFrColor);
		btnAdvanced.setFont(GuiParameters.helpTextFont);

		JButton btnSetRecommended = new JButton("Set recomended settings");
		btnSetRecommended.setIcon(new ImageIcon(MainWindow.class
				.getResource("/img/people_small.png")));
		btnSetRecommended.setAlignmentY(Component.BOTTOM_ALIGNMENT);
		btnSetRecommended.setAlignmentX(Component.CENTER_ALIGNMENT);
		btnSetRecommended.setBackground(GuiParameters.minorButtonBgColor);
		btnSetRecommended.setForeground(GuiParameters.minorButtonFrColor);
		btnSetRecommended.setFont(GuiParameters.helpTextFont);

		btnRemoveAll = new JButton("Hide All Properties");
		btnRemoveAll.setFont(GuiParameters.helpTextFont);
		btnRemoveAll.setIcon(new ImageIcon(MainWindow.class
				.getResource("/img/scissors_s.png")));
		btnRemoveAll.setAlignmentY(Component.BOTTOM_ALIGNMENT);
		btnRemoveAll.setAlignmentX(Component.CENTER_ALIGNMENT);
		btnRemoveAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelImgTopRight.setState("hidden");
				panelImgBottomRight.setState("hidden");
				panelImgTopLeft.setState("hidden");
				panelImgBottomLeft.setState("hidden");
			}
		});
		btnRemoveAll.setBackground(GuiParameters.minorButtonBgColor);
		btnRemoveAll.setForeground(GuiParameters.minorButtonFrColor);
		panelMetadataExtraButtons.setLayout(new MigLayout("",
				"[137px][181px][158px]", "[38px]"));
		panelMetadataExtraButtons.add(btnAdvanced, "cell 0 0,alignx left,grow");
		panelMetadataExtraButtons.add(btnSetRecommended,
				"cell 1 0,alignx left,grow");
		panelMetadataExtraButtons
				.add(btnRemoveAll, "cell 2 0,alignx left,grow");

		panelImgTopRight = new ImagePanel("Title", "");

		panelImgTopRight.setFont(GuiParameters.helpTextFont);

		panelImgBottomRight = new ImagePanel("Author", "");

		panelImgTopLeft = new ImagePanel("Date", "");

		panelImgBottomLeft = new ImagePanel("Modified", "");

		JLabel label_2 = new JLabel("");
		label_2.setHorizontalAlignment(SwingConstants.CENTER);
		label_2.setBackground(GuiParameters.darkBlue);
		label_2.setIcon(new ImageIcon(MainWindow.class
				.getResource("/img/people.png")));

		JLabel label_3 = new JLabel("");
		label_3.setHorizontalAlignment(SwingConstants.CENTER);
		label_3.setBackground(GuiParameters.darkBlue);
		label_3.setIcon(new ImageIcon(MainWindow.class
				.getResource("/img/people.png")));

		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(MainWindow.class
				.getResource("/img/people_small.png")));
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setBackground(GuiParameters.darkBlue);

		JLabel label_1 = new JLabel("");
		label_1.setIcon(new ImageIcon(MainWindow.class
				.getResource("/img/people_small.png")));
		label_1.setHorizontalAlignment(SwingConstants.CENTER);
		label_1.setBackground(GuiParameters.darkBlue);
		GroupLayout gl_panel_9 = new GroupLayout(panel_9);
		gl_panel_9.setHorizontalGroup(
			gl_panel_9.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_9.createSequentialGroup()
					.addGroup(gl_panel_9.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_9.createSequentialGroup()
							.addGap(268)
							.addComponent(panel_6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_9.createSequentialGroup()
							.addGap(63)
							.addGroup(gl_panel_9.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel_9.createSequentialGroup()
									.addComponent(label_1, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
									.addGap(18)
									.addComponent(panelImgTopLeft, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_panel_9.createSequentialGroup()
									.addComponent(label, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(panelImgBottomLeft, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
							.addGap(131)
							.addGroup(gl_panel_9.createParallelGroup(Alignment.TRAILING)
								.addGroup(gl_panel_9.createSequentialGroup()
									.addComponent(panelImgBottomRight, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(label_3, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
									.addGap(123))
								.addGroup(gl_panel_9.createSequentialGroup()
									.addComponent(panelImgTopRight, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(label_2, GroupLayout.PREFERRED_SIZE, 63, GroupLayout.PREFERRED_SIZE)
									.addGap(129))))
						.addComponent(panelMetadataExtraButtons, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_panel_9.setVerticalGroup(
			gl_panel_9.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel_9.createSequentialGroup()
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGroup(gl_panel_9.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_panel_9.createSequentialGroup()
							.addGroup(gl_panel_9.createParallelGroup(Alignment.LEADING)
								.addComponent(panel_6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(panelImgTopLeft, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_panel_9.createSequentialGroup()
									.addGap(34)
									.addComponent(label_2))
								.addComponent(panelImgTopRight, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(64)
							.addGroup(gl_panel_9.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel_9.createSequentialGroup()
									.addGroup(gl_panel_9.createParallelGroup(Alignment.LEADING)
										.addGroup(gl_panel_9.createSequentialGroup()
											.addPreferredGap(ComponentPlacement.RELATED, 48, Short.MAX_VALUE)
											.addComponent(label_3)
											.addGap(45))
										.addComponent(panelImgBottomRight, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
									.addGap(11))
								.addComponent(panelImgBottomLeft, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_panel_9.createSequentialGroup()
							.addGap(34)
							.addComponent(label_1, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, 170, Short.MAX_VALUE)
							.addComponent(label, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
							.addGap(57)))
					.addGap(18)
					.addComponent(panelMetadataExtraButtons, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(19))
		);

		JLabel lblTitle_1 = new JLabel("Title");
		lblTitle_1.setForeground(Color.WHITE);

		// txtFile = new JTextField();
		// txtFile.setHorizontalAlignment(SwingConstants.CENTER);
		// txtFile.setText("File");
		// txtFile.setColumns(10);
		// txtFile.setOpaque(false);
		// txtFile.setBorder(null);
		// GroupLayout gl_panelImgCreationDate = new
		// GroupLayout(panelImgCreationDate);
		// gl_panelImgCreationDate.setHorizontalGroup(
		// gl_panelImgCreationDate.createParallelGroup(Alignment.LEADING)
		// .addGroup(Alignment.TRAILING,
		// gl_panelImgCreationDate.createSequentialGroup()
		// .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
		// .addComponent(txtFile, GroupLayout.PREFERRED_SIZE,
		// GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
		// .addContainerGap())
		// .addGroup(gl_panelImgCreationDate.createSequentialGroup()
		// .addGap(40)
		// .addComponent(lblTitle_1)
		// .addContainerGap(40, Short.MAX_VALUE))
		// );
		// gl_panelImgCreationDate.setVerticalGroup(
		// gl_panelImgCreationDate.createParallelGroup(Alignment.LEADING)
		// .addGroup(gl_panelImgCreationDate.createSequentialGroup()
		// .addContainerGap()
		// .addComponent(lblTitle_1)
		// .addGap(18)
		// .addComponent(txtFile, GroupLayout.PREFERRED_SIZE,
		// GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
		// .addContainerGap(37, Short.MAX_VALUE))
		// );
		// panelImgCreationDate.setLayout(gl_panelImgCreationDate);
		//

		// panelImgTitle.setLayout(new GridLayout(1, 0, 0, 0));
		panel_9.setLayout(gl_panel_9);
		GroupLayout gl_panelCenterMeta = new GroupLayout(panelCenterMeta);
		gl_panelCenterMeta
				.setHorizontalGroup(gl_panelCenterMeta
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_panelCenterMeta
										.createSequentialGroup()
										.addGroup(
												gl_panelCenterMeta
														.createParallelGroup(
																Alignment.LEADING)
														.addComponent(
																scrollPane_2,
																GroupLayout.PREFERRED_SIZE,
																714,
																GroupLayout.PREFERRED_SIZE)
														.addComponent(
																panel_3,
																GroupLayout.PREFERRED_SIZE,
																622,
																GroupLayout.PREFERRED_SIZE)
														.addComponent(
																panel_9,
																GroupLayout.PREFERRED_SIZE,
																623,
																GroupLayout.PREFERRED_SIZE))
										.addContainerGap(
												GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)));
		gl_panelCenterMeta
				.setVerticalGroup(gl_panelCenterMeta.createParallelGroup(
						Alignment.LEADING).addGroup(
						gl_panelCenterMeta
								.createSequentialGroup()
								.addComponent(panel_3,
										GroupLayout.PREFERRED_SIZE, 35,
										GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(panel_9,
										GroupLayout.PREFERRED_SIZE, 348,
										GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(scrollPane_2,
										GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE).addGap(23)));
		panel_3.setLayout(new MigLayout("", "[grow]", "[][grow]"));
		panel_3.setBackground(GuiParameters.panelBackgroundColor);
		JTextPane txtpnSelectTheProperties = new JTextPane() {
			public void setBorder(Border border) {
			}
		};
		txtpnSelectTheProperties.setText("Select the properties to hide.");
		txtpnSelectTheProperties.setFont(new Font("Lucida Fax", Font.BOLD, 16));
		txtpnSelectTheProperties.setEditable(false);
		txtpnSelectTheProperties.setBackground(GuiParameters.darkBlue);
		txtpnSelectTheProperties.setForeground(GuiParameters.lightGray);

		panel_3.add(txtpnSelectTheProperties, "cell 0 0,grow");
		panelCenterMeta.setLayout(gl_panelCenterMeta);

		panelCenterEncryptDoc = new JPanel();
		panelCenterEncryptDoc.setBackground(GuiParameters.panelBackgroundColor);
		panelCenter.add(panelCenterEncryptDoc, "name_10027163043687");

		// //////////////////////////////////////////////////////////////////////////////////////////////////

		panelCenterEncryptPic = new JPanel();
		panelCenterEncryptPic.setBackground(GuiParameters.panelBackgroundColor);
		panelCenter.add(panelCenterEncryptPic, "name_10027163043688");

		JPanel panel_11 = new JPanel();
		panel_11.setBackground(GuiParameters.panelBackgroundColor);

		lblRecommendationPic = new JLabel("");
		// lblRecommendationPic.setIcon(new
		// ImageIcon(MainWindow.class.getResource("/img/people_small.png")));
		// lblRecommendationPic.setIcon(new ImageIcon(MainWindow.class
		// .getResource("/img/people.png")));
		lblRecommendationPic.setBackground(GuiParameters.darkBlue);

		class CallBackResponderHoverPic implements CallBack {
			public void methodToCallBackOnClick(Boolean state) {
			}

			public void methodToCallBackOnHover(Boolean state) {
				if (state) {

					lblRecommendationPic.setVisible(true);
					lblRecommendationPic.setIcon(new ImageIcon(MainWindow.class
							.getResource("/img/people_small.png")));
				} else {
					lblRecommendationPic.setVisible(false);
				}
			}
		}
		CallBack callBackHoverPic = new CallBackResponderHoverPic();

		lblThumbnailPic = new ChangingImageLabel("thumbnail_attached_pic_s",
				"no_thumbnail_attached_pic_s", "add_thumbnail_pic_s",
				"remove_thumbnail_pic_s", encryptionEnabledByDefault, false,
				callBackHoverPic);

		lblThumbnailPic.setHorizontalTextPosition(SwingConstants.CENTER);
		lblThumbnailPic.setVerticalTextPosition(SwingConstants.TOP);

		lblThumbnailPic.setFont(GuiParameters.menuFont);

		lblBlurredPic = new ChangingImageLabel("thumbnail_attached_pic_s",
				"no_thumbnail_attached_pic_s", "add_thumbnail_pic_s",
				"remove_thumbnail_pic_s", encryptionEnabledByDefault, false,
				callBackHoverPic);
		lblBlurredPic.setHorizontalTextPosition(SwingConstants.CENTER);
		lblBlurredPic.setVerticalTextPosition(SwingConstants.TOP);

		lblBlurredPic.setFont(GuiParameters.menuFont);

		btnPicPreview = new JButton("");
		btnPicPreview.setIcon(new ImageIcon(MainWindow.class
				.getResource("/img/Loupe_s.png")));
		btnPicPreview.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// pic thumbnail preview
				// Check if file has changed since last call
				if (thumbnailPreview.getFile() == null
						|| !thumbnailPreview.getFile().getAbsolutePath()
								.equals(txtPath.getText())) {
					thumbnailPreview.setMainWindowResolution(resolutionPic
							.getSelectedIndex());
					try {
						thumbnailPreview.setFile(new File(txtPath.getText()));
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				// Show thumbnail window
				thumbnailPreview.setSelectedIndex(resolutionPic
						.getSelectedIndex());
				thumbnailPreview.setVisible(true);

				// If user pressed OK
				if (thumbnailPreview.isUserChoseOK()) {
					// chckbxPicThumbnail.setSelected(true);
					resolutionPic.setSelectedIndex(thumbnailPreview
							.getSelectedIndex());
				}
			}

		});
		btnPicPreview.setBackground(GuiParameters.panelBackgroundColor);
		btnPicPreview.setForeground(GuiParameters.minorButtonFrColor);
		btnPicPreview.setFont(GuiParameters.helpTextFont);
		btnPicPreview.setBorder(null);

		btnBlurredPicPreview = new JButton("");
		btnBlurredPicPreview.setIcon(new ImageIcon(MainWindow.class
				.getResource("/img/Loupe_s.png")));
		btnBlurredPicPreview.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// If new file is selected
				if (!imageBlurDialog.getImageFile().getAbsolutePath()
						.equals(txtPath.getText())) {
					imageBlurDialog.setImageFile(new File(txtPath.getText()));
				}

				imageBlurDialog.setVisible(true);
				if (imageBlurDialog.isUserChoseOK()) {
					// chckbxEditBlur.setSelected(true);
					imageBlurDialog.setVisible(false);
					/*
					 * try { ImageIO.write(docThumPrev.getOutputImage(), "png",
					 * new File("outputBlurred.png")); } catch (IOException e) {
					 * // TODO Auto-generated catch block e.printStackTrace(); }
					 */
				}
			}
		});
		btnBlurredPicPreview.setBackground(GuiParameters.panelBackgroundColor);
		btnBlurredPicPreview.setFont(GuiParameters.helpTextFont);
		btnBlurredPicPreview.setBorder(null);

		class CallBackResponderBothPic extends CallBackResponderHoverPic
				implements
					CallBack {
			public void methodToCallBackOnClick(Boolean state) {
				if (state) {

					lblThumbnailPic.setEnabled(true);
					lblBlurredPic.setEnabled(true);
					btnPicPreview.setEnabled(true);
					btnBlurredPicPreview.setEnabled(true);

				} else {
					lblThumbnailPic.setEnabled(false);
					lblBlurredPic.setEnabled(false);
					btnPicPreview.setEnabled(false);
					btnBlurredPicPreview.setEnabled(false);
				}
			}
		}

		CallBack callBackBothPic = new CallBackResponderBothPic();

		lblEncryptionPic = new ChangingImageLabel("encryption_on_s",
				"encryption_off_s", "add_encryption_s", "remove_encryption_s",
				true, encryptionEnabledByDefault, callBackBothPic);

		lblEncryptionPic.setHorizontalTextPosition(SwingConstants.CENTER);
		lblEncryptionPic.setVerticalTextPosition(SwingConstants.TOP);

		lblEncryptionPic.setFont(GuiParameters.menuFont);

		JCheckBox checkBoxPicReview = new JCheckBox("");
		checkBoxPicReview.setBackground(GuiParameters.darkBlue);

		JCheckBox chckbxNewCheckBox_7 = new JCheckBox("");
		chckbxNewCheckBox_7.setBackground(GuiParameters.darkBlue);

		JCheckBox checkBox = new JCheckBox("");
		checkBox.setBackground(GuiParameters.darkBlue);

		radbtnSelectFile = new JRadioButton("Select File");
		radbtnSelectFile.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				showPanel(panelCenterSelect);

			}
		});
		radbtnSelectFile.setFont(GuiParameters.menuFont);
		radbtnSelectFile.addMouseMotionListener(new MouseMotionAdapter() {

			@Override
			public void mouseDragged(MouseEvent arg0) {
				showPanel(panelCenterSelect);

			}
		});
		radbtnSelectFile.setFocusable(false);
		radbtnSelectFile.setHorizontalAlignment(SwingConstants.CENTER);
		radbtnSelectFile.setBackground(GuiParameters.radioButtonSelectedColor);
		radbtnSelectFile
				.setForeground(GuiParameters.radioButtonSelectedTxtColor);
		radbtnSelectFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				showPanel(panelCenterSelect);
			}
		});

		radbtnSelectFile.setVerticalTextPosition(SwingConstants.BOTTOM);
		radbtnSelectFile.setHorizontalTextPosition(SwingConstants.CENTER);
		radbtnSelectFile.setIcon(new ImageIcon(MainWindow.class
				.getResource("/img/document.png")));

		radbtnRisk = new JRadioButton("Risk explanation");
		radbtnRisk.setFont(GuiParameters.menuFont);
		radbtnRisk.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				if (txtPath.getText().equals("")) {
					showPanel(panelCenterSelect);
				}
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				if (!txtPath.getText().equals("")) {
					showPanel(panelCenterRisk);
				} else {
					btnAnalyzeRisk.setVisible(false);
					showPanel(panel_no_selection);
				}
			}
		});

		radbtnRisk.setFocusable(false);
		radbtnRisk.setHorizontalAlignment(SwingConstants.CENTER);
		radbtnRisk.setBackground(GuiParameters.radioButtonUnselectedColor);

		radbtnRisk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!txtPath.getText().equals("")) {

					showPanel(panelCenterRisk);
				} else {
					btnAnalyzeRisk.setVisible(false);
					showPanel(panel_no_selection);
				}
			}
		});
		radbtnRisk.setIcon(new ImageIcon(MainWindow.class
				.getResource("/img/ligthbulb_on.png")));
		radbtnRisk.setVerticalTextPosition(SwingConstants.BOTTOM);
		radbtnRisk.setHorizontalTextPosition(SwingConstants.CENTER);

		radbtnEditProp = new JRadioButton("Edit properties");
		radbtnEditProp.setFont(GuiParameters.menuFont);
		radbtnEditProp.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				if (txtPath.getText().equals("")) {
					showPanel(panelCenterSelect);
				}
			}
		});
		radbtnEditProp.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				if (!txtPath.getText().equals("")) {
					// Determine if selected file is image or not
					boolean image = false;
					String selectedFileExtension = FilenameUtils
							.getExtension(txtPath.getText());
					for (String ex : metadataManager.supportedImageFormats) {
						if (selectedFileExtension.toLowerCase().equals(ex)) {
							image = true;
						}
					}

					// Getting metadata form file
					metadataManager.parseMetada(txtPath.getText());

					// Setting property names
					if (image) {
						if (selectedFileExtension.toLowerCase().equals("jpg")) {

							panelImgTopLeft.setTxtPropertyName("Creator");
							panelImgTopLeft
									.setPropertyType(ManagedMetadata.CREATOR);
							panelImgTopLeft.setTxtProperty(metadataManager
									.getJpgArtist());

							panelImgTopRight.setTxtPropertyName("Created");
							panelImgTopRight
									.setPropertyType(ManagedMetadata.CREATION_DATE);
							panelImgTopRight.setTxtProperty(metadataManager
									.getJpgCreationDate());

							panelImgBottomLeft.setTxtPropertyName("Title");
							panelImgBottomLeft
									.setPropertyType(ManagedMetadata.TITLE);
							panelImgBottomLeft.setTxtProperty(metadataManager
									.getJpgTitle());

							panelImgBottomRight.setTxtPropertyName("GPS");
							panelImgBottomRight
									.setPropertyType(ManagedMetadata.GPS);
							panelImgBottomRight.setTxtProperty(metadataManager
									.getJpgGPS());

						} else if (selectedFileExtension.toLowerCase().equals(
								"png")) {
							panelImgTopLeft.setTxtPropertyName("Author");
							panelImgTopLeft
									.setPropertyType(ManagedMetadata.AUTHOR);
							panelImgTopLeft.setTxtProperty(metadataManager
									.getPngAuthor());

							panelImgTopRight.setTxtPropertyName("Created");
							panelImgTopRight
									.setPropertyType(ManagedMetadata.CREATION_DATE);
							panelImgTopRight.setTxtProperty(metadataManager
									.getPngCreationTime());

							panelImgBottomLeft.setTxtPropertyName("Title");
							panelImgBottomLeft
									.setPropertyType(ManagedMetadata.TITLE);
							panelImgBottomLeft.setTxtProperty(metadataManager
									.getPngTitle());

							panelImgBottomRight.setTxtPropertyName("Source");
							panelImgBottomRight
									.setPropertyType(ManagedMetadata.SOURCE);
							panelImgBottomRight.setTxtProperty(metadataManager
									.getPngSource());
						}
					} else {
						panelImgTopLeft.setTxtPropertyName("Author");
						panelImgTopLeft.setPropertyType(ManagedMetadata.AUTHOR);
						panelImgTopLeft.setTxtProperty(metadataManager
								.getDocAuthor());

						panelImgTopRight.setTxtPropertyName("Created");
						panelImgTopRight
								.setPropertyType(ManagedMetadata.CREATION_DATE);
						panelImgTopRight.setTxtProperty(metadataManager
								.getDocCreationDate());

						panelImgBottomLeft.setTxtPropertyName("Modified");
						panelImgBottomLeft
								.setPropertyType(ManagedMetadata.LAST_MODIFIED);
						panelImgBottomLeft.setTxtProperty(metadataManager
								.getDocLastModifiedDate());

						panelImgBottomRight.setTxtPropertyName("Title");
						panelImgBottomRight
								.setPropertyType(ManagedMetadata.TITLE);
						panelImgBottomRight.setTxtProperty(metadataManager
								.getDocTitle());
					}

					showPanel(panelCenterMeta);
				} else {
					btnAnalyzeRisk.setVisible(false);
					showPanel(panel_no_selection);
				}
			}
		});
		radbtnEditProp.setFocusable(false);
		radbtnEditProp.setHorizontalAlignment(SwingConstants.CENTER);
		radbtnEditProp.setBackground(GuiParameters.radioButtonUnselectedColor);

		radbtnEditProp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!txtPath.getText().equals("")) {

					showPanel(panelCenterMeta);
				} else {
					btnAnalyzeRisk.setVisible(false);
					showPanel(panel_no_selection);
				}
			}
		});
		radbtnEditProp.setIcon(new ImageIcon(MainWindow.class
				.getResource("/img/edit_metadata.png")));
		radbtnEditProp.setVerticalTextPosition(SwingConstants.BOTTOM);
		radbtnEditProp.setHorizontalTextPosition(SwingConstants.CENTER);

		radbtnEncrypt = new JRadioButton("Encrypt");
		radbtnEncrypt.setFont(GuiParameters.menuFont);
		radbtnEncrypt.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent arg0) {
				if (txtPath.getText().equals("")) {
					showPanel(panelCenterSelect);
				}
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				if (!txtPath.getText().equals("")) {
					// Determine if selected file is image or not
					boolean image = false;
					String selectedFileExtension = FilenameUtils
							.getExtension(txtPath.getText());
					for (String ex : contentHidingModel
							.getSupportedImageFormats()) {
						if (selectedFileExtension.toLowerCase().equals(ex)) {
							image = true;
						}
					}

					// Show image or doc encryption settings based on file
					// selected
					if (image) {
						try {
							resolutionPic
									.setModel(new DefaultComboBoxModel<String>(
											generateJComboBoxResoultion(1)));
							showPanel(panelCenterEncryptPic);
						} catch (IOException e1) {
							JOptionPane
									.showMessageDialog(
											frame,
											"Can't read selected file. Check in the 'Select file' menu if the selected filepath is valid.",
											"File read error",
											JOptionPane.ERROR_MESSAGE);
							radbtnSelectFile.doClick();
							// e.printStackTrace();
						}

					} else {
						File doc = new File(txtPath.getText());
						if (doc.isFile()) {
							try {
								resolutionDoc
										.setModel(new DefaultComboBoxModel<String>(
												generateJComboBoxResoultion(0)));
								showPanel(panelCenterEncryptDoc);
							} catch (IOException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						} else {
							JOptionPane
									.showMessageDialog(
											frame,
											"Can't read selected file. Check in the 'Select file' menu if the selected filepath is valid.",
											"File read error",
											JOptionPane.ERROR_MESSAGE);
							radbtnSelectFile.doClick();
						}
					}

				} else {
					btnAnalyzeRisk.setVisible(false);
					showPanel(panel_no_selection);

					// JOptionPane
					// .showMessageDialog(
					// frame,
					// "No file is selected. Please select a file in the 'Select file' menu.",
					// "No file found", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		radbtnEncrypt.setFocusable(false);
		radbtnEncrypt.setHorizontalAlignment(SwingConstants.CENTER);
		radbtnEncrypt.setBackground(GuiParameters.radioButtonUnselectedColor);

		radbtnEncrypt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!txtPath.getText().equals("")) {
					// Determine if selected file is image or not
					boolean image = false;
					String selectedFileExtension = FilenameUtils
							.getExtension(txtPath.getText());
					for (String ex : contentHidingModel
							.getSupportedImageFormats()) {
						if (selectedFileExtension.equals(ex)) {
							image = true;
						}
					}

					// Show image or doc encryption settings based on file
					// selected
					if (image) {
						try {
							resolutionPic
									.setModel(new DefaultComboBoxModel<String>(
											generateJComboBoxResoultion(1)));
							showPanel(panelCenterEncryptPic);
						} catch (IOException e) {
							JOptionPane
									.showMessageDialog(
											frame,
											"Can't read selected file. Check in the 'Select file' menu if the selected filepath is valid.",
											"File read error",
											JOptionPane.ERROR_MESSAGE);
							radbtnSelectFile.doClick();
							// e.printStackTrace();
						}

					} else {
						File doc = new File(txtPath.getText());
						if (doc.isFile()) {
							try {
								resolutionDoc
										.setModel(new DefaultComboBoxModel<String>(
												generateJComboBoxResoultion(0)));
								showPanel(panelCenterEncryptDoc);
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} else {
							JOptionPane
									.showMessageDialog(
											frame,
											"Can't read selected file. Check in the 'Select file' menu if the selected filepath is valid.",
											"File read error",
											JOptionPane.ERROR_MESSAGE);
							radbtnSelectFile.doClick();
						}
					}

				} else {
					btnAnalyzeRisk.setVisible(false);
					showPanel(panel_no_selection);

				}
				// JOptionPane
				// .showMessageDialog(
				// frame,
				// "No file is selected. Please select a file in the 'Select file' menu.",
				// "No file found", JOptionPane.ERROR_MESSAGE);
				// }
			}
		});
		radbtnEncrypt.setIcon(new ImageIcon(MainWindow.class
				.getResource("/img/lock.png")));
		radbtnEncrypt.setVerticalTextPosition(SwingConstants.BOTTOM);
		radbtnEncrypt.setHorizontalTextPosition(SwingConstants.CENTER);
		GroupLayout gl_panelWest = new GroupLayout(panelWest);
		gl_panelWest.setHorizontalGroup(gl_panelWest.createParallelGroup(
				Alignment.LEADING).addGroup(
				gl_panelWest
						.createSequentialGroup()
						.addContainerGap()
						.addGroup(
								gl_panelWest
										.createParallelGroup(Alignment.LEADING)
										.addComponent(radbtnEncrypt,
												Alignment.TRAILING,
												GroupLayout.DEFAULT_SIZE, 102,
												Short.MAX_VALUE)
										.addComponent(radbtnEditProp,
												Alignment.TRAILING,
												GroupLayout.DEFAULT_SIZE, 102,
												Short.MAX_VALUE)
										.addComponent(radbtnSelectFile,
												Alignment.TRAILING,
												GroupLayout.DEFAULT_SIZE, 102,
												Short.MAX_VALUE)
										.addComponent(radbtnRisk,
												GroupLayout.DEFAULT_SIZE, 102,
												Short.MAX_VALUE))));
		gl_panelWest.setVerticalGroup(gl_panelWest.createParallelGroup(
				Alignment.LEADING).addGroup(
				gl_panelWest.createSequentialGroup()
						.addComponent(radbtnSelectFile)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(radbtnRisk)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(radbtnEditProp)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(radbtnEncrypt).addGap(92)));
		panelWest.setLayout(gl_panelWest);

		Component verticalGlue = Box.createVerticalGlue();
		panelEast.setLayout(new MigLayout("", "[1px][238px]",
				"[238px][14px][][]"));
		panelEast.add(verticalGlue, "cell 0 0,alignx left,aligny top");

		// //////////////////////////////////////////////////////////////////////////////////////////////////

		Radial1Vertical gauge = new Radial1Vertical();
		gauge.setFrameVisible(false);
		gauge.setTickmarkColorFromThemeEnabled(false);
		gauge.setMinorTickmarkVisible(false);
		gauge.setFrameBaseColorEnabled(true);
		gauge.setFrameBaseColor(GuiParameters.darkBlue);
		gauge.setFont(new Font("Lucida Fax", Font.BOLD, 26));
		gauge.setFrameEffect(FrameEffect.EFFECT_INNER_FRAME);
		gauge.setTitleAndUnitFontEnabled(true);
		gauge.setKnobType(KnobType.BIG_STD_KNOB);
		gauge.setBackgroundColor(BackgroundColor.NOISY_PLASTIC);
		gauge.setTextureColor(GuiParameters.lightGray);// (new Color(0, 0,
		// 204));
		gauge.setBackgroundVisible(true);
		gauge.setCustomLcdUnitFont(new Font("Lucida Fax", Font.BOLD, 24));
		gauge.setFrameDesign(FrameDesign.CUSTOM);
		gauge.setLedColor(LedColor.GREEN_LED);
		gauge.setLedBlinking(true);
		gauge.setLcdVisible(false);
		gauge.setTicklabelOrientation(TicklabelOrientation.HORIZONTAL);
		gauge.setGaugeType(GaugeType.TYPE5);
		gauge.setValueAnimated(50.0);
		gauge.setMinimumSize(new Dimension(280, 300));
		gauge.setPointerShadowVisible(true);
		gauge.setPointerType(PointerType.TYPE5);
		// gauge.setGlowVisiblje(true);
		// gauge.setGlowing(true);
		// gauge.setAutoResetToZero(true);
		// gauge.setPeakValue(60.0);
		// gauge.setPeakValueVisible(true);
		gauge.setOrientation(Orientation.NORTH);
		gauge.setTrackStartColor(Color.GREEN);// new Color(255, 69, 0));
		gauge.setTrackSectionColor(GuiParameters.orange);// new Color(255, 165,
		// 0));
		gauge.setTrackStopColor(Color.RED);// new Color(173, 255, 47));
		// gauge.setTrackVisible(true);
		gauge.setTitleAndUnitFont(new Font("Lucida Fax", Font.BOLD, 24));
		// gauge.setTrackStop(1);
		// gauge.setTrackStart(100.0);
		// gauge.setOpaque(false);
		gauge.setRangeOfMeasuredValuesVisible(false);
		gauge.setTitle("");

		gauge.setUnitString("%");

		gauge.setTickmarkColor(Color.BLACK);
		gauge.setLabelColor(Color.BLACK);
		gauge.setSections(new Section(0, 33, new Color(240, 206, 104)),
				new Section(34, 66, GuiParameters.orange), new Section(67, 100,
						new Color(218, 149, 66)));
		gauge.setAreas(new Section(0, 33, new Color(240, 206, 104)),
				new Section(34, 66, GuiParameters.orange), new Section(67, 100,
						new Color(252, 173, 0)));

		// gauge.setSectionsVisible(true);
		gauge.setAreasVisible(true);
		gauge.setHighlightSection(true);
		gauge.setHighlightArea(true);
		// else {
		// gauge.setBackgroundVisible(false);
		// gauge.setCustomLayerVisible(false);
		// }
		// ////////////////////////////////////////////////////////
		gauge.setVisible(true);
		panelEast.add(gauge, "cell 1 1,grow");

		JLabel lblRiskMeter = new JLabel("Risk Meter");
		lblRiskMeter.setForeground(GuiParameters.lightGray);
		lblRiskMeter.setFont(new Font("Lucida Fax", Font.BOLD, 24));
		panelEast.add(lblRiskMeter, "cell 1 3,alignx center,aligny top");

		// //////////////////////////////////////////////////
		boolean setGaugeImage = false;
		if (setGaugeImage) {
			BufferedImage customLayer;
			try {
				customLayer = ImageIO.read(getClass().getResource(
						"/img/Tachometer_or.png"));
			} catch (IOException exception) {
				customLayer = null;
			}

			// Add the following code somewhere after the gauge was initialized
			if (customLayer != null) {
				gauge.setCustomLayer(customLayer);
			}
			gauge.setBackgroundVisible(true);
			gauge.setCustomLayerVisible(true);
		}

		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);

		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);

		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				exitProcedure();
			}
		});
		mnFile.add(mntmExit);

		JMenu mnNewMenu = new JMenu("Edit");
		menuBar.add(mnNewMenu);

		JMenuItem mntmPreferences = new JMenuItem("Preferences...");
		mnNewMenu.add(mntmPreferences);

		JMenuItem menuItem = new JMenuItem("");
		menuBar.add(menuItem);

		// Collect the center panels into a list
		// in order to ease handling them (visibility)
		centerPanelList = new ArrayList<>();
		centerPanelList.add(panelCenterSelect);
		centerPanelList.add(panelCenterRisk);
		centerPanelList.add(panel_no_selection);

		GroupLayout gl_panelCenterRisk = new GroupLayout(panelCenterRisk);
		gl_panelCenterRisk
				.setHorizontalGroup(gl_panelCenterRisk
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_panelCenterRisk
										.createSequentialGroup()
										.addGroup(
												gl_panelCenterRisk
														.createParallelGroup(
																Alignment.LEADING)
														.addComponent(
																textPane_1,
																GroupLayout.PREFERRED_SIZE,
																648,
																GroupLayout.PREFERRED_SIZE)
														.addComponent(
																textField,
																GroupLayout.PREFERRED_SIZE,
																684,
																GroupLayout.PREFERRED_SIZE))
										.addContainerGap(
												GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)));
		gl_panelCenterRisk.setVerticalGroup(gl_panelCenterRisk
				.createParallelGroup(Alignment.LEADING).addGroup(
						gl_panelCenterRisk
								.createSequentialGroup()
								.addComponent(textField,
										GroupLayout.PREFERRED_SIZE, 40,
										GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(textPane_1,
										GroupLayout.PREFERRED_SIZE, 273,
										GroupLayout.PREFERRED_SIZE)));
		panelCenterRisk.setLayout(gl_panelCenterRisk);
		centerPanelList.add(panelCenterMeta);
		centerPanelList.add(panelCenterEncryptDoc);

		JTextPane txtpnChooseThePreferred = new JTextPane() {
			public void setBorder(Border border) {
			}
		};
		txtpnChooseThePreferred
				.setText("\nChoose the preferred encryption settings.\n");
		txtpnChooseThePreferred.setFont(new Font("Lucida Fax", Font.BOLD, 16));
		txtpnChooseThePreferred.setEditable(false);
		txtpnChooseThePreferred.setBackground(GuiParameters.darkBlue);
		txtpnChooseThePreferred.setForeground(GuiParameters.lightGray);

		JLabel label_6 = new JLabel("");
		label_6.setIcon(new ImageIcon(MainWindow.class
				.getResource("/img/people.png")));
		label_6.setBackground(GuiParameters.darkBlue);

		JPanel panel_docOptions = new JPanel();
		panel_docOptions.setBackground(GuiParameters.darkBlue);
		SimpleAttributeSet attribs = new SimpleAttributeSet();
		StyleConstants.setAlignment(attribs, StyleConstants.ALIGN_CENTER);
		StyleConstants.setFontFamily(attribs, "Lucida Fax");
		StyleConstants.setBold(attribs, true);
		StyleConstants.setFontSize(attribs, 14);
		StyleConstants.setForeground(attribs, GuiParameters.lightGray);

		lblRecommendationDoc = new JLabel("");
		// lblRecommendationDoc.setIcon(new
		// ImageIcon(MainWindow.class.getResource("/img/people_small.png")));

		class CallBackResponderHoverDoc implements CallBack {
			public void methodToCallBackOnClick(Boolean state) {
			}

			public void methodToCallBackOnHover(Boolean state) {
				if (state) {

					lblRecommendationDoc.setVisible(true);
					lblRecommendationDoc.setIcon(new ImageIcon(MainWindow.class
							.getResource("/img/people_small.png")));
				} else {
					lblRecommendationDoc.setVisible(false);
				}
			}
		}
		CallBack callBackHoverDoc = new CallBackResponderHoverDoc();

		GroupLayout gl_panelCenterEncryptDoc = new GroupLayout(
				panelCenterEncryptDoc);
		gl_panelCenterEncryptDoc.setHorizontalGroup(gl_panelCenterEncryptDoc
				.createParallelGroup(Alignment.LEADING)
				.addGroup(
						gl_panelCenterEncryptDoc
								.createSequentialGroup()
								.addComponent(txtpnChooseThePreferred,
										GroupLayout.DEFAULT_SIZE, 616,
										Short.MAX_VALUE).addGap(6))
				.addComponent(panel_docOptions, Alignment.TRAILING,
						GroupLayout.PREFERRED_SIZE, 622, Short.MAX_VALUE));
		gl_panelCenterEncryptDoc.setVerticalGroup(gl_panelCenterEncryptDoc
				.createParallelGroup(Alignment.LEADING).addGroup(
						gl_panelCenterEncryptDoc
								.createSequentialGroup()
								.addComponent(txtpnChooseThePreferred,
										GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED,
										GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE)
								.addComponent(panel_docOptions,
										GroupLayout.PREFERRED_SIZE, 339,
										GroupLayout.PREFERRED_SIZE)
								.addContainerGap()));

		resolutionDoc = new JComboBox();
		resolutionDoc.setBackground(GuiParameters.minorButtonBgColor);
		resolutionDoc.setForeground(GuiParameters.minorButtonFrColor);
		resolutionDoc.setFont(GuiParameters.helpTextFont);

		JPanel right_panel = new JPanel();
		right_panel.setBackground(GuiParameters.panelBackgroundColor);

		JPanel left_panel = new JPanel();
		left_panel.setBackground(GuiParameters.panelBackgroundColor);

		lblSummaryDoc = new ChangingImageLabel("summary_attached_s",
				"no_summary_attached_s", "add_summary_s", "remove_summary_s",
				encryptionEnabledByDefault, false, callBackHoverDoc);
		left_panel.add(lblSummaryDoc);

		lblSummaryDoc.setHorizontalTextPosition(SwingConstants.CENTER);
		lblSummaryDoc.setVerticalTextPosition(SwingConstants.TOP);
		lblSummaryDoc.setFont(GuiParameters.menuFont);

		btnPreviewSummary = new JButton("");
		left_panel.add(btnPreviewSummary);
		btnPreviewSummary.setIcon(new ImageIcon(MainWindow.class
				.getResource("/img/Loupe_s.png")));
		btnPreviewSummary.setBorder(null);
		btnPreviewSummary.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!lblThumbnailDoc.isEnabled()) {
					return;
				}
				// Create summary
				if (!txtPath.getText().equals("")) {
					// If summary hasn't been generated yet
					if (contentHidingModel.getSummary() == null) {
						try {
							contentHidingModel.createSummary(new File(txtPath
									.getText()));
						} catch (IOException e) {
							JOptionPane
									.showMessageDialog(
											frame,
											"Can't find document to summarize. Check if a file is selected in the 'Select file' menu.",
											"No document found",
											JOptionPane.ERROR_MESSAGE);
							// e.printStackTrace();
						}
					}
					if (contentHidingModel.getSummary() != null) {

						DocSummaryPreview docSumPrev = new DocSummaryPreview(
								frame, contentHidingModel.getSummary());
						// If user has pressed OK
						if (docSumPrev.isSaveText()) {
							// The preview window's summary will be set
							contentHidingModel.setSummary(docSumPrev
									.getSummaryText());
							// chckbxSummary.setSelected(true);
						}
					}

					// System.out.println(contentHidingModel.getSummary());
				} else {
					JOptionPane
							.showMessageDialog(
									frame,
									"File has to be selected to summarize. Check if a file is selected in the 'Select file' menu.",
									"No document found",
									JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		btnPreviewSummary.setBackground(GuiParameters.panelBackgroundColor);
		// btnPreviewSummary.setForeground(GuiParameters.minorButtonFrColor);
		// btnPreviewSummary.setFont(GuiParameters.helpTextFont);
		btnPreviewSummary.setOpaque(false);

		lblThumbnailDoc = new ChangingImageLabel("thumbnail_attached_doc_s",
				"no_thumbnail_attached_doc_s", "add_thumbnail_doc_s",
				"remove_thumbnail_doc_s", encryptionEnabledByDefault, false,
				callBackHoverDoc);
		right_panel.add(lblThumbnailDoc);
		lblThumbnailDoc.setHorizontalTextPosition(SwingConstants.CENTER);
		lblThumbnailDoc.setVerticalTextPosition(SwingConstants.TOP);
		lblThumbnailDoc.setFont(GuiParameters.menuFont);

		btnPreviewThumbnailDoc = new JButton("");
		right_panel.add(btnPreviewThumbnailDoc);
		btnPreviewThumbnailDoc.setIcon(new ImageIcon(MainWindow.class
				.getResource("/img/Loupe_s.png")));
		btnPreviewThumbnailDoc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!lblThumbnailDoc.isEnabled()) {
					return;
				}
				// doc thumbnail preview
				// Check if file has changed since last call
				if (thumbnailPreview.getFile() == null
						|| !thumbnailPreview.getFile().getAbsolutePath()
								.equals(txtPath.getText())) {
					thumbnailPreview.setMainWindowResolution(resolutionDoc
							.getSelectedIndex());
					try {
						thumbnailPreview.setFile(new File(txtPath.getText()));
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				// Show thumbnail window
				thumbnailPreview.setSelectedIndex(resolutionDoc
						.getSelectedIndex());
				thumbnailPreview.setVisible(true);

				// If user pressed OK
				if (thumbnailPreview.isUserChoseOK()) {
					// chckbxDocThumbnail.setSelected(true);
					resolutionPic.setSelectedIndex(thumbnailPreview
							.getSelectedIndex());
				}
			}

		});
		btnPreviewThumbnailDoc
				.setBackground(GuiParameters.panelBackgroundColor);
		// btnDocPreview.setForeground(GuiParameters.minorButtonFrColor);
		btnPreviewThumbnailDoc.setFont(GuiParameters.helpTextFont);
		btnPreviewThumbnailDoc.setOpaque(false);
		btnPreviewThumbnailDoc.setBorder(null);

		panelCenterEncryptDoc.setLayout(gl_panelCenterEncryptDoc);
		centerPanelList.add(panelCenterEncryptPic);

		class CallBackResponderBothDoc extends CallBackResponderHoverDoc
				implements
					CallBack {
			public void methodToCallBackOnClick(Boolean state) {
				if (state) {

					lblSummaryDoc.setEnabled(true);
					lblThumbnailDoc.setEnabled(true);
					btnPreviewSummary.setEnabled(true);
					btnPreviewThumbnailDoc.setEnabled(true);

				} else {
					lblSummaryDoc.setEnabled(false);
					lblThumbnailDoc.setEnabled(false);
					btnPreviewSummary.setEnabled(false);
					btnPreviewThumbnailDoc.setEnabled(false);
				}
			}
		}

		CallBack callBackBoth = new CallBackResponderBothDoc();

		lblEncryptionDoc = new ChangingImageLabel("encryption_on_s",
				"encryption_off_s", "add_encryption_s", "remove_encryption_s",
				true, encryptionEnabledByDefault, callBackBoth);
		lblEncryptionDoc.setHorizontalTextPosition(SwingConstants.CENTER);
		lblEncryptionDoc.setVerticalTextPosition(SwingConstants.TOP);
		lblEncryptionDoc.setFont(GuiParameters.menuFont);

		GroupLayout gl_panel_docOptions = new GroupLayout(panel_docOptions);
		gl_panel_docOptions
				.setHorizontalGroup(gl_panel_docOptions
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_panel_docOptions
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(left_panel,
												GroupLayout.PREFERRED_SIZE,
												155, GroupLayout.PREFERRED_SIZE)
										.addGap(18)
										.addGroup(
												gl_panel_docOptions
														.createParallelGroup(
																Alignment.LEADING)
														.addComponent(
																lblEncryptionDoc,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE)
														.addGroup(
																gl_panel_docOptions
																		.createSequentialGroup()
																		.addGap(98)
																		.addComponent(
																				lblRecommendationDoc)))
										.addGroup(
												gl_panel_docOptions
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																gl_panel_docOptions
																		.createSequentialGroup()
																		.addGap(18)
																		.addComponent(
																				right_panel,
																				GroupLayout.PREFERRED_SIZE,
																				146,
																				GroupLayout.PREFERRED_SIZE)
																		.addGap(324)
																		.addComponent(
																				label_6))
														.addGroup(
																gl_panel_docOptions
																		.createSequentialGroup()
																		.addGap(73)
																		.addComponent(
																				resolutionDoc,
																				GroupLayout.PREFERRED_SIZE,
																				GroupLayout.DEFAULT_SIZE,
																				GroupLayout.PREFERRED_SIZE)))));
		gl_panel_docOptions
				.setVerticalGroup(gl_panel_docOptions
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_panel_docOptions
										.createSequentialGroup()
										.addGroup(
												gl_panel_docOptions
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																gl_panel_docOptions
																		.createSequentialGroup()
																		.addGap(81)
																		.addComponent(
																				left_panel,
																				GroupLayout.PREFERRED_SIZE,
																				221,
																				GroupLayout.PREFERRED_SIZE))
														.addGroup(
																gl_panel_docOptions
																		.createSequentialGroup()
																		.addGroup(
																				gl_panel_docOptions
																						.createParallelGroup(
																								Alignment.LEADING)
																						.addGroup(
																								gl_panel_docOptions
																										.createSequentialGroup()
																										.addComponent(
																												lblEncryptionDoc,
																												GroupLayout.PREFERRED_SIZE,
																												GroupLayout.DEFAULT_SIZE,
																												GroupLayout.PREFERRED_SIZE)
																										.addGap(32)
																										.addComponent(
																												lblRecommendationDoc))
																						.addGroup(
																								gl_panel_docOptions
																										.createSequentialGroup()
																										.addGap(54)
																										.addComponent(
																												resolutionDoc,
																												GroupLayout.PREFERRED_SIZE,
																												GroupLayout.DEFAULT_SIZE,
																												GroupLayout.PREFERRED_SIZE)
																										.addPreferredGap(
																												ComponentPlacement.RELATED)
																										.addComponent(
																												right_panel,
																												GroupLayout.PREFERRED_SIZE,
																												221,
																												GroupLayout.PREFERRED_SIZE)))
																		.addPreferredGap(
																				ComponentPlacement.RELATED,
																				33,
																				Short.MAX_VALUE)
																		.addComponent(
																				label_6)))
										.addContainerGap()));
		panel_docOptions.setLayout(gl_panel_docOptions);

		JTextPane txtpnnchooseThePreferred = new JTextPane() {
			public void setBorder(Border border) {
			}
		};
		txtpnnchooseThePreferred
				.setText("\nChoose the preferred encryption settings.\n");
		txtpnnchooseThePreferred.setFont(new Font("Lucida Fax", Font.BOLD, 16));
		txtpnnchooseThePreferred.setEditable(false);
		txtpnnchooseThePreferred.setBackground(GuiParameters.darkBlue);
		txtpnnchooseThePreferred.setForeground(GuiParameters.lightGray);

		GroupLayout gl_panelCenterEncryptPic = new GroupLayout(
				panelCenterEncryptPic);
		gl_panelCenterEncryptPic
				.setHorizontalGroup(gl_panelCenterEncryptPic
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_panelCenterEncryptPic
										.createSequentialGroup()
										.addGroup(
												gl_panelCenterEncryptPic
														.createParallelGroup(
																Alignment.LEADING)
														.addComponent(
																txtpnnchooseThePreferred,
																GroupLayout.PREFERRED_SIZE,
																653,
																GroupLayout.PREFERRED_SIZE)
														.addGroup(
																gl_panelCenterEncryptPic
																		.createSequentialGroup()
																		.addContainerGap()
																		.addComponent(
																				panel_11,
																				GroupLayout.PREFERRED_SIZE,
																				GroupLayout.DEFAULT_SIZE,
																				GroupLayout.PREFERRED_SIZE)))
										.addContainerGap(69, Short.MAX_VALUE)));
		gl_panelCenterEncryptPic.setVerticalGroup(gl_panelCenterEncryptPic
				.createParallelGroup(Alignment.LEADING).addGroup(
						gl_panelCenterEncryptPic
								.createSequentialGroup()
								.addComponent(txtpnnchooseThePreferred,
										GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(panel_11,
										GroupLayout.PREFERRED_SIZE, 329,
										Short.MAX_VALUE).addGap(19)));

		resolutionPic = new JComboBox<>();
		resolutionPic.setBackground(GuiParameters.minorButtonBgColor);
		resolutionPic.setForeground(GuiParameters.minorButtonFrColor);
		resolutionPic.setFont(GuiParameters.helpTextFont);
		GroupLayout gl_panel_11 = new GroupLayout(panel_11);
		gl_panel_11
				.setHorizontalGroup(gl_panel_11
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_panel_11
										.createSequentialGroup()
										.addGroup(
												gl_panel_11
														.createParallelGroup(
																Alignment.LEADING,
																false)
														.addGroup(
																gl_panel_11
																		.createSequentialGroup()
																		.addContainerGap()
																		.addComponent(
																				lblThumbnailPic,
																				GroupLayout.PREFERRED_SIZE,
																				GroupLayout.DEFAULT_SIZE,
																				GroupLayout.PREFERRED_SIZE)
																		.addGap(12)
																		.addComponent(
																				lblEncryptionPic,
																				GroupLayout.PREFERRED_SIZE,
																				GroupLayout.DEFAULT_SIZE,
																				GroupLayout.PREFERRED_SIZE)
																		.addGroup(
																				gl_panel_11
																						.createParallelGroup(
																								Alignment.LEADING)
																						.addGroup(
																								gl_panel_11
																										.createSequentialGroup()
																										.addGap(26)
																										.addComponent(
																												lblBlurredPic,
																												GroupLayout.PREFERRED_SIZE,
																												GroupLayout.DEFAULT_SIZE,
																												GroupLayout.PREFERRED_SIZE))
																						.addGroup(
																								gl_panel_11
																										.createSequentialGroup()
																										.addGap(95)
																										.addComponent(
																												resolutionPic,
																												GroupLayout.PREFERRED_SIZE,
																												GroupLayout.DEFAULT_SIZE,
																												GroupLayout.PREFERRED_SIZE))))
														.addGroup(
																Alignment.TRAILING,
																gl_panel_11
																		.createSequentialGroup()
																		.addGap(64)
																		.addComponent(
																				btnPicPreview)
																		.addGap(176)
																		.addComponent(
																				lblRecommendationPic)
																		.addPreferredGap(
																				ComponentPlacement.RELATED,
																				GroupLayout.DEFAULT_SIZE,
																				Short.MAX_VALUE)
																		.addComponent(
																				btnBlurredPicPreview)
																		.addGap(62)))
										.addContainerGap(109, Short.MAX_VALUE)));
		gl_panel_11
				.setVerticalGroup(gl_panel_11
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_panel_11
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												gl_panel_11
														.createParallelGroup(
																Alignment.TRAILING)
														.addComponent(
																lblThumbnailPic,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE)
														.addComponent(
																lblEncryptionPic,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE)
														.addGroup(
																gl_panel_11
																		.createSequentialGroup()
																		.addComponent(
																				resolutionPic,
																				GroupLayout.PREFERRED_SIZE,
																				GroupLayout.DEFAULT_SIZE,
																				GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				ComponentPlacement.RELATED)
																		.addComponent(
																				lblBlurredPic,
																				GroupLayout.PREFERRED_SIZE,
																				GroupLayout.DEFAULT_SIZE,
																				GroupLayout.PREFERRED_SIZE)))
										.addPreferredGap(
												ComponentPlacement.RELATED)
										.addGroup(
												gl_panel_11
														.createParallelGroup(
																Alignment.TRAILING)
														.addGroup(
																gl_panel_11
																		.createParallelGroup(
																				Alignment.LEADING)
																		.addComponent(
																				btnBlurredPicPreview)
																		.addComponent(
																				btnPicPreview))
														.addComponent(
																lblRecommendationPic))
										.addGap(60)));
		panel_11.setLayout(gl_panel_11);
		panelCenterEncryptPic.setLayout(gl_panelCenterEncryptPic);

		// Collect the side menu points into a list
		// in order to ease handling them (visibility)
		menuPointList = new ArrayList<>();
		menuPointList.add(radbtnSelectFile);
		menuPointList.add(radbtnRisk);
		menuPointList.add(radbtnEditProp);
		menuPointList.add(radbtnEncrypt);

		cloudServices = new ArrayList<>();

		loadServices();

		// If after load root folder is empty or unreachable then prompt
		// FirstStart window
		File f = new File(privySealRootDir);
		if (privySealRootDir.equals("") || !f.isDirectory()) {
			FirstStart fs = new FirstStart(frame);
			privySealRootDir = fs.getPSRootFolder();
		}

		// Directory change listener has to be set for services
		for (ServiceCheckBox scb : cloudServices) {
			if (!scb.getPrivySealFolder().equals("")) {
				scb.setSyncManager(new SyncManager(new File(privySealRootDir
						+ File.separator + scb.getPrivySealFolder()
						+ File.separator + "settings.xml")));
			}
		}

		showPanel(panelCenterSelect);

	}

	public String getUploadFilePath() {
		return txtPath.getText();
	}

	public JFrame getFrame() {
		return frame;
	}

	// http://www.mkyong.com/java/how-to-create-xml-file-in-java-dom/
	private void saveServices(List<ServiceCheckBox> list) {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory
				.newInstance();
		DocumentBuilder docBuilder;
		try {
			docBuilder = docFactory.newDocumentBuilder();

			// root elements
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("services");
			doc.appendChild(rootElement);
			// Add PS root folder
			Element psRootFolderElement = doc.createElement("PSRootFolder");
			psRootFolderElement.appendChild(doc
					.createTextNode(privySealRootDir));
			rootElement.appendChild(psRootFolderElement);

			Integer serviceID = 0;
			for (ServiceCheckBox scb : list) {
				// staff elements
				Element service = doc.createElement("service");
				rootElement.appendChild(service);

				// set attribute to staff element
				Attr attr = doc.createAttribute("id");
				attr.setValue(serviceID.toString());
				service.setAttributeNode(attr);
				serviceID++;

				Element name = doc.createElement("name");
				name.appendChild(doc.createTextNode(scb.getName()));
				service.appendChild(name);

				Element iconPath = doc.createElement("iconPath");
				iconPath.appendChild(doc.createTextNode(scb.getIconPath()));
				service.appendChild(iconPath);

				Element serviceFolder = doc.createElement("serviceFolder");
				serviceFolder.appendChild(doc.createTextNode(scb
						.getServiceFolder()));
				service.appendChild(serviceFolder);

				Element privySealFolder = doc.createElement("privySealFolder");
				privySealFolder.appendChild(doc.createTextNode(scb
						.getPrivySealFolder()));
				service.appendChild(privySealFolder);

				// write the content into xml file
				TransformerFactory transformerFactory = TransformerFactory
						.newInstance();
				transformerFactory.setAttribute("indent-number", 4); // doesn't
				// work...
				Transformer transformer = transformerFactory.newTransformer();
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
				DOMSource source = new DOMSource(doc);
				StreamResult result = new StreamResult(new File("services.xml"));

				// Output to console for testing
				// StreamResult result = new StreamResult(System.out);

				transformer.transform(source, result);

				// System.out.println("File saved!");
			}
		} catch (TransformerException te) {
			te.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// http://www.mkyong.com/java/how-to-read-xml-file-in-java-dom-parser/
	private void loadServices() {
		try {

			File fXmlFile = new File("services.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);

			// optional, but recommended
			// read this -
			// http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
			doc.getDocumentElement().normalize();

			// System.out.println("Root element :" +
			// doc.getDocumentElement().getNodeName());

			// Set PrivySeal folder
			Node n = doc.getElementsByTagName("PSRootFolder").item(0);
			privySealRootDir = n.getTextContent();

			NodeList nList = doc.getElementsByTagName("service");

			// System.out.println("----------------------------");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				// System.out.println("\nCurrent Element :" +
				// nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					final ServiceCheckBox serChkbx = new ServiceCheckBox(
							eElement.getElementsByTagName("name").item(0)
									.getTextContent(), eElement
									.getElementsByTagName("iconPath").item(0)
									.getTextContent(), eElement
									.getElementsByTagName("serviceFolder")
									.item(0).getTextContent(), eElement
									.getElementsByTagName("privySealFolder")
									.item(0).getTextContent(), true);
					serChkbx.setForeground(GuiParameters.lightGray);
					serChkbx.setBackground(GuiParameters.darkBlue);
					serChkbx.addActionListener(new ActionListener() {

						@Override
						public void actionPerformed(ActionEvent arg0) {
							ServiceCheckBox source = (ServiceCheckBox) arg0
									.getSource();

							// Extra handler for default services
							// Folders have to be set up when the user clicks on
							// the provider for the first time
							if (source.getServiceFolder().equals("")
									|| source.getPrivySealFolder().equals("")) {
								AddNewService newService = new AddNewService(
										frame, serChkbx.getName(), serChkbx
												.getIconPath(), serChkbx
												.getServiceFolder(), serChkbx
												.getPrivySealFolder(),
										privySealRootDir);
								// If OK button was pressed
								if (newService.getChoice() == 0) {
									serChkbx.setName(newService
											.getServiceName());
									serChkbx.setIconPath(newService
											.getServiceLogo());
									serChkbx.setServiceFolder(newService
											.getServiceFolder());
									serChkbx.setPrivySealFolder(newService
											.getServicePrivySealFolder());
									serChkbx.setSyncManager(new SyncManager(
											new File(
													privySealRootDir
															+ File.separator
															+ newService
																	.getServicePrivySealFolder()
															+ File.separator
															+ "settings.xml")));
								}
							}
							for (ServiceCheckBox scb : cloudServices) {
								if (!scb.equals(source)) {
									scb.setSelected(false);
									scb.setBorderPainted(false);
									scb.setBackground(GuiParameters.darkBlue);
									scb.setForeground(GuiParameters.lightGray);
								}

								if (source.isSelected()) {
									source.setBackground(GuiParameters.lightGray);
									source.setForeground(Color.black);
									source.setBorderPainted(true);
								} else {
									source.setBorderPainted(false);
								}
							}

						}
					});

					serChkbx.setDropTarget(new DropTarget() {
						public synchronized void drop(DropTargetDropEvent evt) {
							try {
								// Extra handler for default services
								// Folders have to be set up when the user
								// clicks on the provider for the first time
								if (serChkbx.getServiceFolder().equals("")
										|| serChkbx.getPrivySealFolder()
												.equals("")) {
									AddNewService newService = new AddNewService(
											frame, serChkbx.getName(), serChkbx
													.getIconPath(), serChkbx
													.getServiceFolder(),
											serChkbx.getPrivySealFolder(),
											privySealRootDir);
									// If OK button was pressed
									if (newService.getChoice() == 0) {
										serChkbx.setName(newService
												.getServiceName());
										serChkbx.setIconPath(newService
												.getServiceLogo());
										serChkbx.setServiceFolder(newService
												.getServiceFolder());
										serChkbx.setPrivySealFolder(newService
												.getServicePrivySealFolder());
										serChkbx.setSyncManager(new SyncManager(
												new File(
														privySealRootDir
																+ File.separator
																+ newService
																		.getServicePrivySealFolder()
																+ File.separator
																+ "settings.xml")));

									}
								}
								evt.acceptDrop(DnDConstants.ACTION_COPY);
								List<File> dropppedFiles = (List<File>) evt
										.getTransferable().getTransferData(
												DataFlavor.javaFileListFlavor);
								for (File file : dropppedFiles) {
									txtPath.setText(file.getAbsolutePath());
									for (ServiceCheckBox sbc : cloudServices) {
										sbc.setSelected(false);
										sbc.setBackground(GuiParameters.darkBlue);
										sbc.setForeground(GuiParameters.lightGray);
										sbc.setBorderPainted(false);
									}

									serChkbx.setBackground(GuiParameters.lightGray);
									serChkbx.setForeground(Color.black);
									serChkbx.setSelected(true);
									serChkbx.setBorderPainted(true);
								}
							} catch (UnsupportedFlavorException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}

					});

					serChkbx.setAlignmentX(Component.CENTER_ALIGNMENT);
					serChkbx.setHorizontalAlignment(SwingConstants.CENTER);
					serChkbx.setBackground(GuiParameters.darkBlue);
					serChkbx.setBorder(new LineBorder(Color.BLACK, 3));

					cloudServices.add(serChkbx);
					// panelSelect.remove(panelAddnewProvider);

					JPanel jp = new JPanel();
					jp.setBackground(GuiParameters.darkBlue);
					jp.setLayout(new BoxLayout(jp, BoxLayout.Y_AXIS));
					GridBagConstraints gbc_panel = new GridBagConstraints();
					gbc_panel.fill = GridBagConstraints.NONE;
					gbc_panel.gridx = (cloudServices.size() - 1) % 3;
					gbc_panel.gridy = (cloudServices.size() - 1) / 3;
					panelSelect.add(jp, gbc_panel);

					jp.add(serChkbx);

					GridBagConstraints gbc_panelAddNewProv = new GridBagConstraints();
					gbc_panelAddNewProv.fill = GridBagConstraints.NONE;
					gbc_panelAddNewProv.gridx = (cloudServices.size()) % 3;
					gbc_panelAddNewProv.gridy = (cloudServices.size()) / 3;
					// panelSelect.add(panelAddnewProvider,
					// gbc_panelAddNewProv);

					/*
					 * System.out.println("id : " +
					 * eElement.getAttribute("id"));
					 * System.out.println("name : " +
					 * eElement.getElementsByTagName
					 * ("name").item(0).getTextContent());
					 * System.out.println("iconPath : " +
					 * eElement.getElementsByTagName
					 * ("iconPath").item(0).getTextContent());
					 * System.out.println("serviceFolder : " +
					 * eElement.getElementsByTagName
					 * ("serviceFolder").item(0).getTextContent());
					 * System.out.println("privySealFolder : " +
					 * eElement.getElementsByTagName
					 * ("privySealFolder").item(0).getTextContent());
					 */
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void exitProcedure() {
		saveServices(cloudServices);
		System.exit(0);
	}

	private String[] generateJComboBoxResoultion(int type) throws IOException {
		String[] modelStr = null;
		int[][] res = null;
		int maxResIndex = contentHidingModel.resolutionA4DocValues.length;

		// Doc resolutions
		if (type == 0) {
			res = contentHidingModel.generateDocResolutions();
			modelStr = new String[maxResIndex];
			for (int i = 0; i < maxResIndex; i++) {
				modelStr[i] = res[i][0] + " x " + res[i][1];
			}
			// Image resoulutions
		} else if (type == 1) {
			res = contentHidingModel
					.generateImageResolutions(txtPath.getText());
			// Image is bigger then min base value
			if (res != null) {
				// Get maximum resolution that is not bigger than the image
				BufferedImage img = ImageIO.read(new File(txtPath.getText()));
				for (maxResIndex = 0; maxResIndex < res.length; maxResIndex++) {
					if (res[maxResIndex][0] > img.getWidth()
							|| res[maxResIndex][1] > img.getHeight()) {
						break;
					}
				}

				modelStr = new String[maxResIndex];
				for (int i = 0; i < maxResIndex; i++) {
					modelStr[i] = res[i][0] + " x " + res[i][1];
				}
			} else {
				modelStr = new String[1];
				modelStr[0] = "Image resoltuion is smaller than minimum thumbnail resolution (200px).";
			}
		}

		return modelStr;
	}

	private void showPanel(JPanel panel) {

		// if the currentPanel is the selected one, no need to switch
		if (currentPanel == panel) {
			return;
		} else {
			currentPanel = panel;
		}
		// Seting panels
		for (JPanel p : centerPanelList) {
			if (p == panel) {
				p.setVisible(true);
			} else {
				p.setVisible(false);
			}
		}
		if (panel == panelCenterSelect) {
			handleMenuSelection(menuPointList, radbtnSelectFile);
			btnAddNewProvider.setVisible(true);
			btnAnalyzeRisk.setVisible(true);
			btnUpload.setVisible(false);
		} else if (panel == panelCenterRisk) {
			handleMenuSelection(menuPointList, radbtnRisk);
			btnAddNewProvider.setVisible(false);
			btnAnalyzeRisk.setVisible(false);
			btnUpload.setVisible(true);
		} else if (panel == panelCenterMeta) {
			handleMenuSelection(menuPointList, radbtnEditProp);
			btnAddNewProvider.setVisible(false);
			btnAnalyzeRisk.setVisible(false);
			btnUpload.setVisible(true);
		} else if ((panel == panelCenterEncryptDoc || panel == panelCenterEncryptPic)) {
			handleMenuSelection(menuPointList, radbtnEncrypt);
			btnAddNewProvider.setVisible(false);
			btnAnalyzeRisk.setVisible(false);
			btnUpload.setVisible(true);
		} else if ((panel == panel_no_selection)) {
			btnAddNewProvider.setVisible(false);
			btnAnalyzeRisk.setVisible(false);
			btnUpload.setVisible(false);
		}

		strTextHelp = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard \n"
				+ "dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.\n"
				+ " It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.";
		txtHelp.setText(strTextHelp);

		if (panel == panelCenterMeta) {
			panelMetadataExtraButtons.setVisible(true);
		} else {
			panelMetadataExtraButtons.setVisible(false);
		}
	}

	private void handleMenuSelection(ArrayList<JRadioButton> menuPointList,
			JRadioButton selectedButton) {
		selectedButton.setBackground(GuiParameters.radioButtonSelectedColor);
		selectedButton.setForeground(GuiParameters.radioButtonSelectedTxtColor);

		for (JRadioButton button : menuPointList) {
			if (button != selectedButton) {
				button.setBackground(GuiParameters.radioButtonUnselectedColor);
				button.setForeground(GuiParameters.radioButtonUnselectedTxtColor);

			}
		}
		return;

	}
}