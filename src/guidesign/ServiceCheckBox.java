package guidesign;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.nio.channels.FileChannel;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.SwingConstants;

import syncecncrypt.SyncManager;


public class ServiceCheckBox extends JCheckBox{
	private String name;
	private String iconPath;
	private String serviceFolder;
	private String privySealFolder;
	private SyncManager syncManager;
	
	public ServiceCheckBox(String name, String iconPath, String serviceFolder, String privySealFolder, boolean loaded){
		
		super();
		
		this.name = name;
		this.iconPath = iconPath;
		this.serviceFolder = serviceFolder;
		this.privySealFolder = privySealFolder;
		
		this.setText(this.name);
		this.setFont(new Font("Lucida Fax", Font.BOLD, 16));
		this.setVerticalTextPosition(SwingConstants.BOTTOM);
		this.setHorizontalTextPosition(SwingConstants.CENTER);
		//setSelectedIcon(new ImageIcon(MainWindow.class.getResource("/img/google-drive-transparent_small_selected.png")));
		
		try {			
			File originalIcon = new File(this.iconPath);
			//if icons are loaded no resize needed
			if(!loaded){				
				//copy file in folder that can be persistent
				File icon =  new File("img/" + originalIcon.getName());
				copyFile(originalIcon, icon);
				
				//resize image proportionately
				int resizedWidth;
				int resizedHeight;
				BufferedImage originalImage = ImageIO.read(icon);
				if(originalImage.getWidth() > originalImage.getHeight()){
					int ratio = originalImage.getWidth() / 100;
					resizedHeight = originalImage.getHeight() / ratio;
					resizedWidth = 100;
				}else{
					int ratio = originalImage.getHeight() / 100;
					resizedWidth = originalImage.getWidth() / ratio;
					resizedHeight = 100;
				}
				BufferedImage resizedImage = new BufferedImage(resizedWidth, resizedHeight, BufferedImage.TYPE_INT_ARGB);
				Graphics2D g = resizedImage.createGraphics();
				g.drawImage(originalImage, 0, 0, resizedWidth, resizedHeight, null);			
				ImageIO.write(resizedImage, "png", icon);
				g.dispose();
				
				setIcon(new ImageIcon(icon.getPath()));
				this.iconPath = icon.getPath();
			}else{
				setIcon(new ImageIcon(originalIcon.getPath()));
				this.iconPath = originalIcon.getPath();
			}
			
		} catch (IOException e) {
			//If file not found use default icon
			setIcon(new ImageIcon("img/default_service.png"));
			this.iconPath = "img/default_service.png";
		}
	}
	
	public static void copyFile(File sourceFile, File destFile) throws IOException {
	    if(!destFile.exists()) {
	        destFile.createNewFile();
	    }

	    FileChannel source = null;
	    FileChannel destination = null;

	    try {
	        source = new FileInputStream(sourceFile).getChannel();
	        destination = new FileOutputStream(destFile).getChannel();
	        destination.transferFrom(source, 0, source.size());
	    }
	    finally {
	        if(source != null) {
	            source.close();
	        }
	        if(destination != null) {
	            destination.close();
	        }
	    }
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIconPath() {
		return iconPath;
	}

	public void setIconPath(String iconPath) {
		this.iconPath = iconPath;
	}

	public String getServiceFolder() {
		return serviceFolder;
	}

	public void setServiceFolder(String serviceFolder) {
		this.serviceFolder = serviceFolder;
	}

	public String getPrivySealFolder() {
		return privySealFolder;
	}

	public void setPrivySealFolder(String privySealFolder) {
		this.privySealFolder = privySealFolder;
	}

	public SyncManager getSyncManager() {
		return syncManager;
	}

	public void setSyncManager(SyncManager syncHandler) {
		this.syncManager = syncHandler;
	}
}
