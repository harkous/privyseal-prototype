package guidesign;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.SwingConstants;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.poifs.property.Parent;

import contenthiding.ContentHidingModel;
import de.uni_siegen.wineme.come_in.thumbnailer.ThumbnailerException;

public class ThumbnailPreview extends JDialog implements ActionListener {

	private final JPanel contentPanel = new JPanel();
	private JFrame parentFrame;
	private JPanel panel;
	private JButton btnOk;
	private File selectedFile;
	private File[] thumbFilesArray;
	private JLabel lblImageThumbnail;
	private JPanel panel_1;
	private JComboBox<String> comboBox;
	private ContentHidingModel contentHidingModel;
	private boolean userChoseOK;
	private int[][] resolution;
	private int mainWinResIndex;

	/*
	public static void main(String[] args) {
		try {
			JFrame frame = new JFrame();
			frame.setSize(300, 200);
			frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			frame.setVisible(true);
			File f = new File("C:\\Users\\The Tiger\\Desktop\\purplecloud-logo_small.png");
			ThumbnailPreview dialog = new ThumbnailPreview(frame, f);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	*/
	/**
	 * Create the dialog.
	 */
	public ThumbnailPreview(JFrame frame, ContentHidingModel chm) {
		super(frame, true);
		this.selectedFile = null;
		this.contentHidingModel = chm;
		this.userChoseOK = false;
		this.parentFrame = frame;
		this.thumbFilesArray = new File[ContentHidingModel.resolutionBaseValues.length];
		createUI(parentFrame);
		addWindowListener(new WindowAdapter() {
			@Override  
		      public void windowClosing(WindowEvent e) {  
		        userChoseOK = false;
		      }  
		});
	}
	
	public File[] getThumbFiles() {
		return thumbFilesArray;
	}
	
	public void setThumbFile(File f, int index){
		thumbFilesArray[index] = f;
	}

	public void createUI(JFrame frame){
		setTitle("Thumbnail preview");
		setBounds(100, 100, 300, 300);
		contentPanel.setBackground(GuiParameters.panelBackgroundColor);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPanel);
		contentPanel.setLayout(new BorderLayout(0, 0));
		
//		JLabel lblThumbnail = new JLabel("Thumbnail:");
//		lblThumbnail.setBackground(GuiParameters.panelBackgroundColor);
//		lblThumbnail.setForeground(GuiParameters.defaultTxtColor);
//		lblThumbnail.setHorizontalAlignment(SwingConstants.CENTER);
//		contentPanel.add(lblThumbnail, BorderLayout.NORTH);
//		lblThumbnail.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		panel_1.setBorder(null);
		contentPanel.add(panel_1, BorderLayout.CENTER);
		
		lblImageThumbnail = new JLabel("");
		panel_1.add(lblImageThumbnail);
		lblImageThumbnail.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		comboBox = new JComboBox<>();	
		comboBox.setFont(GuiParameters.helpTextFont);
		comboBox.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				//Generate new thumbnail
				try {
					int selectedIndex = comboBox.getSelectedIndex();
					
					if(thumbFilesArray[selectedIndex] == null && resolution != null){
						thumbFilesArray[selectedIndex] = contentHidingModel.generateThumbnail(selectedFile, resolution[selectedIndex][0], resolution[selectedIndex][1]);
					}
					
					displayThumbnail(selectedIndex);
				} catch (Exception e1) {
					
					e1.printStackTrace();
				}			
			}
		});
				
		panel = new JPanel();
		panel.setBackground(Color.WHITE);
		contentPanel.add(panel, BorderLayout.SOUTH);
		
		btnOk = new JButton("OK");
		panel.add(btnOk);
		btnOk.setBackground(GuiParameters.minorButtonBgColor);
		btnOk.setForeground(GuiParameters.minorButtonFrColor);
		btnOk.setActionCommand("OK");	
		
		panel.add(comboBox);
		btnOk.addActionListener(this);
		btnOk.setFont(GuiParameters.helpTextFont);
		setLocationRelativeTo(frame);	
		setVisible(false);	
	}

	private void onNewFile() throws Exception{
		try {			
			//Clean up
			for (int i = 0; i < thumbFilesArray.length; i++) {
				thumbFilesArray[i] = null;
			}
			
			// Determine if selected file is image or not
			boolean image = false;
			String selectedFileExtension = FilenameUtils.getExtension(selectedFile.getPath());
			for (String ex : contentHidingModel.getSupportedImageFormats()) {
				if (selectedFileExtension.toLowerCase().equals(ex)) {
					image = true;
				}
			}
			
			//Determine resolutions
			if(image){
				//Image resolutions
				comboBox.setModel(new DefaultComboBoxModel<String>(generateJComboBoxResoultion(1)));
				resolution = contentHidingModel.generateImageResolutions(selectedFile.getPath());
			}else{
				//Doc resolutions
				comboBox.setModel(new DefaultComboBoxModel<String>(generateJComboBoxResoultion(0)));
				resolution = contentHidingModel.generateDocResolutions();
			}
			
			//Generate first thumbnail
			if(thumbFilesArray[mainWinResIndex] == null && resolution != null){
				thumbFilesArray[mainWinResIndex] = contentHidingModel.generateThumbnail(selectedFile, resolution[mainWinResIndex][0], resolution[mainWinResIndex][1]);
			}
			
			//Display the generated thumbnail
			displayThumbnail(mainWinResIndex);
			
			
		} catch (IOException | ThumbnailerException e) {
			lblImageThumbnail.setText("No thumbnail to show");
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equals("OK")){
			userChoseOK = true;
		}else{
			userChoseOK = false;
		}
		setVisible(false);	
	}	
	
	public boolean isUserChoseOK() {
		return userChoseOK;
	}

	public void setFile(File f) throws Exception{
		this.selectedFile = f;
		onNewFile();
	}
	
	public File getFile(){
		return this.selectedFile;
	}
	
	public void setMainWindowResolution(int resIndex){
		this.mainWinResIndex = resIndex;
	}
	
	public int getSelectedIndex(){
		return this.comboBox.getSelectedIndex();
	}
	
	public void setSelectedIndex(int i){
		this.comboBox.setSelectedIndex(i);
	}
	
	private String[] generateJComboBoxResoultion(int type) throws IOException {
		String[] modelStr = null;
		int[][] res = null;
		int maxResIndex = contentHidingModel.resolutionA4DocValues.length;

		// Doc resolutions
		if (type == 0) {
			res = contentHidingModel.generateDocResolutions();
			modelStr = new String[maxResIndex];
			for (int i = 0; i < maxResIndex; i++) {
				modelStr[i] = res[i][0] + " x " + res[i][1];
			}
			// Image resoulutions
		} else if (type == 1) {
			res = contentHidingModel.generateImageResolutions(selectedFile.getPath());
			//Image is bigger then min base value
			if(res != null){
				// Get maximum resolution that is not bigger than the image
				BufferedImage img = ImageIO.read(selectedFile);
				for (maxResIndex = 0; maxResIndex < res.length; maxResIndex++) {
					if (res[maxResIndex][0] > img.getWidth()
							|| res[maxResIndex][1] > img.getHeight()) {
						break;
					}
				}
				
				modelStr = new String[maxResIndex];
				for (int i = 0; i < maxResIndex; i++) {
					modelStr[i] = res[i][0] + " x " + res[i][1];
				}
			}else{
				modelStr = new String[1];
				modelStr[0] = "Image resoltuion is smaller than minimum thumbnail resolution (200px).";			
			}
		}
		

		return modelStr;
	}
	
	private void displayThumbnail(int index){
		BufferedImage img;
		try {
			if(thumbFilesArray[index] != null){
				img = ImageIO.read(thumbFilesArray[index]);
			}else{
				img = ImageIO.read(ThumbnailPreview.class.getResource("/img/noThumbnail.png"));
			}
			ImageIcon icon = new ImageIcon(img);
			lblImageThumbnail.setIcon(icon);
			
			this.setSize(img.getWidth() + 45, img.getHeight() + 120); //addition to fit other components in window as well
			setLocationRelativeTo(parentFrame);
		} catch (IOException e) {
			System.err.println("Default 'no thumbnail' image is not found.");
		}				
	}
}
