package metadata;

import java.util.List;

import metadata.MetadataManager.ManagedMetadata;

public interface IMetadataFileWriter {
	public void write(String srcPath, String dstPath, List<ManagedMetadata> metaToShow);
}
