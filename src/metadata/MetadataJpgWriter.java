package metadata;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.ImageWriteException;
import org.apache.commons.imaging.Imaging;
import org.apache.commons.imaging.common.IImageMetadata;
import org.apache.commons.imaging.formats.jpeg.JpegImageMetadata;
import org.apache.commons.imaging.formats.jpeg.exif.ExifRewriter;
import org.apache.commons.imaging.formats.tiff.TiffField;
import org.apache.commons.imaging.formats.tiff.TiffImageMetadata;
import org.apache.commons.imaging.formats.tiff.constants.ExifTagConstants;
import org.apache.commons.imaging.formats.tiff.constants.GpsTagConstants;
import org.apache.commons.imaging.formats.tiff.constants.TiffTagConstants;
import org.apache.commons.imaging.formats.tiff.taginfos.TagInfoGpsText;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputDirectory;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputField;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputSet;
import org.apache.commons.imaging.util.IoUtils;
import org.apache.commons.io.FilenameUtils;

import metadata.MetadataManager.ManagedMetadata;

public class MetadataJpgWriter implements IMetadataFileWriter{

	@Override
	public void write(String srcPath, String dstPath, List<ManagedMetadata> metaToShow) {        				
		OutputStream os = null;
		try {
			// Note that metadata might be null if no metadata is found.
	        final IImageMetadata metadata = Imaging.getMetadata(new File(srcPath));
	        final JpegImageMetadata jpegMetadata = (JpegImageMetadata) metadata;
	        
	        //Create new TIFF directory which means we'll have an empty set of exif metadata
			TiffOutputSet outputSet = new TiffOutputSet();
	        TiffOutputDirectory tiffOutDir = outputSet.getOrCreateExifDirectory();
	        
	        if (null != jpegMetadata) {
				
	            // Note that exif might be null if no Exif metadata is found.
	        	TiffImageMetadata exif = jpegMetadata.getExif();
	            
	            
	        	//Keeping only the requested properties			
	            TiffField field;
	            TiffOutputField outField;
	            
	            
	            field = jpegMetadata.findEXIFValue(TiffTagConstants.TIFF_TAG_ARTIST);
	            if(field != null && metaToShow.contains(ManagedMetadata.CREATOR)){
	            	outField = new TiffOutputField(TiffTagConstants.TIFF_TAG_ARTIST, field.getFieldType(), (int)field.getCount(), field.getByteArrayValue());
	            	tiffOutDir.add(outField);
	            }
	            
	            
	            field = jpegMetadata.findEXIFValue(ExifTagConstants.EXIF_TAG_DATE_TIME_ORIGINAL);
	            if(field != null && metaToShow.contains(ManagedMetadata.CREATION_DATE)){
	            	outField = new TiffOutputField(ExifTagConstants.EXIF_TAG_DATE_TIME_ORIGINAL, field.getFieldType(), (int)field.getCount(), field.getByteArrayValue());
	            	tiffOutDir.add(outField);	            	
	            }	            
	            
	            
	            TiffImageMetadata.GPSInfo gpsInfo = exif.getGPS();
	            if(gpsInfo != null  && metaToShow.contains(ManagedMetadata.GPS)){
	            	outputSet.setGPSInDegrees(gpsInfo.getLongitudeAsDegreesEast(), gpsInfo.getLatitudeAsDegreesNorth());
	            }
	            
	            field = jpegMetadata.findEXIFValue(TiffTagConstants.TIFF_TAG_IMAGE_DESCRIPTION);
	            if(field != null && metaToShow.contains(ManagedMetadata.TITLE)){
	            	outField = new TiffOutputField(TiffTagConstants.TIFF_TAG_IMAGE_DESCRIPTION, field.getFieldType(), (int)field.getCount(), field.getByteArrayValue());
	            	tiffOutDir.add(outField);
	            }
	                       			
	        }						
			
	        //Removing all data by creating a new image file
	        BufferedImage inputImage = ImageIO.read(new File(srcPath));
	        String tempPath = FilenameUtils.getFullPath(dstPath);
	        String name = FilenameUtils.getBaseName(dstPath);
	        name += "_temp.jpg";
	        tempPath += name;
			ImageIO.write(inputImage, "jpg", new File(tempPath));


	        //Writing file on disk	
	        os = new FileOutputStream(dstPath);
	        os = new BufferedOutputStream(os);
	        new ExifRewriter().updateExifMetadataLossless(new File(tempPath), os, outputSet);
	        
	        //Delete temp file
	        File f = new File(tempPath);
	        f.delete();
	        
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ImageReadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ImageWriteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
            try {
				IoUtils.closeQuietly(true, os);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
		
	}

}
