package metadata;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.pdfbox.examples.pdmodel.CreateBlankPDF;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.tika.Tika;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.SAXException;

public class MetadataManager {
	//If new format added, modify MainWindow radbtnEditProps action listener
	public final String[] supportedImageFormats = {"jpg", "png"};
	public final String[] supportedDocumentFormats = {"pdf", "doc", "xls", "ppt", "docx", "xlsx", "pptx"};
	public static enum ManagedMetadata {CREATOR, GPS, CREATION_DATE, TITLE, AUTHOR, SOURCE, LAST_MODIFIED};
	private Metadata metadata;
	
	public void parseMetada(String path){
		try {		
			//Input file for parsing
			File new_meta = new File(path);
		    InputStream is = new FileInputStream(new_meta);
	
		    //Setting up Tika
		    metadata = new Metadata();
		    BodyContentHandler ch = new BodyContentHandler();
		    AutoDetectParser parser = new AutoDetectParser();
		    
		    //Detecting file type
		    String mimeType = new Tika().detect(new_meta);
		    metadata.set(Metadata.CONTENT_TYPE, mimeType);

		    //Actual parsing
		    parser.parse(is, ch, metadata, new ParseContext());
		    is.close();
		    
		    //Test sysout of metadata
		    for (int i = 0; i < metadata.names().length; i++) {
		        String item = metadata.names()[i];
		        for(String s : metadata.getAllValues(item)){
		        	System.out.println(item + " -- " + s);
		        }
		    }
		    
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException | TikaException | SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void writeFile(File srcFile, String dstPath, List<ManagedMetadata> metaToShow){
		String srcExtension = FilenameUtils.getExtension(srcFile.getPath());
		//Office 2007
		if(srcExtension.equals("docx") || srcExtension.equals("xlsx") || srcExtension.equals("pptx")){
			
			IMetadataFileWriter office2007Writer = new MetadataOffice2007Writer();
			office2007Writer.write(srcFile.getPath(), dstPath, metaToShow);
			
		//Office 97-2003
		}else if(srcExtension.equals("doc") || srcExtension.equals("xls") || srcExtension.equals("ppt")){
			IMetadataFileWriter office97_2003Writer = new MetadataOffice97_2003Writer();
			office97_2003Writer.write(srcFile.getPath(), dstPath, metaToShow);
		//PDF
		}else if(srcExtension.equals("pdf")){
			IMetadataFileWriter pdfWriter = new MetadataPdfWriter();
			pdfWriter.write(srcFile.getPath(), dstPath, metaToShow);
		//JPG
		}else if(srcExtension.equals("jpg")){
			IMetadataFileWriter jpgWriter = new MetadataJpgWriter();
			jpgWriter.write(srcFile.getPath(), dstPath, metaToShow);
		//PNG
		}else if(srcExtension.equals("png")){
			IMetadataFileWriter pngWriter = new MetadataPngWriter();
			pngWriter.write(srcFile.getPath(), dstPath, metaToShow);
		}else{
			
		}
	}
	
	public String getDocCreationDate(){
		String creationDate = null;
		for(String name : metadata.names()){
			if(name.equals("Creation-Date") || name.equals("date")){
				creationDate = metadata.get(name);
				break;
			}
		}
		
		return creationDate;
	}
	
	public String getDocAuthor(){
		String author = null;
		for(String name : metadata.names()){
			if(name.equals("Author") || name.equals("creator")){
				author = metadata.get(name);
				break;
			}
		}
		
		return author;
	}
	
	public String getDocLastModifiedDate(){
		String lastModifiedDate = null;
		for(String name : metadata.names()){
			if(name.equals("Last-Modified")){
				lastModifiedDate = metadata.get(name);
				break;
			}
		}
		
		return lastModifiedDate;
	}
	
	public String getDocTitle(){
		String title = null;
		for(String name : metadata.names()){
			if(name.equals("title")){
				title = metadata.get(name);
				break;
			}
		}
		
		return title;
	}
	
	public String getJpgTitle(){
		String title = null;
		for(String name : metadata.names()){
			if(name.equals("description") || name.equals("Unknown tag (0x010e)")){
				title = metadata.get(name);
				break;
			}
		}
		
		return title;
	}
	
	public String getJpgArtist(){
		String artist = null;
		for(String name : metadata.names()){
			if(name.equals("Artist") || name.equals("Unknown tag (0x013b)")){
				artist = metadata.get(name);
				break;
			}
		}
		
		return artist;
	}
	
	public String getJpgGPS(){
		//May use StringBuffer to increase performance
		String gps = null;
		for(String name : metadata.names()){
			if(name.equals("GPS Longitude")){
				if(gps == null){
					gps = metadata.get(name);
				}else{
					gps += System.lineSeparator() + metadata.get(name);
				}
			}
			
			if(name.equals("GPS Latitude")){
				if(gps == null){
					gps = metadata.get(name);
				}else{
					gps += System.lineSeparator() + metadata.get(name);
				}
			}
		}
		
		return gps;
	}
	
	public String getJpgCreationDate(){
		String creationDate = null;
		for(String name : metadata.names()){
			if(name.equals("Date/Time Original") || name.equals("Unknown tag (0x0132)")){
				creationDate = metadata.get(name);
				break;
			}
		}
		
		return creationDate;
	}
	
	public String getPngTitle(){
		String title = null;
		String[] metadataAllValues = metadata.getAllValues("Text TextEntry");
		if(metadataAllValues != null){
			for(String s : metadataAllValues){
				String[] textArray = s.split(",");
				for(String text : textArray){
					if(text.contains("keyword=Title")){
						for(String value : textArray){
							if(value.contains("value=")){
								int valueIndex = value.indexOf("=");
								title = value.substring(valueIndex + 1); //+1 to start after =
							}
						}
					}
				}
			}
		}
		
		return title;
	}
	
	public String getPngAuthor(){
		String author = null;
		String[] metadataAllValues = metadata.getAllValues("Text TextEntry");
		if(metadataAllValues != null){
			for(String s : metadataAllValues){
				String[] textArray = s.split(",");
				for(String text : textArray){
					if(text.contains("keyword=Author")){
						for(String value : textArray){
							if(value.contains("value=")){
								int valueIndex = value.indexOf("=");
								author = value.substring(valueIndex + 1); //+1 to start after =
							}
						}
					}
				}
			}
		}
		
		return author;
	}
	
	public String getPngCreationTime(){
		String creationTime = null;
		String[] metadataAllValues = metadata.getAllValues("Text TextEntry");
		if(metadataAllValues != null){
			for(String s : metadataAllValues){
				String[] textArray = s.split(",");
				for(String text : textArray){
					if(text.contains("keyword=Creation Time")){
						for(String value : textArray){
							if(value.contains("value=")){
								int valueIndex = value.indexOf("=");
								creationTime = value.substring(valueIndex + 1); //+1 to start after =
							}
						}
					}
				}
			}
		}
		
		return creationTime;
	}
	
	public String getPngSource(){
		String source = null;
		String[] metadataAllValues = metadata.getAllValues("Text TextEntry");
		if(metadataAllValues != null){
			for(String s : metadataAllValues){
				String[] textArray = s.split(",");
				for(String text : textArray){
					if(text.contains("keyword=Source")){
						for(String value : textArray){
							if(value.contains("value=")){
								int valueIndex = value.indexOf("=");
								source = value.substring(valueIndex + 1); //+1 to start after =
							}
						}
					}
				}
			}
		}
		
		return source;
	}
}
