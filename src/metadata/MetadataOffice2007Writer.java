package metadata;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.POIXMLDocument;
import org.apache.poi.POIXMLProperties;
import org.apache.poi.openxml4j.util.Nullable;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import metadata.MetadataManager.ManagedMetadata;

public class MetadataOffice2007Writer implements IMetadataFileWriter{

	@Override
	public void write(String srcPath, String dstPath, List<ManagedMetadata> metaToShow) {
		// TODO Auto-generated method stub
		try {
			InputStream is = new FileInputStream(srcPath);
			POIXMLDocument doc = null;

			//The doc object is dependent on the file format
			String srcExtension = FilenameUtils.getExtension(srcPath);
			if(srcExtension.equals("docx")){
				doc = new XWPFDocument(is);
			}else if(srcExtension.equals("xlsx")){
				doc = new XSSFWorkbook(is);
			}else if(srcExtension.equals("pptx")){
				doc = new XMLSlideShow(is);
			}
 
			//Getting properties
			POIXMLProperties props =  doc.getProperties();
			POIXMLProperties.CoreProperties coreProps = props.getCoreProperties();
			POIXMLProperties.CustomProperties customProps = props.getCustomProperties();
			POIXMLProperties.ExtendedProperties extProps = props.getExtendedProperties();

			//Keeping only the requested properties
			HashMap<ManagedMetadata, Object> metaToKeep = new HashMap<>();
			
			for(ManagedMetadata mm : metaToShow){
					if(mm.equals(ManagedMetadata.AUTHOR)){
						metaToKeep.put(ManagedMetadata.AUTHOR, coreProps.getCreator());
					
					}else if(mm.equals(ManagedMetadata.CREATION_DATE)){
						metaToKeep.put(ManagedMetadata.CREATION_DATE, coreProps.getCreated());
					
					}else if(mm.equals(ManagedMetadata.LAST_MODIFIED)){
						metaToKeep.put(ManagedMetadata.LAST_MODIFIED, coreProps.getModified());
						
					}else if(mm.equals(ManagedMetadata.TITLE)){
						metaToKeep.put(ManagedMetadata.TITLE, coreProps.getTitle());
					}
			}
			
			//Removing all other properties
			removeAllProperties(coreProps, extProps, customProps);
			
			//Writing back properties to keep
			if(metaToKeep.containsKey(ManagedMetadata.AUTHOR)){
				coreProps.setCreator((String)metaToKeep.get(ManagedMetadata.AUTHOR));
			}
			if(metaToKeep.containsKey(ManagedMetadata.CREATION_DATE)){
				coreProps.setCreated((Nullable<Date>)metaToKeep.get(ManagedMetadata.CREATION_DATE));
			}
			if(metaToKeep.containsKey(ManagedMetadata.LAST_MODIFIED)){
				coreProps.setModified((Nullable<Date>)metaToKeep.get(ManagedMetadata.LAST_MODIFIED));
			}
			if(metaToKeep.containsKey(ManagedMetadata.TITLE)){
				coreProps.setTitle((String)metaToKeep.get(ManagedMetadata.TITLE));
			}

			//Write file on disk
			doc.write(new FileOutputStream(new File(dstPath)));
						
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void removeAllProperties(POIXMLProperties.CoreProperties coreProps, POIXMLProperties.ExtendedProperties extProps, POIXMLProperties.CustomProperties custProps){
		removeCoreProperties(coreProps);
		removeExtendedProperties(extProps);
		removeCustomProperties(custProps);
	}
	
	private void removeCoreProperties(POIXMLProperties.CoreProperties coreProps){
		coreProps.setCategory(null);
		coreProps.setContentStatus(null);
		coreProps.setContentType(null);
		coreProps.setCreated((String)null);
		coreProps.setCreator(null);
		coreProps.setDescription(null);
		coreProps.setIdentifier(null);
		coreProps.setKeywords(null);
		coreProps.setLastPrinted((String)null);
		coreProps.setModified((String)null);
		coreProps.setRevision(null);
		coreProps.setSubjectProperty(null);
		coreProps.setTitle(null);
		coreProps.getUnderlyingProperties().setLanguageProperty(null);
		coreProps.getUnderlyingProperties().setLastModifiedByProperty(null);
		coreProps.getUnderlyingProperties().setVersionProperty(null);
		coreProps.getUnderlyingProperties();
	}
	
	private void removeExtendedProperties(POIXMLProperties.ExtendedProperties extProps){
		if(extProps.getUnderlyingProperties().isSetApplication()){
			extProps.getUnderlyingProperties().unsetApplication();
		}
		if(extProps.getUnderlyingProperties().isSetAppVersion()){
			extProps.getUnderlyingProperties().unsetAppVersion();
		}
		if(extProps.getUnderlyingProperties().isSetCharacters()){
			extProps.getUnderlyingProperties().unsetCharacters();
		}
		if(extProps.getUnderlyingProperties().isSetCharactersWithSpaces()){
			extProps.getUnderlyingProperties().unsetCharactersWithSpaces();
		}
		if(extProps.getUnderlyingProperties().isSetCompany()){
			extProps.getUnderlyingProperties().unsetCompany();
		}
		if(extProps.getUnderlyingProperties().isSetDigSig()){
			extProps.getUnderlyingProperties().unsetDigSig();
		}
		if(extProps.getUnderlyingProperties().isSetDocSecurity()){
			extProps.getUnderlyingProperties().unsetDocSecurity();
		}
		if(extProps.getUnderlyingProperties().isSetHeadingPairs()){
			extProps.getUnderlyingProperties().unsetHeadingPairs();
		}
		if(extProps.getUnderlyingProperties().isSetHiddenSlides()){
			extProps.getUnderlyingProperties().unsetHiddenSlides();
		}
		if(extProps.getUnderlyingProperties().isSetHLinks()){
			extProps.getUnderlyingProperties().unsetHLinks();
		}
		if(extProps.getUnderlyingProperties().isSetHyperlinkBase()){
			extProps.getUnderlyingProperties().unsetHyperlinkBase();
		}
		if(extProps.getUnderlyingProperties().isSetHyperlinksChanged()){
			extProps.getUnderlyingProperties().unsetHyperlinksChanged();
		}
		if(extProps.getUnderlyingProperties().isSetLines()){
			extProps.getUnderlyingProperties().unsetLines();
		}
		if(extProps.getUnderlyingProperties().isSetLinksUpToDate()){
			extProps.getUnderlyingProperties().unsetLinksUpToDate();
		}
		if(extProps.getUnderlyingProperties().isSetManager()){
			extProps.getUnderlyingProperties().unsetManager();
		}
		if(extProps.getUnderlyingProperties().isSetMMClips()){
			extProps.getUnderlyingProperties().unsetMMClips();
		}
		if(extProps.getUnderlyingProperties().isSetNotes()){
			extProps.getUnderlyingProperties().unsetNotes();
		}
		if(extProps.getUnderlyingProperties().isSetPages()){
			extProps.getUnderlyingProperties().unsetPages();
		}
		if(extProps.getUnderlyingProperties().isSetParagraphs()){
			extProps.getUnderlyingProperties().unsetParagraphs();
		}
		if(extProps.getUnderlyingProperties().isSetPresentationFormat()){
			extProps.getUnderlyingProperties().unsetPresentationFormat();
		}
		if(extProps.getUnderlyingProperties().isSetScaleCrop()){
			extProps.getUnderlyingProperties().unsetScaleCrop();
		}
		if(extProps.getUnderlyingProperties().isSetSharedDoc()){
			extProps.getUnderlyingProperties().unsetSharedDoc();
		}
		if(extProps.getUnderlyingProperties().isSetSlides()){
			extProps.getUnderlyingProperties().unsetSlides();
		}
		if(extProps.getUnderlyingProperties().isSetTemplate()){
			extProps.getUnderlyingProperties().unsetTemplate();
		}
		if(extProps.getUnderlyingProperties().isSetTitlesOfParts()){
			extProps.getUnderlyingProperties().unsetTitlesOfParts();
		}
		if(extProps.getUnderlyingProperties().isSetTotalTime()){
			extProps.getUnderlyingProperties().unsetTotalTime();
		}
		if(extProps.getUnderlyingProperties().isSetWords()){
			extProps.getUnderlyingProperties().unsetWords();
		}
	}
	
	private void removeCustomProperties(POIXMLProperties.CustomProperties custProps){
		custProps = null;
	}



}
