package metadata;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.poi.hpsf.DocumentSummaryInformation;
import org.apache.poi.hpsf.MarkUnsupportedException;
import org.apache.poi.hpsf.NoPropertySetStreamException;
import org.apache.poi.hpsf.PropertySet;
import org.apache.poi.hpsf.PropertySetFactory;
import org.apache.poi.hpsf.SummaryInformation;
import org.apache.poi.hpsf.UnexpectedPropertySetTypeException;
import org.apache.poi.hpsf.WritingNotSupportedException;
import org.apache.poi.poifs.filesystem.DirectoryEntry;
import org.apache.poi.poifs.filesystem.DocumentEntry;
import org.apache.poi.poifs.filesystem.DocumentInputStream;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import com.sun.star.i18n.Calendar;

import metadata.MetadataManager.ManagedMetadata;

public class MetadataOffice97_2003Writer implements IMetadataFileWriter{

	@Override
	public void write(String srcPath, String dstPath, List<ManagedMetadata> metaToShow) {
		// Open the POI filesystem.
		SummaryInformation si = null;
	    DocumentSummaryInformation dsi = null;
		DirectoryEntry dir = null;
		POIFSFileSystem poifs = null;
		/* Handling the document summary information is analogous to handling
	     * the summary information. An additional feature, however, are the
	     * custom properties. */
		try {
			InputStream is = new FileInputStream(srcPath);
			poifs = new POIFSFileSystem(is);
			is.close();
	     
			// Read the summary information.
			dir = poifs.getRoot();
			DocumentEntry siEntry = (DocumentEntry)dir.getEntry(SummaryInformation.DEFAULT_STREAM_NAME);
			DocumentInputStream dis = new DocumentInputStream(siEntry);
			PropertySet ps = new PropertySet(dis);
			dis.close();
			si = new SummaryInformation(ps);
			
			// Read the document summary information.
			DocumentEntry dsiEntry = (DocumentEntry)dir.getEntry(DocumentSummaryInformation.DEFAULT_STREAM_NAME);
			DocumentInputStream dis2 = new DocumentInputStream(dsiEntry);
			PropertySet ps2 = new PropertySet(dis2);
			dis2.close();
			dsi = new DocumentSummaryInformation(ps2);
	         
	     }catch (FileNotFoundException ex){
	         // There is no summary information yet. We have to create a new one.
	    	 si = PropertySetFactory.newSummaryInformation();
	    	 // There is no document summary information yet. We have to create a new one.
	    	 dsi = PropertySetFactory.newDocumentSummaryInformation();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoPropertySetStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MarkUnsupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnexpectedPropertySetTypeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	     //Keeping only the requested properties
		 HashMap<ManagedMetadata, Object> metaToKeep = new HashMap<>();
		
		 for(ManagedMetadata mm : metaToShow){
			if(mm.equals(ManagedMetadata.AUTHOR)){
				metaToKeep.put(ManagedMetadata.AUTHOR, si.getAuthor());
				
			}else if(mm.equals(ManagedMetadata.CREATION_DATE)){
				metaToKeep.put(ManagedMetadata.CREATION_DATE, si.getCreateDateTime());
			
			}else if(mm.equals(ManagedMetadata.LAST_MODIFIED)){
				metaToKeep.put(ManagedMetadata.LAST_MODIFIED, si.getLastSaveDateTime());
				
			}else if(mm.equals(ManagedMetadata.TITLE)){
				metaToKeep.put(ManagedMetadata.TITLE, si.getTitle());
			}
		 }
		 
		//Removing all other properties
		removeAllProperties(si, dsi);
		
		//Writing back properties to keep
		if(metaToKeep.containsKey(ManagedMetadata.AUTHOR)){
			si.setAuthor((String)metaToKeep.get(ManagedMetadata.AUTHOR));
		}
		
		if(metaToKeep.containsKey(ManagedMetadata.CREATION_DATE)){
			si.setCreateDateTime((Date)metaToKeep.get(ManagedMetadata.CREATION_DATE));
		}
		
		if(metaToKeep.containsKey(ManagedMetadata.LAST_MODIFIED)){
			si.setLastSaveDateTime((Date)metaToKeep.get(ManagedMetadata.LAST_MODIFIED));
		}
		
		if(metaToKeep.containsKey(ManagedMetadata.TITLE)){
			si.setTitle((String)metaToKeep.get(ManagedMetadata.TITLE));
		}
		
	    try {
	 		// Write the summary information and the document summary information to the POI filesystem.
			si.write(dir, SummaryInformation.DEFAULT_STREAM_NAME);
			dsi.write(dir, DocumentSummaryInformation.DEFAULT_STREAM_NAME);
			
		    /* Write the POI filesystem back to the original file. Please note that
		     * in production code you should never write directly to the origin
		     * file! In case of a writing error everything would be lost. */
		     OutputStream out = new FileOutputStream(dstPath);
		     poifs.writeFilesystem(out);
		     out.close();
		} catch (WritingNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	     
		
	}
	
	private void removeAllProperties(SummaryInformation si, DocumentSummaryInformation dsi){
		removeSummaryInfo(si);
		removeDocSummaryInfo(dsi);
	}
	
	private void removeSummaryInfo(SummaryInformation si){
		si.removeApplicationName();
		si.removeAuthor();
		si.removeCharCount();
		si.removeComments();
		si.removeCreateDateTime();
		si.removeEditTime();
		si.removeKeywords();
		si.removeLastAuthor();
		si.removeLastPrinted();
		si.removeLastSaveDateTime();
		si.removePageCount();
		si.removeRevNumber();
		si.removeSecurity();
		si.removeSubject();
		si.removeTemplate();
		si.removeThumbnail();
		si.removeTitle();
		si.removeWordCount();
	}
	
	private void removeDocSummaryInfo(DocumentSummaryInformation dsi){
		dsi.removeByteCount();
		dsi.removeCategory();
		dsi.removeCompany();
		//dsi.removeCustomProperties(); 		//Here it takes care of custom properties
		dsi.removeDocparts();
		dsi.removeHeadingPair();
		dsi.removeHiddenCount();
		dsi.removeLineCount();
		dsi.removeLinksDirty();
		dsi.removeManager();
		dsi.removeMMClipCount();
		dsi.removeNoteCount();
		dsi.removeParCount();
		dsi.removePresentationFormat();
		dsi.removeScale();
		dsi.removeSlideCount();
	}

}
