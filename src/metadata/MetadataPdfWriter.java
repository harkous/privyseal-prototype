package metadata;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;


import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;

import metadata.MetadataManager.ManagedMetadata;

public class MetadataPdfWriter implements IMetadataFileWriter{

	@Override
	public void write(String srcPath, String dstPath, List<ManagedMetadata> metaToShow) {
		try {
			//Getting basic information
			PDDocument doc = PDDocument.load(srcPath);
			PDDocumentInformation info = doc.getDocumentInformation();
			
			//Keeping only the requested properties
			HashMap<ManagedMetadata, Object> metaToKeep = new HashMap<>();
			
			for(ManagedMetadata mm : metaToShow){
				if(mm.equals(ManagedMetadata.AUTHOR)){
					metaToKeep.put(ManagedMetadata.AUTHOR, info.getAuthor());
					
				}else if(mm.equals(ManagedMetadata.CREATION_DATE)){
					metaToKeep.put(ManagedMetadata.CREATION_DATE, info.getCreationDate());
				
				}else if(mm.equals(ManagedMetadata.LAST_MODIFIED)){
					metaToKeep.put(ManagedMetadata.LAST_MODIFIED, info.getModificationDate());
					
				}else if(mm.equals(ManagedMetadata.TITLE)){
					metaToKeep.put(ManagedMetadata.TITLE, info.getTitle());
				}
			}
			 
			//Removing all other properties
			removeAllProperties(info);
			
			//Writing back properties to keep
			if(metaToKeep.containsKey(ManagedMetadata.AUTHOR)){
				info.setAuthor((String)metaToKeep.get(ManagedMetadata.AUTHOR));
			}
			
			if(metaToKeep.containsKey(ManagedMetadata.CREATION_DATE)){
				info.setCreationDate((Calendar)metaToKeep.get(ManagedMetadata.CREATION_DATE));
			}
			
			if(metaToKeep.containsKey(ManagedMetadata.LAST_MODIFIED)){
				info.setModificationDate((Calendar)metaToKeep.get(ManagedMetadata.LAST_MODIFIED));
			}
			
			if(metaToKeep.containsKey(ManagedMetadata.TITLE)){
				info.setTitle((String)metaToKeep.get(ManagedMetadata.TITLE));
			}
			
			//Writing file on disk
			doc.setDocumentInformation(info);
			doc.save(dstPath);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (COSVisitorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}				
		
	}
	
	private void removeAllProperties(PDDocumentInformation info){
		info.setAuthor(null);
		info.setCreationDate(null);
		info.setCreator(null);
		info.setCustomMetadataValue(null, null);
		info.setKeywords(null);
		info.setModificationDate(null);
		info.setProducer(null);
		info.setProducer(null);
		info.setSubject(null);
		info.setTitle(null);
		info.setTrapped(null);
	}

}
