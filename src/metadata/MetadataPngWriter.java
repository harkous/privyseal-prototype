package metadata;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.ImageWriteException;
import org.apache.commons.imaging.common.bytesource.ByteSourceFile;
import org.apache.commons.imaging.formats.png.ChunkType;
import org.apache.commons.imaging.formats.png.PngConstants;
import org.apache.commons.imaging.formats.png.PngImageParser;
import org.apache.commons.imaging.formats.png.PngText;
import org.apache.commons.imaging.formats.png.PngWriter;
import org.apache.commons.imaging.formats.png.chunks.PngTextChunk;

import metadata.MetadataManager.ManagedMetadata;

public class MetadataPngWriter implements IMetadataFileWriter{

	@Override
	public void write(String srcPath, String dstPath, List<ManagedMetadata> metaToShow) {
		
		PngImageParser parser = new PngImageParser();
		try {			
			
			//Get metadata (only textual information is relevant for privacy)
			List<PngTextChunk> metaData = parser.getMetadataTextChunks(new ByteSourceFile(new File(srcPath)));
	        //Container of textual metadata
			List<PngText>  newMetaData = new ArrayList<>();
			
			//Keep only the requested metadata
			for (PngTextChunk p : metaData) {
				if(p.getKeyword().equals("Author") && metaToShow.contains(ManagedMetadata.AUTHOR)){

					newMetaData.add(p.getContents());

				}else if(p.getKeyword().equals("Title") && metaToShow.contains(ManagedMetadata.TITLE)){
					
					newMetaData.add(p.getContents());
					
				}else if(p.getKeyword().equals("Creation Time") && metaToShow.contains(ManagedMetadata.CREATION_DATE)){
					
					newMetaData.add(p.getContents());
					
				}else if(p.getKeyword().equals("Source") && metaToShow.contains(ManagedMetadata.SOURCE)){
					newMetaData.add(p.getContents());
				}
			}
			
			//Write image
			//Metadata information for the image
	        Map<String, Object> params = new HashMap<>();
	        params.put(PngConstants.PARAM_KEY_PNG_TEXT_CHUNKS, newMetaData); 	
	        
	        OutputStream os = new FileOutputStream(dstPath);
	        os = new BufferedOutputStream(os);
	        
	        PngWriter writer = new PngWriter(true);
			BufferedImage img = ImageIO.read(new File(srcPath));
			
	        writer.writeImage(img, os, params);
		} catch (ImageReadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ImageWriteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}                
		
	}		
}
