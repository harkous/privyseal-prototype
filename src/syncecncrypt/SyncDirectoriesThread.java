package syncecncrypt;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.sun.star.drawing.SnapObjectType;

import jfs.sync.JFSComparison;
import jfs.sync.JFSSynchronization;

public class SyncDirectoriesThread extends Thread{
	private WatchService watcher;
	private volatile boolean listen;
	
	public SyncDirectoriesThread(WatchService watcher){
		this.watcher = watcher;
		listen = true;
	}
	
	public void setListen(boolean l){
		listen = l;
	}
	
	
	public void sync(){
		//Compare directories
		JFSComparison.getInstance().compare();
		
		// Compute synchronization list:
	    JFSSynchronization.getInstance().computeSynchronizationLists();
	    
	    // Synchronize:
	    JFSSynchronization.getInstance().synchronize();
	}
	
public void removeFileFromXML(Path fileName){
	//TODO
}
	
	public void run(){
		//Sync when on thread start
		sync();
		while (listen) {
            WatchKey key;
            try {
                key = watcher.take();
            } catch (InterruptedException ex) {
                return;
            }
            
            
            List<WatchEvent<?>> eventList = key.pollEvents();
            //Remove entries of encrypted files if deleted
            for (WatchEvent<?> event : eventList) {
            	WatchEvent.Kind<?> kind = event.kind();
            	
            	WatchEvent<Path> ev = (WatchEvent<Path>) event;
                Path fileName = ev.context();
            	System.out.println(kind.name() + ": " + fileName);  
            	if(kind.name().equals("ENTRY_DELETE")){
            		removeFileFromXML(fileName);
            	}
            	
            }
            //If there is at least one event then sync
            if(!eventList.isEmpty()){
            	sync();
            }
                   
            boolean valid = key.reset();
            if (!valid) {
                break;
            }         
            
            try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
	}
}
