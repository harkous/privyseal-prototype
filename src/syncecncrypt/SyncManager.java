package syncecncrypt;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.Vector;
import static java.nio.file.StandardWatchEventKinds.*;

import jfs.conf.JFSConfig;
import jfs.conf.JFSDirectoryPair;
import jfs.conf.JFSSettings;
import jfs.sync.JFSComparison;
import jfs.sync.JFSSynchronization;
import jfs.sync.JFSTable;

public class SyncManager {
	private WatchService watcher;
	private SyncDirectoriesThread dirChThread;
	private File srcDir;
	private File tgtDir;
	
	public SyncManager(File confXML){
		 // Get settings for the first time in order to load stored
        // settings before doing any actions:
        JFSSettings s = JFSSettings.getInstance();

        // Get translation and configuration object:
        //JFSText t = JFSText.getInstance();
        JFSConfig config = JFSConfig.getInstance();

        // Clean config before starting (if main method is used as service):
        config.clean();      

        // Initialize new synchronization table:
        JFSTable table = JFSTable.getInstance();
        config.attach(table);
        
        setConfigFile(confXML);               
        
	}
	
	public void setConfigFile(File confXML){
		// Load XML file
		JFSConfig config = JFSConfig.getInstance();
        config.load(confXML);
        
        // Save the directories that are synced
        Vector<JFSDirectoryPair> pair = config.getDirectoryList();
        srcDir = new File(pair.get(0).getSrc());
        tgtDir = new File(pair.get(0).getTgt());

        // Setting up directory change listening
        try {
			watcher = FileSystems.getDefault().newWatchService();
			Path srcPath = srcDir.toPath();
			Path tgtPath = tgtDir.toPath();		
			srcPath.register(watcher, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
			tgtPath.register(watcher, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
			
			//Stop previous listening thread
			if(dirChThread != null && dirChThread.isAlive()){
				dirChThread.setListen(false);
				dirChThread.interrupt();
			}
			//Start new thread
			dirChThread = new SyncDirectoriesThread(watcher);
			dirChThread.start();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
	}

}
